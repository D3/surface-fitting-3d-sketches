# Piecewise-Smooth Surface Fitting onto Unstructured 3D Sketches

## Dependencies

### Python packages

You can use conda to install all dependencies:

```bash
conda env create -f environment.yml
conda activate surf-sketch
```

Or have a look at [`environment.yml`](environment.yml) for a list of packages.

### Github submodules

For convenience, we put as Github submodules a few repositories. Only these two are necessary to run the main examples:

* `pygco`
* `3d-sketches-curated-dataset`

The other submodules are for preprocessing your own input 3D sketches, so you don't have to pull them at first. Please refer to [Preprocessing section](#preprocessing) for installation instructions.

`pygco` must be built from source:

* Download the `gco-v3.0` library into the `gco_source` folder from the authors' website https://vision.cs.uwaterloo.ca/code/
* Build

```bash
cd external/pygco
make all
```

### Optional: CGAL (swig bindings)

Installation instructions can be found on the repo: https://github.com/CGAL/cgal-swig-bindings

CGAL is used in this project for:

* Point cloud simplification (in the preprocessing, to create a proxy mesh with VIPSS). Note that this step is not necessary to try our code, you can also use the sketches and proxy meshes we provide (see [Input data](#input-data)).
* Closest point to the surface queries in `scripts/utils/projection.py`. If CGAL is unavailable to you, you can replace this by [`trimesh`](https://trimsh.org/index.html). We have a replacement function in `scripts/utils/projection.py`, that uses `trimesh` instead of `CGAL`.

## Input data

To test our method, we provide some input sketches (included with this repo as submodules): https://gitlab.inria.fr/D3/3d-sketches-curated-dataset

We also provide some ready-to-go proxy meshes: https://ns.inria.fr/d3/Surface3DSketch/proxies-cleaned.zip After downloading, put all OBJ files in `proxies/cleaned`.

## Running the code

The code can be run in 2 main steps:

* Compute a segmentation of the proxy mesh, along with some parameteric surface models that fit the sketch.

```bash
cd scripts
python3 run/run_segment_and_fit.py \
	[--sketch author1] \ # Use a regex to select sketches by name (all sketches by default)
	[-S -H ] \ # Add flags to export the result as a log file and hide the debug visualisations
	[--log-folder my_results] # Specify the name of the output folder (it will be prefixed by the current date)
	[--runs 5] # Run multiple times with different random initializations and take result with lowest energy
```

* Project the proxy mesh on the parameteric surface models to obtain a piecewise-smooth surface that approximates the sketch.

```bash
cd scripts
python3 run/run_projection.py \
	[--folder-name "2022-09-14_my_results"] # Specify the name of the log folder (last created folder by default)
	[--sketch "bulbasaur|car"] \ # Use a regex to select sketches by name (all sketches by default)
	[-E -H ] \ # Add flags to export the result as a .ply mesh and hide the debug visualisations
```

## Visualizing results

Then you can use Polyscope or Blender to visualize the resulting mesh.

### With polyscope

```bash
cd scripts
python3 figure_rendering/quick_vis.py \
	[--folder 2022-09-14_my_results] \ # Default is last created folder
	[--sketch car] # Default is all sketches
```

![Polyscope visualizer](doc/Polyscope_visualize_result.png)

### With Blender

```bash
cd scripts

# Create a Blender file where all result meshes are loaded
/Applications/Blender.app/Contents/MacOS/Blender -b -P figure_rendering/blender_load_results.py -- \
	--blend-file my_results --folder 2022-09-14_my_results
# Now you can open the Blender file (in results/blender_files) and visualize the results in Blender (see fig. below)

# Render images
# Note: this requires the Blender file to contain cameras for each model
# Cameras can be created in the Blender UI, just put the camera(s) in the collection of the sketch for which it should be used (see fig. below)
/Applications/Blender.app/Contents/MacOS/Blender -b -P figure_rendering/blender_render.py -- --blend-file my_results
# All renders can be found in subfolders of results/renders
```

Below is an example what you should see after opening the Blender file. Here you can move around the cameras, or add more cameras. A camera will be used to render a given sketch/result if the camera is located in the collection of the sketch, eg here `author1_bulbasaur_camera1` and `author1_bulbasaur_camera2` will be used to render the sketch/result `author1_bulbasaur`.

![Blender UI view after blender_load_results.py script](doc/Blender_visualize_result.png)

The camera positions/orientations that are set for sketches in our dataset are loaded from the file `scripts/figure_rendering/ressources/blender_cameras.csv`. You can define your own default cameras here for any new sketch, so that they are loaded automatically by the script.

## Preprocessing

### Dependencies

#### CGAL

We use CGAL in `preprocess/simplify_xyz.py` to simplify the sketch point clouds and make it possible to run `VIPSS` on those. `VIPSS` gets really slow beyond a few thousand points. If CGAL is not available to you, you might find another off the shelf method that can simplify a point cloud and export an `obj` result. Slight modifications to the script `preprocess/convert_sketch_to_xyz.py` can then help you convert your `obj` point cloud to the `xyz` format for VIPSS.

#### VIPSS

We include VIPSS as a github submodule.

Follow instructions from their [README](https://github.com/adshhzy/VIPSS) for dependencies, then build by running:

```bash
cd external/VIPSS/vipss
cmake .
make
```

This is necessary if you want to use `VIPSS` to create your own proxy meshes with the script `run_vipss.py`

Any other point cloud surfacing method that works well with super sparse and unoriented data can do the job here.

#### Instant meshes

Instant meshes is used to isotropically remesh proxy meshes, in `scripts/preprocess/proxy_mesh_cleanup.py`.

We include [Instant meshes](https://github.com/wjakob/instant-meshes) as a github submodule. 

* Download the precompiled binaries: https://github.com/wjakob/instant-meshes#pre-compiled-binaries  
* OR: pull submodule recursively and build:

```bash
cd external/instant-meshes
cmake .
make
# Make note of the path to the built executable
```

* Then replace the variable `instant_mesh_executable` in `proxy_mesh_cleanup.py` by the path to the executable, eg on Mac it could be something like:

```python
# If downloaded as precompiled binary:
instant_mesh_executable = "/Applications/Instant\ Meshes.app/Contents/MacOS/Instant\ Meshes"
```

### Preprocessing sketches

We provide all our preprocessing scripts, useful to create proxy meshes for new input sketches, or to prepare a rough hand modelled proxy mesh.

Note that before running preprocessing or our method on a new sketch, you must add it to `metadata.csv` and fill in the columns:

*  `sketch_name` the name of your sketch. We expect all related files to have the same name (eg sketch is at: `input_sketches/3d-sketches-curated-dataset/sketch_name.obj`)
* `use_manual_proxy` a binary flag to indicate whether you will be using a hand modelled proxy (that should be provided in `proxies/manual`) or not
* `proxy_resolution` the desired edge length for the proxy mesh (I've chosen `0.007` for most of our sketches)
* `is_symmetric` a binary flag to indicate if the sketch is symmetric (along X axis)
* `open_boundary` a binary flag to indicate if the sketch has an open boundary and/or hole(s) that should be trimmed out
* `on_border_stroke_points` a list of stroke point indices that indicate which points mark the boundaries of the surface
* `ignored_stroke_points` a list of stroke point indices to be ignored by the method
* `sketch_error_dist` a parameter that defines what degree of imprecision we expect (1% of the bounding box diagonal is ok for most sketches)

To easily mark on border or ignored stroke points, I have used Blender with a little add-on that lets me load in one or multiple sketches (while maintaining vertex ordering), and lets me copy the list of indices with a button click. You can install the add-on by using this [script](scripts/preprocess/vertex_selector_blender_addon.py). The add-on panel will display on the right of the screen, alongside the `View` panel.  Vertices can be selected with any of Blender's built-in tools in edit mode, then just click the button and paste the result in the `csv` file.

```bash
cd scripts

# Optional: normalize sketch scale and center it (this will deal with all sketches from input_sketches/original_obj)
python3 preprocess/normalize_sketch_scale.py

# AUTOMATIC PROXY MESH GENERATION WITH VIPSS
# Prepare VIPSS input files
python3 preprocess/convert_sketch_to_xyz.py [--sketch "regex to select by sketch name"]

# Run VIPSS (takes some time...)
python3 preprocess/run_vipss.py [--sketch "regex to select by sketch name"] [-l VIPSS lambda parameter] [-r marching cube meshing resolution]
# Some sketches might require a different lambda parameter, eg:
python3 preprocess/run_vipss.py --sketch author1_ship -l 0.0001

# PROXY MESH PREPARATION
# Clean up proxy meshes
python3 preprocess/proxy_mesh_cleanup.py [--sketch "regex to select by sketch name"]
# Check logs for error messages: an output mesh of Instant Meshes can be non manifold, in this case the clean up process is aborted, as a non manifold proxy mesh will break our method further down the line
```

