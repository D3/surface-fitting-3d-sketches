from matplotlib import colors
import numpy as np


colors_list = list(colors.TABLEAU_COLORS.values())

def color_from_label(label):
    if label == -1:
        return colors.to_rgb("black")
    else:
        return colors.to_rgb(colors_list[int(label) % len(colors_list)])

def colors_from_labels(labels):
    colors_array = np.array([colors.to_rgb(c) for c in colors_list])
    color_per_vertex = colors_array[labels % len(colors_array)]
    return color_per_vertex