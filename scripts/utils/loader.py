import os
import json
import numpy as np
from pygel3d import hmesh

SKETCH_FILE_FOLDER = "/Users/emilie/Documents/Work/CASSIE-data/data/sketch_history"


def load_sketch_and_proxy(sketch_parameters, w_stroke, w_sharp):

    if 'sharp_strokes' in sketch_parameters:
        sharp_strokes = sketch_parameters['sharp_strokes']
    else:
        sharp_strokes = []
    if 'ignore_strokes' in sketch_parameters:
        ignore_strokes = sketch_parameters['ignore_strokes']
    else:
        ignore_strokes = []

    if 'target_segment_length' in sketch_parameters:
        target_edge_length = sketch_parameters['target_segment_length']
    else:
        target_edge_length = 0.2

    if 'sketch_file' in sketch_parameters:
        file = os.path.join(SKETCH_FILE_FOLDER, sketch_parameters['sketch_file'])

        sketch = try_load_data(file)

        all_strokes = [np.array(stroke["input_samples"]) for stroke in sketch if ((stroke["deletion_time"] is None) and (stroke["id"] not in ignore_strokes))]
        all_stroke_ids = [stroke["id"] for stroke in sketch if ((stroke["deletion_time"] is None) and (stroke["id"] not in ignore_strokes))]


        # Flatten into array of points
        data_points = np.array([point for stroke in all_strokes for point in stroke])
        segments = get_edges_for_strokes(all_strokes)
    elif 'curves_file' in sketch_parameters:
        # Try to load curves file instead
        all_strokes = try_load_curves_data(sketch_parameters['curves_file'])
        all_stroke_ids = np.arange(len(all_strokes))

        # Flatten into array of points
        data_points = np.array([point for stroke in all_strokes for point in stroke])
        segments = get_edges_for_strokes(all_strokes)
    elif 'obj_file' in sketch_parameters:
        full_path = os.path.join(OBJ_FOLDER, sketch_parameters['obj_file'])
        data_points, segments = try_load_obj_data(full_path)
        all_stroke_ids = None
    else:
        print("no sketch file defined")
        return

    # Rescale
    # all_strokes, scale_factor = rescale(all_strokes, target_scale=10)
    data_points, scale_factor = rescale(data_points, target_scale=10)

    # Resample
    # all_strokes = [resample_polyline(stroke, target_distance=target_edge_length) for stroke in all_strokes]

    if all_stroke_ids is not None:
        segment_weights = []
        v_start_idx = 0

        for stroke_id, stroke in zip(all_stroke_ids, all_strokes):
            N_s = len(stroke)
            weight = w_stroke
            if stroke_id in sharp_strokes:
                print("sharp stroke", stroke_id)
                weight = w_sharp
            
            for v_idx in range(v_start_idx, v_start_idx + N_s - 1):
                segment_weights.append(weight)

            v_start_idx += N_s
    else:
        segment_weights = [w_stroke] * len(segments)

    # Load proxy mesh
    gel_proxy_mesh = hmesh.load(sketch_parameters['proxy_mesh_file'])
    V = gel_proxy_mesh.positions()
    V *= scale_factor

    return data_points, segments, segment_weights, gel_proxy_mesh, scale_factor

def try_load_data(full_path):
    if full_path is None:
        return None

    # Reading the json as a dict
    with open(full_path) as json_data:
        data = json.load(json_data)
    
    return data

CURVES_FOLDER = '/Users/emilie/Documents/Work/3D sketch data/curves_data'

def try_load_curves_data(path):
    if path is None:
        return None
    full_path = os.path.join(CURVES_FOLDER, path)
    
    polylines = []
    current_polyline = np.empty((0,3))
    target_vertex_count = -1

    with open(full_path) as f:
        for line in f:
            els = line.split()
            if els[0] == 'v':
                # Start a new polyline
                current_polyline = np.empty((0,3))
                target_vertex_count = int(els[1])
                
            else:
                if len(current_polyline) >= target_vertex_count:
                    print("error reading file, vertex count and actual vertex data is inconsistent.")
                    break
                # Parse vertex 3D position and add it to current polyline
                pos = np.array([float(els[i]) for i in range(3)])
                current_polyline = np.row_stack([current_polyline, pos])

                if len(current_polyline) == target_vertex_count:
                    polylines.append(current_polyline)

    return polylines

OBJ_FOLDER = '/Users/emilie/Documents/Work/3D sketch data/obj_data'

def try_load_obj_data(path):
    V = np.empty((0,3))
    E = np.empty((0,2), dtype=int)
    with open(path) as file:
        for i, line in enumerate(file):
            els = line.split()
            if els[0] == 'v':
                p = np.array(els[1:]).astype(np.float)
                V = np.row_stack([V, p])
            elif els[0] == 'l':
                l = np.array(els[1:], dtype=int)
                edges = np.column_stack([l[:-1], l[1:]]) - 1
                E = np.row_stack([E, edges])
    return V, E

def get_edges_for_strokes(polylines):
    edges = np.empty((0,2))

    for idx, poly in enumerate(polylines):
        # edges = add_edge_indices_for_stroke(edges, len(poly))
        poly_indices = np.arange(len(poly) - 1) + len(edges) + idx
        poly_edges_indices = np.column_stack([poly_indices, poly_indices + 1])
        edges = np.row_stack([edges, poly_edges_indices])

    return edges.astype(int)

def resample_polyline(stroke, target_distance):
    new_stroke = np.empty((0,3))
    prev_pt_idx = 0
    for i in range(len(stroke) - 1):
        # A = stroke[i]
        A = stroke[prev_pt_idx]
        B = stroke[i + 1]
        if np.linalg.norm(B - A) >= target_distance:
            new_segment = subdivide_segment(A, B, target_distance)
            if i < len(stroke) - 2:
                new_segment = new_segment[:-1]
            new_stroke = np.row_stack((new_stroke, new_segment))
            prev_pt_idx = i + 1
        # otherwise continue

    # If empty stroke, add at least first and last point
    if len(new_stroke) == 0:
        new_segment = np.array([stroke[0], stroke[-1]])
        new_stroke = np.row_stack((new_stroke, new_segment))
    else:
        new_segment = np.array([new_segment[-1], stroke[-1]])
        new_stroke = np.row_stack((new_stroke, new_segment))

    return new_stroke

def subdivide_segment(A, B, target_distance):
    if np.linalg.norm(B - A) <= target_distance:
        return np.array([A, B])
    else:
        new_pt = 0.5 * (A + B)
        return np.row_stack([subdivide_segment(A, new_pt, target_distance)[:-1], subdivide_segment(new_pt, B, target_distance)])

# def rescale(strokes, target_scale):
def rescale(pts, target_scale):
    # pts = np.array([point for stroke in strokes for point in stroke])
    # Compute initial scale
    initial_scale = np.max(np.max(pts, axis = 0) - np.min(pts, axis = 0))
    scale_factor = target_scale / initial_scale
    print("initial scale = {}".format(initial_scale))
    # rescaled_strokes = [[point * scale_factor for point in stroke] for stroke in strokes]
    # pts = np.array([point for stroke in rescaled_strokes for point in stroke])
    print("final scale = {}".format(np.max(np.max(pts, axis = 0) - np.min(pts, axis = 0))))
    rescaled_pts = pts * scale_factor
    return rescaled_pts, scale_factor
    # return rescaled_strokes, scale_factor