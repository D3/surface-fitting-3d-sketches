import numpy as np

def get_subsets_from_labels(labels):
    labels_unique, inverse = np.unique(labels, return_inverse=True)
    subsets = [[] for i in range(len(labels_unique))]
    for i in range(len(labels)):
        subset_id = inverse[i]
        subsets[subset_id].append(i)
    return subsets, labels_unique.astype(int)

def get_subset_id_from_label(unique_labels, label):
    reverse_idx_l = np.flatnonzero(unique_labels == label)
    if len(reverse_idx_l) == 0:
        # No match
        return -1
    return reverse_idx_l.item()

def get_labels_from_subsets(subsets, N):
    labels = np.zeros(N)
    for i_l, subset in enumerate(subsets):
        for idx in subset:
            labels[idx] = i_l
    return labels

def compute_unary_energy(labels, unary_matrix):
    energy_by_node = unary_matrix[range(len(labels)), labels]
    return np.sum(energy_by_node)

def compute_smooth_energy(labels, edges, edge_weights):
    edge_labels = labels[edges]
    edges_with_disparate_labels = np.flatnonzero(edge_labels[:, 0] != edge_labels[:, 1])
    weights_to_count = edge_weights[edges_with_disparate_labels]

    return np.sum(weights_to_count)

def compute_label_energy(labels, label_costs):
    unique_labels = np.unique(labels)
    return np.sum(label_costs[unique_labels])


def get_ordered_labels(labels, normalize=True):
    u, indices = np.unique(labels, return_inverse=True)

    if normalize and len(u) > 1:
        indices = indices / (len(u) - 1)

    return indices