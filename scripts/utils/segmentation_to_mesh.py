import numpy as np
from matplotlib import colors

from main.mesh_optimization import project_to_models, smooth_project_to_models
from utils.labelling import get_subsets_from_labels


def mesh_by_projection(proxy_mesh, labels, models, vertex_is_on_mirror, in_out_labelling):
    subsets, assigned_labels = get_subsets_from_labels(labels)

    assigned_models = np.array(models)[assigned_labels]

    # V_opt = project_to_models(V, assigned_models, subsets, maxIt=10, gtol=1e-4)

    V_opt = smooth_project_to_models(proxy_mesh, assigned_models, subsets, is_on_mirror=vertex_is_on_mirror, in_out_labelling=in_out_labelling, maxIt=2, gtol=0.01)

    # v_colors = np.array([color_from_label(l) for l in labels])
    # if len(subsets) > 1:
    #     v_labels = v_labels / (len(subsets) - 1) # go from 0 to 1

    return V_opt, labels