import meshio
import numpy as np

def export_to_ply(V, F, filename, vertex_colors=None, uvs=None):

    points_data = {}
    if vertex_colors is not None:
        points_data['red'] = (vertex_colors[:, 0] * 255).astype(np.uint8)
        points_data['green'] = (vertex_colors[:, 1] * 255).astype(np.uint8)
        points_data['blue'] = (vertex_colors[:, 2] * 255).astype(np.uint8)
        #     "red": (vertex_colors[:, 0] * 255).astype(np.uint8),
        #     "green": (vertex_colors[:, 1] * 255).astype(np.uint8),
        #     "blue": (vertex_colors[:, 2] * 255).astype(np.uint8)
        # }
    # WARNING: Blender does not read correctly the uvs from the ply file
    # Use export to OBJ instead if that doesn't work
    if uvs is not None:
        # points_data['u'] = uvs[:, 0].astype(np.float)
        # points_data['v'] = uvs[:, 1].astype(np.float)
        points_data['obj:vt'] = (np.column_stack([np.arange(len(V)) / len(V), np.zeros(len(V))])).astype(np.float)
    # else:
    #     points_data = None

    mesh = meshio.Mesh(
        V,
        cells = [("triangle", F.astype(np.int32))], point_data=points_data)

    mesh.write(filename)


def export_to_obj( V, F, filename: str, tex_coords=None):
    F += 1 # indices start at 1
    with open( filename, 'w' ) as ofile:
        for vtx in V:
            ofile.write('v '+' '.join(['{}'.format(v) for v in vtx])+'\n')
        if tex_coords is not None:
            for tex in tex_coords:
                ofile.write('vt '+' '.join(['{}'.format(vt) for vt in tex])+'\n')
        if F is not None:
            for f in F:
                if tex_coords is None:
                    ofile.write('f '+' '.join(['{}'.format(vi) for vi in f])+'\n')
                else:
                    ofile.write('f '+' '.join(['{}/{}'.format(vi, vi) for vi in f])+'\n')