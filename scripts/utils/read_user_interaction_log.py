from operator import index
import numpy as np
import sys


from utils.mirroring import index_in_half_mesh
from utils.math import remove_mesh_vertices

def get_new_segment_sharpness(mark_sharp_action, N_pts, segments, sharpness_by_segment, data_points_mask=None):
    selected_pts = np.array(mark_sharp_action['parameters']['selected_pts'])
    sharpness_value = mark_sharp_action['parameters']['sharpness']

    if data_points_mask is not None:
        is_selected = np.zeros(len(data_points_mask), dtype=bool)
        is_selected[selected_pts] = 1
        is_selected = is_selected[data_points_mask]
        selected_pts = np.flatnonzero(is_selected)

    is_selected = np.zeros(N_pts, dtype=bool)
    is_selected[selected_pts] = True

    # Find segments that are selected
    segment_is_selected = np.any(is_selected[segments], axis=1)

    new_shaprness = sharpness_by_segment.copy()
    new_shaprness[segment_is_selected] = sharpness_value

    return new_shaprness

def get_selected_vertices(on_mesh_action, V, F, is_symmetric, in_out_labelling):
    selected_verts = np.array(on_mesh_action['parameters']['selected_verts'])
    # if is_symmetric:
    #     print(selected_verts)
    #     selected_verts = index_in_half_mesh(V, selected_verts)
    #     print(selected_verts)

    if np.any(in_out_labelling):
        # Correct indexing to account for removed vertices
        _, _, mask = remove_mesh_vertices(V, F, in_out_labelling)
        kept_idx = np.arange(len(mask))[mask]
        selected_verts = selected_verts[selected_verts < len(kept_idx)]
        selected_verts = kept_idx[selected_verts]
    
    return selected_verts