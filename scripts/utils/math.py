import numpy as np

def get_bounds(data_points, padding=0.2):
    sketch_min_bound = np.min(data_points, axis = 0)
    sketch_max_bound = np.max(data_points, axis = 0)

    span = sketch_max_bound - sketch_min_bound
    span[span < 1e-3] += 1e-3 # add just a little bit of extra padding (in case it's zero)

    sketch_min_bound -= padding * span
    sketch_max_bound += padding * span
    center = np.mean(np.array([sketch_min_bound, sketch_max_bound]), axis = 0)
    span = sketch_max_bound - sketch_min_bound

    return center, span

def bounding_box_diagonal(pts):
    return np.linalg.norm(np.max(pts, axis = 0) - np.min(pts, axis = 0))


def divide_by_square_norm(vectors):
    norms = np.linalg.norm(vectors, axis = 1)
    norms[norms < 1e-8] = 1
    vs = vectors / norms[:, None]**2
    return vs

def normalize(vectors):
    norms = np.linalg.norm(vectors, axis = 1)
    norms[norms < 1e-8] = 1
    vs = vectors / norms[:, None]
    return vs


def estimate_gradient(f, p):
    h = 0.0001
    dx = h * np.array([1,0,0])
    dy = h * np.array([0,1,0])
    dz = h * np.array([0,0,1])
    g_x = f(p + dx) - f(p - dx)
    g_y = f(p + dy) - f(p - dy)
    g_z = f(p + dz) - f(p - dz)

    return np.array([g_x, g_y, g_z]).T / (2 * h)


def cross_product_matrix(v):
    return np.array([
        [0, -v[2], v[1]],
        [v[2], 0, -v[0]],
        [-v[1], v[0], 0]
        ])

def index_into_flat_3d(indices):
    return 3 * np.repeat(indices, 3) + np.tile(np.arange(3), len(indices))


def remove_mesh_vertices(V, F, should_remove_vertex_mask):
    F_should_keep = np.logical_not(np.all(should_remove_vertex_mask[F], axis = 1))

    F_kept = F[F_should_keep]
    V_kept = np.unique(F_kept)

    V_mask = np.zeros(len(V), dtype=bool)
    V_mask[V_kept] = True

    # data_points = data_points[data_points_mask]
    V = V[V_kept]
    # Correct faces indexing
    F = np.searchsorted(V_kept, F_kept)

    return V, F, V_mask

def remove_data_points(V, E, ignored_pts_idx):
    mask_V = np.ones(len(V), dtype=bool)
    mask_V[ignored_pts_idx] = False

    mask_E = np.all(mask_V[E], axis = 1)
    E_kept = E[mask_E]
    vertices_kept = np.unique(E[mask_E])

    mask_V[:] = False
    mask_V[vertices_kept] = True

    V = V[mask_V]

    # Correct edges indexing
    E = np.searchsorted(vertices_kept, E_kept)

    return V, E, mask_V, mask_E

def grow_line(stroke_segments, stroke_index_by_segment, start_node, stroke_index):
    unassigned_segments_index = np.flatnonzero(stroke_index_by_segment == -1)
    unassigned_segments = stroke_segments[unassigned_segments_index]
    while(np.any(unassigned_segments == start_node)):
        matching_edge_idx = np.flatnonzero(np.any(unassigned_segments == start_node, axis = 1))[0]
        next_edge = unassigned_segments[matching_edge_idx]
        next_edge_idx = unassigned_segments_index[matching_edge_idx]
        next_node = next_edge[next_edge != start_node]
        start_node = next_node
        stroke_index_by_segment[next_edge_idx] = stroke_index

        unassigned_segments_index = np.delete(unassigned_segments_index, matching_edge_idx)
        unassigned_segments = stroke_segments[unassigned_segments_index]

def connected_lines_from_segments(stroke_segments):
    stroke_index_by_segment = np.ones(len(stroke_segments), dtype=int) * -1
    stroke_index = 0
    for e in range(len(stroke_segments)):
        if stroke_index_by_segment[e] != - 1:
            continue
        line = stroke_segments[e]
        stroke_index_by_segment[e] = stroke_index
        # Grow line from both sides
        end_node = line[-1]
        grow_line(stroke_segments, stroke_index_by_segment, end_node, stroke_index)
        start_node = line[0]
        grow_line(stroke_segments, stroke_index_by_segment, start_node, stroke_index)
        stroke_index += 1

    return stroke_index_by_segment