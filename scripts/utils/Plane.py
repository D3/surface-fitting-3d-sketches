import numpy as np
from scipy.linalg import svd


class Plane:
    def __init__(self, p0, P):
        self.p0 = p0
        self.P = P # contains basis vector that may not be right-handed

    # TODO: switch names between projec/transform methods

    def project(self, points):
        return (points - self.p0) @ self.P.T

    def inverse_project(self, points):
        return points @ self.P + self.p0

    def transform(self, points):
        proj = self.project(points)
        return proj.T[:2].T, proj.T[2]

    def transform_z(self, points):
        proj = self.project(points)
        return proj.T[2]

    def inverse_transform(self, points, z = None):
        if len(points.shape) == 1:
            points = points.reshape((1,-1))
        if z is None:
            z = np.zeros(len(points))
        if points.shape[-1] == 2:
            return self.inverse_project(np.column_stack([points, z]))
        else:
            inv_proj = self.inverse_project(points)
            # Replace z column
            inv_proj[:, 2] = z
            return inv_proj

    def project_vec(self, vectors):
        return vectors @ self.P.T
        # proj = vector - np.dot(vector, self.normal()) * self.normal()
        # if np.linalg.norm(proj) == 0:
        #     return np.zeros(3)
        # proj /= np.linalg.norm(proj)
        # return proj

    def transform_vec(self, vectors):
        vec_in_plane_coords = self.project_vec(vectors)
        # Keep only 2D coords and normalize 2D vector
        vec_2d = vec_in_plane_coords.T[:2].T
        # vec_2d = vec_2d / np.linalg.norm(vec_2d, axis = 1)[:,None]
        return vec_2d, vec_in_plane_coords.T[2]

    def inverse_transform_vec(self, vectors):
        z = np.zeros(len(vectors))
        return np.column_stack([vectors, z]) @ self.P

    def normal(self):
        return self.P[2]


def least_squares_fit_plane(data_points):
    p0 = np.mean(data_points, axis = 0)
    data_centered = data_points - p0
    u, s, vh = svd(data_centered)
    return Plane(p0, vh)

def plane_from_triangle(a, b, c):
    p0 = np.mean([a, b, c], axis = 0)
    ab = b-a
    ab /= np.linalg.norm(ab)
    ac = c-a
    ac /= np.linalg.norm(ac)
    n = np.cross(ab, ac)
    n /= np.linalg.norm(n)
    u = ab
    v = np.cross(n, u)
    P = np.array([u, v, n])
    triangle_plane = Plane(p0, P)
    return triangle_plane