import math
import numpy as np

from utils.math import remove_mesh_vertices, normalize

on_mirror_threshold = 1e-6
aligned_with_mirror_threshold = math.pi / 8

# For code simplicity, all operations assume a mirror plane at x = 0
# For any mirror plane different from that, the model must be transformed beforehand such that the mirror plane becomes x = 0


def is_on_mirror(V, threshold=None):
    if threshold is None:
        threshold = on_mirror_threshold
    return np.abs(V[:, 0]) < threshold

def should_remove(vs, threshold=None):
    if threshold is None:
        threshold = on_mirror_threshold

    return vs[:, 0] <= - threshold

def is_in_mirror_plane(vec, threshold=None):
    vec = vec.reshape((-1, 3))
    if threshold is None:
        threshold = aligned_with_mirror_threshold

    vec = normalize(vec)

    return np.abs(np.dot(vec, np.array([1, 0, 0]))) < threshold

def remove_symmetric_data(data_points, segments, threshold=None):
    # if threshold is None:
    #     threshold = on_mirror_threshold
    # data_points_mask = data_points[:, 0] > - threshold # Include points on the mirror plane (at x = 0)
    data_points_mask = np.logical_not(should_remove(data_points, threshold))
    # data_points_indices = np.flatnonzero(data_points_mask)
    segments_mask = np.any(data_points_mask[segments], axis = 1)

    data_points_kept = np.unique(segments[segments_mask])

    data_points_mask = np.zeros(len(data_points), dtype=bool)
    data_points_mask[data_points_kept] = True

    # data_points = data_points[data_points_mask]
    data_points = data_points[data_points_kept]
    segments_wrong_indexing = segments[segments_mask]
    segments = np.searchsorted(data_points_kept, segments_wrong_indexing)

    return data_points, segments, segments_mask, data_points_mask

def cut_mesh(V, F):
    # WARNING: this assumes that there is a nice edge line parallel to the mirror and relatively close to it
    V_should_remove = should_remove(V, threshold=0)

    new_V, new_F, V_mask = remove_mesh_vertices(V, F, V_should_remove)

    # # Snap line of vertices closer to the mirror onto the mirror
    # closest_to_mirror_value = np.min(new_V[:, 0])
    # vertices_should_snap = (new_V[:, 0] - closest_to_mirror_value) < on_mirror_threshold

    # new_V[vertices_should_snap, 0] = 0

    return new_V, new_F, V_mask

def get_symmetric(pts):
    sym_pts = pts.copy()
    sym_pts[:, 0] = - sym_pts[:, 0]

    return sym_pts

def symmetrize_points(V):
    sym = get_symmetric(V)
    new_V_indexing = np.append(np.arange(len(V)), np.arange(len(V)))
    return np.row_stack([V, sym]), new_V_indexing

def symmetrize_mesh(V, F, threshold=None):
    # Compute symmetric of all vertices that do not lie on the mirror plane
    # is_lying_on_mirror = np.abs(V[:, 0]) < threshold
    is_lying_on_mirror = is_on_mirror(V, threshold)
    to_mirror_idx = np.flatnonzero(np.logical_not(is_lying_on_mirror))
    V_to_mirror = V[to_mirror_idx]
    V_mirrored = get_symmetric(V_to_mirror)

    new_V = np.row_stack([V, V_mirrored])

    new_V_indexing = np.append(np.arange(len(V)), to_mirror_idx)

    # Symmetrize faces
    V_mirrored_indices = np.arange(len(V), len(new_V))
    index_mapping = np.arange(len(V))
    index_mapping[to_mirror_idx] = V_mirrored_indices

    new_F = np.row_stack([F, index_mapping[F][:, ::-1]]) # reverse order of mirrored faces to get the same ordering

    return new_V, new_F, new_V_indexing

def index_in_half_mesh(V, indices, threshold=None):
    """
        If indices is a selection of indices in the symmetrized mesh,
        this returns the indices in the half mesh
    """
    is_lying_on_mirror = is_on_mirror(V, threshold)
    to_mirror_idx = np.flatnonzero(np.logical_not(is_lying_on_mirror))

    symmetric_indexing = np.append(np.arange(len(V)), to_mirror_idx)

    return np.unique(symmetric_indexing[indices])

def symmetrize_graph(nodes, edges):
    new_nodes, new_indexing = symmetrize_points(nodes)
    edges_mirrored = edges.copy() + len(nodes)
    new_edge_indexing = np.append(np.arange(len(edges)), np.arange(len(edges)))
    return new_nodes, np.row_stack([edges, edges_mirrored]), new_indexing, new_edge_indexing
