import os

ROOT_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, os.pardir)
LOGS_ROOT_FOLDER = os.path.join(ROOT_FOLDER, 'results', 'segment_and_fit_logs')
METADATA_FILE_PATH = os.path.join(ROOT_FOLDER, 'metadata.csv')
MESH_FOLDER = os.path.join(ROOT_FOLDER, 'results', 'meshes')
ORIGINAL_SKETCH_OBJ_FOLDER = os.path.join(ROOT_FOLDER, 'input_sketches', 'original_obj')
SKETCH_OBJ_FOLDER = os.path.join(ROOT_FOLDER, 'input_sketches', '3d-sketches-curated-dataset')
SKETCH_XYZ_FOLDER = os.path.join(ROOT_FOLDER, 'input_sketches', 'vipss_format')
VIPSS_PROXIES_FOLDER = os.path.join(ROOT_FOLDER, 'proxies', 'vipss')
MANUAL_PROXIES_FOLDER = os.path.join(ROOT_FOLDER, 'proxies', 'manual')
CLEANED_PROXIES_FOLDER = os.path.join(ROOT_FOLDER, 'proxies', 'cleaned')
# USER_INTERACTION_LOGS_FOLDER = os.path.join(ROOT_FOLDER, 'scripts', 'user_interaction_recordings')
BLENDER_FILES_PATH = os.path.join(ROOT_FOLDER, "results", "blender_files")
RENDERS_PATH = os.path.join(ROOT_FOLDER, "results", "renders")
# TURNTABLES_PATH = os.path.join(ROOT_FOLDER, "results", "turntables")

def get_parameter_file_name(log_file_name):
    return os.path.splitext(log_file_name)[0] + "-parameters" + ".json"