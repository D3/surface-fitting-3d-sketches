import numpy as np
from scipy.sparse import csr_matrix


def get_edge_key(edge):
    return np.array2string(np.sort(edge), precision=0)

def get_edge_from_key(key):
    return np.fromstring(key[1:-1], dtype=int, sep=' ')

def incidence_matrix(N_vertices, edges):
    M = np.zeros((N_vertices, len(edges)))

    data = np.ones(len(edges) * 2)
    row_indices = np.hstack([edges[:, 0], edges[:, 1]])
    col_indices = np.hstack([np.arange(len(edges)), np.arange(len(edges))])

    return csr_matrix((data, (row_indices, col_indices)), shape=(N_vertices, len(edges)))
        

def affinity_matrix(N_nodes, edges, edge_weights):

    affinity = np.zeros((N_nodes, N_nodes))

    for e, w in zip(edges, edge_weights):
        affinity[e[0], e[1]] = w

    affinity += affinity.T

    return affinity

def affinity_matrix_sparse(N_nodes, edges, edge_weights):
    # data = []
    # rows = []
    # cols = []

    # for e, w in zip(edges, edge_weights):
    #     data.append(w)
    #     rows.append(e[0])
    #     cols.append(e[1])
    data = edge_weights
    rows = edges[:, 0]
    cols = edges[:, 1]
    
    affinity = csr_matrix((data, (rows, cols)), shape=(N_nodes, N_nodes))
    affinity += affinity.T
    return affinity