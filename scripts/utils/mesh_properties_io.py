import numpy as np

def pack_mesh_properties_to_rgb(labels, model_degrees, is_seam, is_sharp):
    seam_marker = np.zeros(len(labels))
    seam_marker[is_seam] = 0.5
    seam_marker[is_sharp] = 1

    return np.column_stack([
                labels / np.max(labels),
                model_degrees / 4,
                seam_marker])

def unpack_mesh_properies(points_data):
    r = points_data["red"] / 255
    g = points_data["green"] / 255
    b = points_data["blue"] / 255

    labels = r
    models_degrees = g * 4
    is_seam = b == 0.5
    is_sharp = b == 1

    return labels, models_degrees, is_seam, is_sharp

def unpack_mesh_properties_blender(blender_mesh):

    color_layer = blender_mesh.vertex_colors['Col']

    sharpness_by_vertex = np.zeros(len(blender_mesh.vertices), dtype=bool)
    vertex_is_seam = np.zeros(len(blender_mesh.vertices), dtype=bool)

    label_by_poly = []
    degree_by_poly = []

    for poly in blender_mesh.polygons:
        # Find out the color of this face
        label = 0
        degree = 0
        for i, index in enumerate(poly.vertices):
            loop_index = poly.loop_indices[i]
            if color_layer.data[loop_index].color[0] > 0:
                label = color_layer.data[loop_index].color[0]
                degree = color_layer.data[loop_index].color[1]

            sharpness_by_vertex[index] = color_layer.data[loop_index].color[2] > 0.5
            vertex_is_seam[index] = color_layer.data[loop_index].color[2] > 0
            
        label_by_poly.append(label)
        degree_by_poly.append(degree)

    return sharpness_by_vertex, vertex_is_seam, label_by_poly, degree_by_poly