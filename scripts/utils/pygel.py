import numpy as np
import meshio

def export_to_ply(m, filename, vertex_colors=None):

    if vertex_colors is not None:
        points_data = {
            "red": (vertex_colors[:, 0] * 255).astype(np.uint8),
            "green": (vertex_colors[:, 1] * 255).astype(np.uint8),
            "blue": (vertex_colors[:, 2] * 255).astype(np.uint8)
        }
    else:
        points_data = None

    mesh = meshio.Mesh(
        m.positions(),
        cells = [("triangle", get_faces(m))], point_data=points_data)

    mesh.write(filename, binary=True)

def split_halfedge(m, he, pos=None):
    # Split
    v_idx = m.split_edge(he)
    
    # Correct position
    if pos is not None:
        m.positions()[v_idx] = pos
    
    # Split both incident faces (except if border)
    face_0 = m.incident_face(he)
    if m.face_in_use(face_0):
        corner_0 = m.incident_vertex(m.next_halfedge(he))
        m.split_face_by_edge(face_0, v_idx, corner_0)
#         print("corner vertex =", corner_0)
    opp_he = m.opposite_halfedge(he)
    face_1 = m.incident_face(opp_he)
    if m.face_in_use(face_1):
        corner_1 = m.incident_vertex(m.next_halfedge(m.next_halfedge(opp_he)))
#         print("opposite corner vertex =", corner_1)
        m.split_face_by_edge(face_1, v_idx, corner_1)

    v1 = m.incident_vertex(he)
    v0 = m.incident_vertex(opp_he)

    # print("split edge [{},{}]".format(v0, v1))
        
    return v_idx

def try_split_halfedge(m, he, eps, pos=None):
    # Only split if the new edges would be longer than eps
    vA = m.incident_vertex(he)
    vB = m.incident_vertex(m.opposite_halfedge(he))
    pos2 = pos
    if pos2 is None:
        pos2 = np.mean([vA, vB], axis = 0)
    dist_a = np.linalg.norm(m.positions()[vA] - pos2)
    dist_b = np.linalg.norm(m.positions()[vB] - pos2)
    if min(dist_a, dist_b) < eps:
        if dist_a < dist_b:
            if ((pos is not None) and (dist_b > eps)):
                print("assigning new position to vertex", vA)
                m.positions()[vA] = pos
            return vA
        else:
            if ((pos is not None) and (dist_a > eps)):
                print("assigning new position to vertex", vB)
                m.positions()[vB] = pos
            return vB
    return split_halfedge(m, he, pos)

def get_halfedge(m, v0, v1):
    for he in m.circulate_vertex(v0, mode='h'):
        v_opp = m.incident_vertex(he)
        if v_opp == v0:
            v_opp = m.incident_vertex(m.opposite_halfedge(he))
        if v_opp == v1:
            return he
    return None

def split_edge(m, v0, v1, pos=None):
    # Find he index
    he_split = -1
    for he in m.circulate_vertex(v0, mode='h'):
        v_opp = m.incident_vertex(he)
        if v_opp == v0:
            v_opp = m.incident_vertex(m.opposite_halfedge(he))
        if v_opp == v1:
            he_split = he
#             print("should split halfedge", he)
            break
    if he_split == -1:
        print("couldn't find edge [{},{}]".format(v0, v1))
        return -1
    
    return split_halfedge(m, he_split, pos)


def get_faces(m):
    faces = []
    for f in m.faces():
        if m.face_in_use(f):
            verts = [v for v in m.circulate_face(f, mode='v')]
            if len(verts) == 3:
                faces.append(verts)
            else:
                print("WARNING: non triangle face")
        else:
            print("WARNING: Ignoring unused face")
    return np.array(faces, dtype=int)


def get_edges(m):
    edges = np.empty((0,2), dtype=int)
    he_indices = []
    for he in m.halfedges():
        # Prevent duplicates by storing only halfedges that point in asc v idx direction
        v0 = m.incident_vertex(m.opposite_halfedge(he))
        v1 = m.incident_vertex(he)
        if v0 < v1:
            # Add edge
            edges = np.row_stack([edges, [v0, v1]])
            he_indices.append(he)
    return edges, he_indices

def compute_edge_lengths(positions, edges):
    return np.linalg.norm(positions[edges][:, 0, :] - positions[edges][:, 1, :], axis = 1)

def get_normals(m):
    normals = np.empty((0,3))
    for vid in range(len(m.positions())):
        normals = np.row_stack([normals, m.vertex_normal(vid)])
    return normals


def get_face_neighbor_by_edge(m, current_face, v0, v1):
    for h in m.circulate_face(current_face, mode='h'):
        if m.incident_vertex(h) == v0 and m.incident_vertex(m.opposite_halfedge(h)) == v1:
            # correct face is incident to the opposite of h
            return m.incident_face(m.opposite_halfedge(h))
        if m.incident_vertex(h) == v1 and m.incident_vertex(m.opposite_halfedge(h)) == v0:
            # correct face is incident to the opposite of h
            return m.incident_face(m.opposite_halfedge(h))
    print("Didn't find neighbor for face {} and edge [{}, {}]".format(current_face, v0, v1))
    return -1



def find_boundary_edges(labels, m):
    boundary_edges_id = []
    labels_at_edges = []

    mesh_edges, he_idx = get_edges(m)

    for e, he_i in zip(mesh_edges, he_idx):
        # do the vertices of this edge have different labels?
        if labels[e[0]] != labels[e[1]]:
            boundary_edges_id.append(he_i)
            labels_at_edges.append([labels[e[0]], labels[e[1]]])
    
    return np.array(boundary_edges_id), labels_at_edges

def find_boundary_faces(labels, m):
    boundary_faces_id = []
    labels_at_faces = []
    for fid in m.faces():
        # Inspect all 3 vertices, if they have all different label, add to the list
        labels_f = []
        for vid in m.circulate_face(fid, mode='v'):
            labels_f.append(labels[vid])
        if len(np.unique(labels_f)) == 3:
            boundary_faces_id.append(fid)
            labels_at_faces.append(labels_f)

    return boundary_faces_id, labels_at_faces

def min_angle(v0, v1, v2):
    a = v1 - v0
    a /= np.linalg.norm(a)

    b = v2 - v1
    b /= np.linalg.norm(b)

    c = v0 - v2
    c /= np.linalg.norm(c)

    return min([np.dot(a, -c), np.dot(b, -a), np.dot(c, -b)])

def min_angle_energy_delta(m, hid):
    hv = m.incident_vertex(hid)
    hnv = m.incident_vertex(m.next_halfedge(hid))
    hov = m.incident_vertex(m.opposite_halfedge(hid))
    honv = m.incident_vertex(m.next_halfedge(m.opposite_halfedge(hid)))


    V = m.positions()
    v0 = V[hv]
    v1 = V[hnv]
    v2 = V[hov]
    v3 = V[honv]

    before = min(min_angle(v0, v1, v2), min_angle(v0, v2, v3))
    after = min(min_angle(v0, v1, v3), min_angle(v1, v2, v3))

    return after - before

def face_vertices(m, fid):
    return [vid for vid in m.circulate_face(fid, mode='v')]