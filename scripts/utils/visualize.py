import numpy as np
import polyscope as ps
import matplotlib.pyplot as plt


from main.mesh_optimization import smooth_project_to_models
from utils.Plane import least_squares_fit_plane
from utils.labelling import get_subsets_from_labels
from utils.colors import colors_from_labels, color_from_label, colors_list
from utils.segmentation_to_mesh import mesh_by_projection
from utils.mirroring import get_symmetric, symmetrize_mesh, symmetrize_graph, symmetrize_points
from utils.math import remove_mesh_vertices



def polygonize_models(models, centers, spans):
    Vs = []
    Fs = []
    for i, m in enumerate(models):
        V, F = m.polygonize(centers[i], spans[i])
        Vs.append(V)
        Fs.append(F)

    return Vs, Fs

def visualize_labelling(proxy_vertices, labels, only_assigned=True, align_to_xy_plane=True):
    # Label colors
    if only_assigned:
        unique_labels, label_colors_idx = np.unique(labels, return_inverse=True)
        if unique_labels[0] == -1:
            label_colors_idx -= 1 # If -1 label is present, assign it to black
        node_colors = np.array([color_from_label(label_colors_idx[idx]) for idx in range(len(labels))])
    else:
        node_colors = np.array([color_from_label(l) for l in labels])

    if align_to_xy_plane:
        # Transform the points to align the best fit plane to xy-plane
        best_plane = least_squares_fit_plane(proxy_vertices)

        proxy_vertices = best_plane.project(proxy_vertices)

    ps_nodes = ps.register_point_cloud("nodes", proxy_vertices)
    ps_nodes.add_color_quantity("label", node_colors, enabled=True)

def visualize_nodes(proxy, labels, fidelity_energy_by_pt):
    node_colors = np.array([color_from_label(l) for l in labels])
    ps_nodes = ps.register_point_cloud("nodes", proxy.nodes_positions)
    ps_nodes.add_color_quantity("label", node_colors, enabled=True)
    ps_nodes.add_scalar_quantity("fidelity energy (first order approx)", fidelity_energy_by_pt)

    return [ps_nodes]


# def visualize_models(models)

def visualize(proxy, labels, Vs, Fs, fidelity_energy_by_pt=None, translation=np.array([0,0,0]), only_assigned_models=True):
    symmetry = proxy.mirror
    nodes_positions = proxy.nodes_positions
    # Keep track of structures added to polyscope
    structures = []

    # Label colors
    if only_assigned_models:
        unique_labels, label_colors_idx = np.unique(labels, return_inverse=True)
        if unique_labels[0] == -1:
            label_colors_idx -= 1 # If -1 label is present, assign it to black
        node_colors = np.array([color_from_label(label_colors_idx[idx]) for idx in range(len(labels))])
    else:
        node_colors = np.array([color_from_label(l) for l in labels])

    for idx, (V, F) in enumerate(zip(Vs, Fs)):
        if symmetry:
            V, F, _ = symmetrize_mesh(V, F)
        ps_mesh_i = ps.register_surface_mesh("mesh model {}".format(idx), V + translation, F, enabled=False, material="wax", color=color_from_label(idx))
        structures.append(ps_mesh_i)

    # Labelling
    # if symmetry:
    #     nodes_positions, new_node_indexing = symmetrize_points(nodes_positions)
    #     node_colors = node_colors[new_node_indexing]
    #     if fidelity_energy_by_pt is not None:
    #         fidelity_energy_by_pt = fidelity_energy_by_pt[new_node_indexing]

    ps_nodes = ps.register_point_cloud("nodes", nodes_positions)
    ps_nodes.add_color_quantity("label", node_colors, enabled=True)

    if fidelity_energy_by_pt is not None:
        # Fidelity energy by node
        ps_nodes.add_scalar_quantity("fidelity energy (first order approx)", fidelity_energy_by_pt)

    structures.append(ps_nodes)

    # Color proxy mesh with labelling
    proxy_mesh_coloring = proxy.property_by_vertex(node_colors)

    V = proxy.V
    F = proxy.F

    if symmetry:
        V, F, new_vertex_indexing = symmetrize_mesh(V, F)
        proxy_mesh_coloring = proxy_mesh_coloring[new_vertex_indexing]

    ps_proxy_colored = ps.register_surface_mesh("labelled proxy", V, F)
    ps_proxy_colored.add_color_quantity("label", proxy_mesh_coloring)

    structures.append(ps_proxy_colored)

    return structures

def visualize_models_proposals(sf, center, span, translation=np.array([0,0,0])):
    # Keep track of structures added to polyscope
    structures = []

    Vs, Fs = sf.get_proposed_models_mesh(center, span)
    for idx, (V, F) in enumerate(zip(Vs, Fs)):
        ps_mesh_i = ps.register_surface_mesh("mesh model {}".format(idx), V + translation, F, enabled=False, material="wax", color=color_from_label(idx))
        structures.append(ps_mesh_i)

    return structures

def visualize_models_by_projection(mesh, V, F, models, labels, in_out_labels=None, is_on_mirror=None, translation=np.array([0,0,0])):
    
    # V_opt, v_labels = mesh_by_projection(V, F, labels, models)
    subsets, assigned_labels = get_subsets_from_labels(labels)
    assigned_models = np.array(models)[assigned_labels]
    V_opt = smooth_project_to_models(mesh, assigned_models, subsets, is_on_mirror=is_on_mirror, in_out_labelling=in_out_labels, maxIt=2, gtol=0.01)

    # Outside boundaries?
    # if in_out_labels is not None:
    #     # V, F, new_V_indexing = remove_mesh_vertices(V, F, in_out_labels)
    #     # labels = labels[new_V_indexing]

    #     # ps_cutout = ps.register_surface_mesh("cutout proxy", V, F)
    #     # ps_cutout.add_scalar_quantity("label", labels)

    #     # ps.show()

    #     # V_opt, F, new_V_indexing = remove_mesh_vertices(V_opt, F, in_out_labels)
    #     # labels = labels[new_V_indexing]

    #     V_opt[in_out_labels] = V[in_out_labels]

    # Symmetrize
    if is_on_mirror is not None:
        V_opt, F, new_V_indexing = symmetrize_mesh(V_opt, F)
        labels = labels[new_V_indexing]
        in_out_labels = in_out_labels[new_V_indexing]

    V_opt += translation

    ps_opt = ps.register_surface_mesh("projected proxy", V_opt, F)
    ps_opt.add_scalar_quantity("label", labels, cmap='rainbow', enabled=True)

    # Visualize model degree
    degrees = [models[l].degree for l in labels]
    ps_opt.add_scalar_quantity("model degree", np.array(degrees), cmap='coolwarm', vminmax=(0,4))
    if in_out_labels is not None:
        ps_opt.add_scalar_quantity("in/out", in_out_labels, defined_on='vertices')
    # ps_opt.add_color_quantity("label color", v_colors)

    return [ps_opt]


def visualize_graph(vertices, graph_edges, edges_weights, data_pts_indices_by_node, data_pts, symmetry=False):
    # if symmetry:
    #     vertices, graph_edges, _, new_edge_indexing = symmetrize_graph(vertices, graph_edges)
    #     edges_weights = edges_weights[new_edge_indexing]
    
    ps_graph = ps.register_curve_network("graph", vertices, graph_edges, enabled=False)
    ps_graph.add_scalar_quantity("weight", edges_weights, defined_on='edges', enabled=True)

    # ps_nodes = ps.register_point_cloud("graph nodes", vertices)
    # nb_pts_per_nodes = np.array([len(indices) for indices in data_pts_indices_by_node])
    # ps_nodes.add_scalar_quantity("nb of data pts", nb_pts_per_nodes)

    # node_with_most_vertices = np.argmax(nb_pts_per_nodes)

    # ps.register_point_cloud("data pts for node", data_pts[data_pts_indices_by_node[node_with_most_vertices]])

    return [ps_graph]


def ps_clear(structures):
    for structure in structures:
        structure.remove()


def plot_energy(energy_progression):
    x = np.arange(1, len(energy_progression) + 1)
    y = energy_progression

    fig, ax = plt.subplots()
    ax.plot(x, y)
    ax.set_xlabel('Iteration')
    ax.set_ylabel('Energy')
    plt.xticks(x)

    plt.show()

def plot_energies(multiple_energy_progressions, groups=None):
    x_max = max([len(energies) for energies in multiple_energy_progressions])

    fig, ax = plt.subplots()
    ax.set_xlabel('Iteration')
    ax.set_ylabel('Energy')
    plt.xticks(range(x_max))

    for idx, es in enumerate(multiple_energy_progressions):
        if groups is not None:
            group = groups[idx]
            color = color_from_label(group)
        else:
            color = color_from_label(idx)

        ax.plot(range(len(es)), es, label='Run {}'.format(idx + 1), color=color)

    ax.legend()

    plt.show()

def plot_multi_group_energies(energy_progressions_by_group, group_labels=None, plot_scatter=True):
    energy_progressions_mean_by_group = np.median(energy_progressions_by_group, axis = 2)
    energy_progression_std_by_group = np.std(energy_progressions_by_group, axis=2)

    print(energy_progressions_mean_by_group.shape)
    print(energy_progression_std_by_group.shape)

    print(energy_progressions_mean_by_group - energy_progression_std_by_group)


    fig, ax = plt.subplots()
    ax.set_xlabel("Iterations")
    ax.set_ylabel("Energy")

    N_groups, N_it, N_rand = energy_progressions_by_group.shape

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    plt.xticks(range(1, N_it + 1))
    x = np.arange(1, N_it + 1)
    low_bounds = energy_progressions_mean_by_group-energy_progression_std_by_group
    high_bounds = energy_progressions_mean_by_group+energy_progression_std_by_group
    for i in range(N_groups):
        ax.plot(x, energy_progressions_mean_by_group[i], c=colors[i])
        ax.fill_between(x, low_bounds[i], high_bounds[i],alpha=0.6, facecolors=colors[i])

    if group_labels is not None:
        ax.legend(group_labels)

    if plot_scatter:
        # Add scattered pts
        xs = np.empty(0)
        for i in range(N_it):
            xs = np.append(xs, i + 1 + np.random.normal(0, 0.04, N_rand))
        for i in range(N_groups):
            print(xs.shape, energy_progressions_by_group[i].shape)
            ax.scatter(xs, energy_progressions_by_group[i].flatten(), c=colors[i], alpha=0.5, s=5)

    plt.show()




def boxplot_multiple_energies(energies_per_group, x_label, y_label, group_labels=None, plot_scatter=True):

    fig, ax = plt.subplots()
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # plt.xticks(rotation=60)
    if group_labels is not None:
        ax.set_xticklabels(group_labels)

    ax.boxplot(energies_per_group.T, showfliers=False)

    if plot_scatter:
        # Add scattered pts
        xs = np.empty(0)
        for i in range(len(energies_per_group)):
            # for j in range(N_random):
            xs = np.append(xs, i + 1 + np.random.normal(0, 0.06, energies_per_group.shape[1]))

        ax.scatter(xs, energies_per_group.flatten())

    plt.show()