# from pygel3d import hmesh
# import polyscope as ps
import numpy as np
# import os
import networkx as nx

# Will require you to install CGAL (https://github.com/CGAL/cgal-swig-bindings)
# from CGAL.CGAL_Kernel import Point_3
# from CGAL.CGAL_Kernel import Triangle_3
# from CGAL.CGAL_AABB_tree import AABB_tree_Triangle_3_soup

# If CGAL is unavailable, try projection with trimesh:
import trimesh


def compute_barycentric_coordinates(face_vertices, p):
    a, b, c = face_vertices
    v0 = b - a
    v1 = c - a
    v2 = p - a
    d00 = np.dot(v0, v0)
    d01 = np.dot(v0, v1)
    d11 = np.dot(v1, v1)
    d20 = np.dot(v2, v0)
    d21 = np.dot(v2, v1)
    denom = d00 * d11 - d01 * d01
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1 - v - w

    return np.array([u,v,w])


# Comment this method if you want to use trimesh instead of CGAL
# def closest_pts(V, F, data_points):
#     triangles = []
#     # Create CGAL AABB tree
#     for face in F:
#         p0, p1, p2 = V[face]
#         triangles.append(
#             Triangle_3(
#                 Point_3(p0[0], p0[1], p0[2]),
#                 Point_3(p1[0], p1[1], p1[2]),
#                 Point_3(p2[0], p2[1], p2[2])
#                 ))

#     tree = AABB_tree_Triangle_3_soup(triangles)

#     projections = np.empty((0,3))

#     barycentric_coords = np.empty((0, 3), dtype=int)
#     triangle_ids = np.empty((0, 1), dtype=int)

#     for p in data_points:
#         pt, faceID = tree.closest_point_and_primitive(Point_3(p[0], p[1], p[2]))
#         pt = np.array([pt.x(), pt.y(), pt.z()])
#         coords_pt = compute_barycentric_coordinates(V[F[faceID]], pt)
#         projections = np.row_stack([projections, pt])

#         barycentric_coords = np.row_stack([barycentric_coords, coords_pt])
#         triangle_ids = np.append(triangle_ids, faceID)

#     return projections, triangle_ids, barycentric_coords

# Uncomment this method if you want to use trimesh instead of CGAL
def closest_pts(V, F, data_points):
    mesh = trimesh.Trimesh(vertices=V, faces = F)
    closest_pts, distances, triangle_ids = mesh.nearest.on_surface(data_points)

    # Compute and store barycentric coordinates of every data points projection on the given face
    barycentric_coordinates = []

    for pt, f_id in zip(closest_pts, triangle_ids):
        b_coords = compute_barycentric_coordinates(V[F[f_id]], pt)
        barycentric_coordinates.append(b_coords)

    return closest_pts, triangle_ids, np.array(barycentric_coordinates)


def mesh_dual_graph(mesh):
    edges = np.empty((0,2), dtype=int)
    edges_weight = []
    corresponding_he_id = []

    already_visited_he = set()

    G = nx.Graph()

    # face_pos = np.empty((0,3))
    edges_graph = np.empty((0,2), dtype=int)
    weights = []


    for hid in mesh.halfedges():
        if mesh.opposite_halfedge(hid) in already_visited_he:
            # print("passing")
            continue
        # If edge is at a boundary, continue
        if mesh.is_halfedge_at_boundary(hid):
            continue
        # Add a new edge between the 2 faces
        fA = mesh.incident_face(hid)
        fB = mesh.incident_face(mesh.opposite_halfedge(hid))

        pA = np.array(mesh.centre(fA))
        pB = np.array(mesh.centre(fB))

        edges_graph = np.row_stack([edges_graph, [fA, fB]])

        edges = np.row_stack([edges, np.array([fA, fB])])
        w = np.linalg.norm(pA - pB)
        edges_weight.append(w)
        corresponding_he_id.append(hid)
        already_visited_he.add(hid)

        G.add_edge(fA, fB, weight=w)

        weights.append(w)


    # import polyscope as ps
    # ps_dual = ps.register_curve_network("dual graph", face_pos, edges_graph)
    # ps_dual.add_scalar_quantity("weight", np.array(weights), defined_on='edges')

    # ps.show()
    return G

def mesh_primal_graph(mesh):
    already_visited_he = set()

    G = nx.Graph()

    for hid in mesh.halfedges():
        if mesh.opposite_halfedge(hid) in already_visited_he:
            continue
        # Add a new edge between the 2 vertices
        vA = mesh.incident_vertex(hid)
        vB = mesh.incident_vertex(mesh.opposite_halfedge(hid))

        pA = mesh.positions()[vA]
        pB = mesh.positions()[vB]

        w = np.linalg.norm(pA - pB)
        already_visited_he.add(hid)

        G.add_edge(vA, vB, weight=w)

    return G


def halfedge_path_from_face_path(mesh, face_path):
    he_path = []
    for i in range(len(face_path) - 1):
        fA = face_path[i]
        fB = face_path[i+1]

        he_i = None

        for he in mesh.circulate_face(fA, mode='h'):
            if mesh.incident_face(mesh.opposite_halfedge(he)) == fB:
                he_i = he
                break

        if he_i == None:
            print("error")
            return
        
        he_path.append(he_i)

    return np.array(he_path, dtype=int)

def path_length(mesh, face_path):
    d = 0
    for i in range(len(face_path) - 1):
        d += np.linalg.norm(
            np.array(mesh.centre(face_path[i+1]))
            - np.array(mesh.centre(face_path[i])))
    return d