import bpy
import bmesh
import numpy as np
import time
import sys
import math
import os
from mathutils import Matrix, Vector, Euler
import csv
import json
import argparse
import re
import shutil
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))


from figure_rendering.blender_load_results import DEFAULT_VIEW_LAYER_NAME, OCCLUDER_VIEW_LAYER_NAME
from utils.paths import *
from figure_rendering.blender_utils import *


def render(output_path, camera, animation=False, resolution=1080, fps=24, rotation_start=0, rotation_end=360, delete_frames=True):
    # output_path += '/'
    bpy.data.scenes['Scene'].render.filepath = output_path
    bpy.data.scenes['Scene'].camera = camera

    bpy.data.scenes['Scene'].render.resolution_x = int(bpy.data.scenes['Scene'].render.resolution_x * resolution / bpy.data.scenes['Scene'].render.resolution_y)
    bpy.data.scenes['Scene'].render.resolution_y = resolution

    initial_frame_end = bpy.data.scenes['Scene'].frame_end
    initial_frame_start = bpy.data.scenes['Scene'].frame_start

    if animation:
        bpy.data.scenes['Scene'].render.filepath += '/frame'
        output_video_path = os.path.join(output_path, os.pardir, os.path.basename(output_path) + '.mp4')

        # Adjust start/end frames
        frames_per_degree = (initial_frame_end - initial_frame_start) / 360
        frame_start = 0
        if rotation_start != 0:
            frame_start = max(1, math.floor(rotation_start * frames_per_degree))
            print("starting at frame", frame_start)
            bpy.data.scenes['Scene'].frame_start = frame_start
        if rotation_end != 360:
            print(rotation_end, frames_per_degree)
            frame_end = min(initial_frame_end, math.ceil(rotation_end * frames_per_degree))
            print("ending at frame", frame_end)
            bpy.data.scenes['Scene'].frame_end = frame_end


    if fps < bpy.data.scenes['Scene'].render.fps:
        step = bpy.data.scenes['Scene'].render.fps // fps
        bpy.data.scenes['Scene'].frame_step = step

    bpy.ops.render.render(write_still = True, animation=animation)

    # Put back values
    bpy.data.scenes['Scene'].frame_end = initial_frame_end
    bpy.data.scenes['Scene'].frame_start = initial_frame_start

    if animation:
        command = f"ffmpeg -f lavfi -i color=c=white:s={bpy.data.scenes['Scene'].render.resolution_x}x{bpy.data.scenes['Scene'].render.resolution_y}:r={fps} -pattern_type glob -framerate {fps} -i '{output_path}/*.png' -vcodec libx264 -filter_complex '[0:v][1:v]overlay=shortest=1,format=yuv420p[out]' -map '[out]' -r 24 {output_video_path}"
        print(command)
        stream = os.popen(command).read()

        if delete_frames:
            shutil.rmtree(output_path)



def hide_all_in_collection(col):
    for ob in col.objects:
        ob.hide_render = True

def show_sketch(col, sketch_name, hide_ignored):
    # - Render sketch + occluder mesh
    curve_sketch_objs = [obj for obj in col.objects if obj.type == 'CURVE']
    if len(curve_sketch_objs) > 0:
        for ob in curve_sketch_objs:
            if hide_ignored and 'ignored' in ob.name:
                ob.hide_render = True
            else:
                ob.hide_render = False
    else:
        sketch_obj = col.objects.get(f'{sketch_name}_sketch')
        if sketch_obj:
            sketch_obj.hide_render = False

def render_sketch(
    sketch_collection,
    camera,
    output_file_name,
    sketches_out_path, proxies_out_path, results_out_path,
    turntables,
    resolution,
    fps,
    rotation_start=0,
    rotation_end=360,
    result_material=None,
    render_pattern=None,
    overlay_sketch=False,
    hide_ignored=True,
    delete_frames=True
    ):

    if render_pattern is None:
        render_pattern = '.+'

    sketch_collection.hide_render = False
            
    sketch_name = sketch_collection.name
    default_collection = sketch_collection.children[f'{sketch_name}_obj']
    if sketch_collection.children.get(f'{sketch_name}_occl'):
        occluder_collection = sketch_collection.children[f'{sketch_name}_occl']
    else:
        occluder_collection = None

    default_collection.hide_render = False

    hide_all_in_collection(default_collection)

    if occluder_collection is not None:
        occluder_collection.hide_render = True

    if re.search(render_pattern, 'sketch'):
        bpy.context.scene.use_nodes = True # activate compositing

        # Activate the occluder view layer
        if bpy.context.scene.view_layers.get(OCCLUDER_VIEW_LAYER_NAME):
            bpy.context.scene.view_layers[OCCLUDER_VIEW_LAYER_NAME].use = True

        # - Render sketch + occluder mesh
        show_sketch(default_collection, sketch_name, hide_ignored)

        if occluder_collection is not None:
            occluder_collection.hide_render = False
        
        render(
            os.path.join(sketches_out_path, f"{output_file_name}_sketch"), 
            camera, 
            animation=turntables, 
            resolution=resolution, 
            fps=fps,
            rotation_start=rotation_start,
            rotation_end=rotation_end,
            delete_frames=delete_frames) 
        
        if occluder_collection is not None:
            occluder_collection.hide_render = True

        bpy.context.scene.use_nodes = False # deactivate compositing
        

    if re.match(render_pattern, 'proxy'):
        # Activate only the necessary view layers
        if bpy.context.scene.view_layers.get(OCCLUDER_VIEW_LAYER_NAME):
            bpy.context.scene.view_layers[OCCLUDER_VIEW_LAYER_NAME].use = False

        # - Render proxy
        hide_all_in_collection(default_collection)

        # All "results" objects
        proxy_objects = [ob for ob in default_collection.objects if re.match('.+_proxy', ob.name)]

        for idx, ob in enumerate(proxy_objects):
            out_file_suffix = f"_{idx}" if len(proxy_objects) > 1 else ""

        # if bpy.data.objects.get(f'{sketch_name}_proxy'):
            ob.hide_render = False
            render(
                os.path.join(proxies_out_path, f"{output_file_name}_proxy{out_file_suffix}"), 
                camera, 
                animation=turntables, 
                resolution=resolution, 
                fps=fps,
                rotation_start=rotation_start,
                rotation_end=rotation_end,
                delete_frames=delete_frames) 

            ob.hide_render = True


    if re.match(render_pattern, 'result'):
            # Activate only the necessary view layers
        if bpy.context.scene.view_layers.get(OCCLUDER_VIEW_LAYER_NAME):
            bpy.context.scene.view_layers[OCCLUDER_VIEW_LAYER_NAME].use = False

        hide_all_in_collection(default_collection)

        if overlay_sketch:
            show_sketch(default_collection, sketch_name, hide_ignored)

        # All "results" objects
        res_objects = [ob for ob in default_collection.objects if re.match('.+_result', ob.name)]

        for idx, ob in enumerate(res_objects):
            # if bpy.data.objects.get(f'{sketch_name}_result'):
            ob.hide_render = False

            out_file_suffix = f"_{idx}" if len(res_objects) > 1 else ""

            if result_material is not None:
                if bpy.data.materials.get(result_material):
                    ob.active_material = bpy.data.materials[result_material]
                elif result_material == 'default':
                    print(f"Using default material to render {ob.active_material}")

                render(
                    os.path.join(results_out_path, f"{output_file_name}_result{out_file_suffix}_{result_material}"), 
                    camera, 
                    animation=turntables, 
                    resolution=resolution, 
                    fps=fps,
                    rotation_start=rotation_start,
                    rotation_end=rotation_end,
                    delete_frames=delete_frames) 

            else:
                # Render with 2 materials (segmentation rainbow and shiny)
                # - Render result (material 1)

                ob.hide_render = False
                ob.active_material = bpy.data.materials['rainbow-labels']

                render(
                    os.path.join(results_out_path, f"{output_file_name}_result{out_file_suffix}_labels"), 
                    camera, 
                    animation=turntables, 
                    resolution=resolution, 
                    fps=fps,
                    rotation_start=rotation_start,
                    rotation_end=rotation_end,
                    delete_frames=delete_frames) 

                # - Render result (material 2)
                ob.active_material = bpy.data.materials['shiny']
                render(
                    os.path.join(results_out_path, f"{output_file_name}_result{out_file_suffix}"), 
                    camera, 
                    animation=turntables, 
                    resolution=resolution, 
                    fps=fps,
                    rotation_start=rotation_start,
                    rotation_end=rotation_end,
                    delete_frames=delete_frames) 

            ob.hide_render = True



if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--blend-file', help='Name of the blend file', type=str, default=None, required=True)
    parser.add_argument('--out-folder', help='Name of the output folder', type=str, default=None, required=False)
    parser.add_argument('--material', help='Name of the material to use for the result mesh', type=str, default=None, required=False)
    parser.add_argument('--show-ignored', help='Show ignored strokes', dest='hide_ignored', action='store_false', default=True)
    parser.add_argument('--overlay-sketch', help='Overlay sketch', dest='overlay_sketch', action='store_true', default=False)
    parser.add_argument('-s', '--sketch', help='Pattern to select sketch(es)', type=str, default=".+")
    parser.add_argument('--turntables', help='Render turntable animations', dest='turntables', action='store_true', default=False)
    parser.add_argument('--keep-frames', help='Do not delete frames from animation rendering', dest='delete_frames', action='store_false', default=True)
    parser.add_argument('-r', '--res', help='Render turntable animations', dest='resolution', type=int, default=1080)
    parser.add_argument('--fps', help='FPS', dest='fps', type=int, default=24)
    parser.add_argument('--rot', help='How many degrees of rotation (default = 360, full turn)', dest='rotation', type=int, default=360)
    parser.add_argument('--rot-start', help='Angle of start of rotation (degrees, default = 0)', dest='rotation_start', type=int, default=0)
    parser.add_argument('--render-only', help='Render only objects that match this regex pattern', dest='render_pattern', default='.+')
    parser.add_argument('--camera', help='Index of the camera to use for rendering', dest='camera_idx', default=None, type=int)


    # args, unknown = parser.parse_known_args()

    # get the args passed to blender after "--", all of which are ignored by
    # blender so scripts may receive their own arguments
    argv = sys.argv
    if "--" not in argv:
        argv = []  # as if no args are passed
    else:
        argv = argv[argv.index("--") + 1:]  # get all args after "--"

    args = parser.parse_args(argv)

    blend_file_path = os.path.join(BLENDER_FILES_PATH, f"{args.blend_file}.blend")

    base_path = RENDERS_PATH
    if args.turntables:
        base_path = TURNTABLES_PATH

    if args.out_folder is None:
        out_path = os.path.join(base_path, args.blend_file)
    else:
        out_path = os.path.join(base_path, args.out_folder)

    # if args.turntables:
    #     out_path = os.path.join(out_path, "tmp/")
    

    bpy.ops.wm.open_mainfile(filepath=blend_file_path)

    # For each top level collection (= each sketch to render), Ignoring the default collection
    all_sketch_collections = [c for c in bpy.data.scenes['Scene'].collection.children if len(c.objects) > 0 and c.name != 'Collection']

    print("Rendering collections:")
    print(all_sketch_collections)


    # Activate the default view layer only
    if bpy.context.scene.view_layers.get(DEFAULT_VIEW_LAYER_NAME):
        # Deactivate all others
        for l in bpy.context.scene.view_layers:
            l.use = False

        bpy.context.scene.view_layers[DEFAULT_VIEW_LAYER_NAME].use = True
        bpy.context.scene.use_nodes = False # deactivate compositing by default
        

    for sketch_collection in all_sketch_collections:
        sketch_name = sketch_collection.name
        if re.search(args.sketch, sketch_name):
            # - Hide all other collections
            for c in all_sketch_collections:
                c.hide_render = True

            cameras = [obj for obj in sketch_collection.objects if obj.type == 'CAMERA']

            if args.camera_idx is not None and args.camera_idx < len(cameras):
                print(f"Selected camera {cameras[args.camera_idx].name} for rendering.")
                cameras = [cameras[args.camera_idx]]

            # default_collection = sketch_collection.children[f'{sketch_name}_obj']
            # occluder_collection = sketch_collection.children[f'{sketch_name}_occl']

            # default_collection.hide_render = False

            # - For each camera
            for idx, camera in enumerate(cameras):
                # For turntables 1 camera is enough
                if args.turntables and idx > 0:
                    break

                render_sketch(
                    sketch_collection,
                    camera,
                    f"{sketch_name}_{idx}",
                    os.path.join(out_path, "sketches"),
                    os.path.join(out_path, "proxies"),
                    os.path.join(out_path, "results"),
                    turntables=args.turntables,
                    resolution=args.resolution,
                    fps=args.fps,
                    rotation_start=args.rotation_start,
                    rotation_end=args.rotation,
                    result_material=args.material,
                    render_pattern=args.render_pattern,
                    hide_ignored=args.hide_ignored,
                    overlay_sketch=args.overlay_sketch,
                    delete_frames=args.delete_frames
                )