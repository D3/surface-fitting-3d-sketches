import bpy
import bmesh
import numpy as np
import os
from mathutils import Matrix, Vector, Euler
from pathlib import Path


def load_mat(mat_file, mat_name):
    # path = os.path.join(Path(__file__).absolute().parent, "materials.blend"+ "\\Material\\")
    bpy.ops.wm.append(filename=mat_name, directory=mat_file + "\\Material\\")
    mat = bpy.data.materials.get(mat_name)
    return mat

def new_emissive_mat(mat_name, rgba):
    mat = bpy.data.materials.new(name=mat_name)
    mat.use_nodes = True
    tree = mat.node_tree

    ENode = tree.nodes.new('ShaderNodeEmission')
    ENode.inputs['Color'].default_value = rgba

    tree.links.new(ENode.outputs['Emission'], tree.nodes["Material Output"].inputs['Surface'])
    return mat


def create_camera(name, position, rotation):
    bpy.ops.object.camera_add(location=position)
    bpy.context.object.name = name
    # lookAt(bpy.context.object, look_at)
    bpy.context.object.rotation_euler = Euler(rotation, 'XYZ')

    return bpy.context.object


def invisibleGround(location = (0,0,0), groundSize = 20, shadowBrightness = 0.7, col=None):
    # initialize a ground for shadow
    bpy.context.scene.cycles.film_transparent = True
    bpy.ops.mesh.primitive_plane_add(location = location, size = groundSize)
    bpy.context.object.cycles.is_shadow_catcher = True

    # # set material
    ground = bpy.context.object
    mat = bpy.data.materials.new('GroundMaterial')
    ground.data.materials.append(mat)
    mat.use_nodes = True
    tree = mat.node_tree
    tree.nodes["Principled BSDF"].inputs['Transmission'].default_value = shadowBrightness

    if col is not None:
        set_collection(ground, col)

def lookAt(camera, point):
    loc = Vector(point)
    direction = loc - camera.location
    rotQuat = direction.to_track_quat('-Z', 'Y')
    camera.rotation_euler = rotQuat.to_euler()


def duplicate_object(ob, new_object_name, collection=None):
    new_ob = ob.copy()
    new_ob.data = ob.data.copy()

    new_ob.name = new_object_name

    if collection is None:
        collection = ob.users_collection[0]
        
    collection.objects.link(new_ob)

    return new_ob

def cameras_in_collection(collection):
    return [obj for obj in collection.all_objects if obj.type == 'CAMERA']

def hide_col(collection):
    collection.hide_viewport = True
    collection.hide_render = True

def show(collection):
    collection.hide_viewport = False
    collection.hide_render = False

def disable_in_view_layer(parent_collection_name, collection, view_layer_name):
    for layer_collection in bpy.context.scene.view_layers[view_layer_name].layer_collection.children[parent_collection_name].children:
        if layer_collection.collection == collection:
            layer_collection.exclude = True

def remove_data_points(V, E, ignored_pts_idx):
    mask_V = np.ones(len(V), dtype=bool)
    mask_V[ignored_pts_idx] = False

    mask_E = np.all(mask_V[E], axis = 1)
    E_kept = E[mask_E]
    vertices_kept = np.unique(E[mask_E])

    mask_V[:] = False
    mask_V[vertices_kept] = True

    V = V[mask_V]

    # Correct edges indexing
    E = np.searchsorted(vertices_kept, E_kept)

    return V, E, mask_V, mask_E

def separate_sketch_part(sketch_ob, indices_to_remove, part_ob_name):
    V = get_mesh_coordinates(sketch_ob)
    E = get_mesh_edges(sketch_ob)

    # - Split part
    new_V, new_E, _, _ = remove_data_points(V, E, indices_to_remove)
    part_ob = add_sketch(new_V, new_E, part_ob_name, collection=sketch_ob.users_collection[0].name)

    return part_ob

def add_mesh(V, F, name, collection='Collection'):
    mesh = bpy.data.meshes.new(name)
    new_obj = bpy.data.objects.new(name, mesh)
    col = bpy.data.collections.get(collection)
    if col is None:
        col = bpy.data.collections.new(collection)
    col.objects.link(new_obj)
    V = list(map(tuple, V))
    F = list(map(tuple, F))
    mesh.from_pydata(V, [], F)

    return new_obj

def set_collection(ob, new_collection):
    old_coll = ob.users_collection #list of all collection the obj is in

    new_collection.objects.link(ob) #link obj to scene

    for col in old_coll: #unlink from all  precedent obj collections
        col.objects.unlink(ob)

def add_sketch(V, E, name, collection='Collection'):
    mesh = bpy.data.meshes.new(name)
    new_obj = bpy.data.objects.new(name, mesh)
    col = bpy.data.collections.get(collection)
    if col is None:
        col = bpy.data.collections.new(collection)
    col.objects.link(new_obj)
    mesh.from_pydata(V, E.tolist(), [])

    return new_obj

def sketch_mesh_to_tubes(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='CURVE')

    ob = bpy.context.view_layer.objects.active
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    if mat is None:
        mat = bpy.data.materials.get("stroke-mat") or new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')

def replace_geometry(ob, V, E, F):
    ob.data.clear_geometry()
    if len(E) > 0:
        ob.data.from_pydata(V, E.tolist(), [])
    elif len(F) > 0:
        ob.data.from_pydata(V, [], F.tolist())

def get_mesh_coordinates(ob):
    count = len(ob.data.vertices)
    V = np.empty(count * 3)
    ob.data.vertices.foreach_get("co", V)
    V.shape = (count, 3)
    return V

def get_mesh_faces(ob):
    count = len(ob.data.vertices)
    F = np.empty((0, 3), dtype=int)
    for p in ob.data.polygons:
        F = np.row_stack([F, p.vertices])
    return F

def get_mesh_edges(ob):
    count = len(ob.data.edges)
    E = np.empty(count * 2, dtype=int)
    ob.data.edges.foreach_get("vertices", E)
    E.shape = (count, 2)
    return E

def set_mesh_coordinates(ob, V):
    ob.data.vertices.foreach_set("co", V.ravel())
    ob.data.update()


def get_selected_vertices():
    # If the current active object is not a mesh, return
    if bpy.context.active_object.type != 'MESH':
        return []

    current_mode = bpy.context.active_object.mode
    if bpy.context.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')

    ob = bpy.context.active_object
    mesh = ob.data
    selected_verts = [v.index for v in mesh.vertices if v.select]
            
    bpy.ops.object.mode_set(mode=current_mode)

    return selected_verts

def mark_selected(ob, vertices):
    # If the current active object is not a mesh, return
    if bpy.context.active_object.type != 'MESH':
        print("not a mesh, can't mark vertices")
        return

    current_mode = bpy.context.active_object.mode
    if bpy.context.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')
    mesh = ob.data
    for v in mesh.vertices:
        if v.index in vertices:
            v.select = True
        else:
            v.select = False
    bpy.ops.object.mode_set(mode=current_mode)


def set_mesh_colormap(ob, quantity_by_vertex, label, colormap='rainbow'):

    if bpy.context.active_object:
        current_mode = bpy.context.active_object.mode
        if bpy.context.mode != 'OBJECT':
            bpy.ops.object.mode_set(mode='OBJECT')

    mesh = ob.data

    color_map = mesh.vertex_colors.get(label) or mesh.vertex_colors.new(name=label)

    # Normalize values
    if np.max(quantity_by_vertex) > 0:
        vertex_colors = quantity_by_vertex / np.max(quantity_by_vertex)
    else:
        vertex_colors = quantity_by_vertex

    i = 0
    for poly in mesh.polygons:
        for idx in poly.loop_indices:
            v_idx = ob.data.loops[idx].vertex_index
            col = vertex_colors[v_idx]
            for c in range(2):
                color_map.data[i].color[c] = col
            color_map.data[i].color[3] = 1.0
            i += 1

    # Set material
    # if colormap == 'rainbow':
    #     mat = bpy.data.materials.get("colormap_rainbow")
    #     ob.active_material = mat
    # else:
    #     mat = bpy.data.materials.get("colormap_coolwarm")
    #     ob.active_material = mat

    if bpy.context.active_object:
        bpy.ops.object.mode_set(mode=current_mode)

def set_per_face_colors(ob, colors_by_face, label):

    color_layer = ob.data.vertex_colors.get(label) or ob.data.vertex_colors.new(name=label)

    for poly, color in zip(ob.data.polygons, colors_by_face):
        # for idx in poly.loop_indices:
        for i, index in enumerate(poly.vertices):
            loop_index = poly.loop_indices[i]
            color_layer.data[loop_index].color[0] = color[0]
            color_layer.data[loop_index].color[1] = color[1]
            color_layer.data[loop_index].color[2] = color[2]
            color_layer.data[loop_index].color[3] = 1 # set alpha


def set_mesh_int_property(ob, property_by_vertex, layer_name):
    mesh = ob.data
    # bm = bmesh.new()   # create an empty BMesh
    # bm.from_mesh(mesh)

    if mesh.is_editmode:
        bm = bmesh.from_edit_mesh(mesh)
    else:
        bm = bmesh.new()
        bm.from_mesh(mesh)

    layer = bm.verts.layers.int.get(layer_name) or bm.verts.layers.int.new(layer_name)

    for v_i, v in enumerate(bm.verts):
        v[layer] = property_by_vertex[v_i]

    # bm.to_mesh(mesh)
    if bm.is_wrapped:
        bmesh.update_edit_mesh(mesh, False, False)
    else:
        bm.to_mesh(mesh)
        mesh.update()

def set_mesh_float_property(ob, property_by_vertex, layer_name):
    mesh = ob.data
    # bm = bmesh.new()   # create an empty BMesh
    # bm.from_mesh(mesh)
    if mesh.is_editmode:
        bm = bmesh.from_edit_mesh(mesh)
    else:
        bm = bmesh.new()
        bm.from_mesh(mesh)

    layer = bm.verts.layers.float.get(layer_name) or bm.verts.layers.float.new(layer_name)

    for v_i, v in enumerate(bm.verts):
        v[layer] = property_by_vertex[v_i]

    # bm.to_mesh(mesh)
    if bm.is_wrapped:
        bmesh.update_edit_mesh(mesh, False, False)
    else:
        bm.to_mesh(mesh)
        mesh.update()

def get_mesh_float_property(ob, layer_name):
    mesh = ob.data
    bm = bmesh.new()   # create an empty BMesh
    bm.from_mesh(mesh)
    layer = bm.verts.layers.float.get(layer_name)

    float_layer_keys = [layer.name for layer in bm.verts.layers.float.values()]

    if layer is None:
        print("Layer does not exist.")
        return None
    props = []
    for v_i, v in enumerate(bm.verts):
        props.append(v[layer])

    return props

def mark_sharp(ob, vertex_is_sharp):
    # Make sure all mesh elements are deselected first
    bpy.context.view_layer.objects.active = ob
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

    for e in ob.data.edges:
        if np.all(vertex_is_sharp[e.vertices]):
            # Mark as sharp
            e.select = True
        else:
            e.select = False
    
    bpy.ops.object.mode_set(mode='EDIT')

    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.mark_sharp(clear=True)
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.mark_sharp()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

    # Add modifier to render sharp edges correctly
    bpy.ops.object.modifier_add(type='EDGE_SPLIT')
    bpy.context.object.modifiers["EdgeSplit"].use_edge_angle = False

    # Shade smooth
    ob.select_set(True)
    bpy.ops.object.shade_smooth()
    ob.select_set(False)


def mark_seam(ob, vertex_is_seam):
    # Make sure all mesh elements are deselected first
    bpy.context.view_layer.objects.active = ob
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

    # Mark segmentation seams as UV seams
    for e in ob.data.edges:
        if np.all(vertex_is_seam[e.vertices]):
            # Mark as sharp
            e.select = True
        else:
            e.select = False

    bpy.ops.object.mode_set(mode='EDIT')

    bpy.ops.mesh.select_all(action='INVERT')
    # bpy.ops.mesh.mark_sharp(clear=True)
    bpy.ops.mesh.mark_seam(clear=True)
    bpy.ops.mesh.select_all(action='INVERT')
    # bpy.ops.mesh.mark_sharp()
    bpy.ops.mesh.mark_seam()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def visualize_vertices_set(mesh_ob, vertices_set, set_name, rgba_color, radius):
    # Set mode to object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    # initialize a primitive sphere
    bpy.ops.mesh.primitive_uv_sphere_add(radius = 1.0)
    sphere = bpy.context.object
    set_collection(sphere, mesh_ob.users_collection[0])
    sphere.hide_set(True)
    bpy.ops.object.shade_smooth()
    particle_mat = bpy.data.materials.get(f'particle_mat_{rgba_color}')
    if particle_mat is None:
        particle_mat = new_emissive_mat(f'particle_mat_{rgba_color}', rgba_color)
    sphere.data.materials.append(particle_mat)
    sphere.active_material = particle_mat

    # init particle system
    bpy.context.view_layer.objects.active = mesh_ob
    bpy.ops.object.particle_system_add()
    particle_system = bpy.context.object.particle_systems[-1]
    particle_system.name = set_name
    settings = particle_system.settings
    settings.frame_start = 0
    settings.frame_end = 0
    settings.render_type = 'OBJECT'
    settings.instance_object = sphere
    settings.particle_size = radius
    settings.physics_type = 'NO'
    settings.use_emit_random = False

    settings.emit_from = 'VERT'
    settings.count = len(vertices_set)

    # Restrict particles to the vertices set
    # - Create a vertex group
    bpy.ops.object.vertex_group_add()
    vertex_group = bpy.context.object.vertex_groups[-1]
    vertex_group.name = set_name
    vertex_group.add(vertices_set, 1.0, 'REPLACE')

    # - Assign the density control to the vertex group
    bpy.context.object.particle_systems[-1].vertex_group_density = vertex_group.name

def visualize_graph(name, nodes, edges, edge_weights, col_name="Collection", edge_thickness=0.01):
    if bpy.context.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')
    # Create new mesh object
    mesh = bpy.data.meshes.new(name)
    obj = bpy.data.objects.new(mesh.name, mesh)
    col = bpy.data.collections.get(col_name)
    col.objects.link(obj)

    # Add edge weight as a vertex group weight
    vg = obj.vertex_groups.new(name="edge_weight")

    # Add all nodes and edges to the mesh
    mesh_verts = []
    mesh_edges = []

    # Weights must be normalized (and not zero!)
    max_w = np.max(edge_weights)
    min_w = np.min(edge_weights)

    eps = 0.01 # min weight

    ratio = (1 - eps) / (max_w - min_w)
    normalized_weights = (edge_weights - min_w) * ratio + eps 

    # Vertices shared by 2 edges must be duplicated, to allow for per-edge color
    for e, w in zip(edges, normalized_weights):
        # Add vertices
        for vi in e:
            v = nodes[vi]
            mesh_verts.append((v[0], v[1], v[2]))
        edge_indices = [len(mesh_verts) - 2, len(mesh_verts) - 1]
        # Add edge
        mesh_edges.append(tuple(edge_indices))

    mesh.from_pydata(mesh_verts, mesh_edges, [])
    
    # Add vertex group weights (edge weights)
    vi = 0
    for w in normalized_weights:
        edge_indices = [vi, vi + 1]
        # Add vertex weight
        vg.add(edge_indices, w, 'REPLACE')
        vi += 2

    # Also store weight in float vertex layer, to inspect it
    bm = bmesh.new()   # create an empty BMesh
    bm.from_mesh(mesh)
    weight_layer = bm.verts.layers.float.new('edge_weight')

    for v_i, v in enumerate(bm.verts):
        v[weight_layer] = edge_weights[int(v_i/2)]
        
    bm.to_mesh(mesh)
    
    # Add skin modifier
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.modifier_add(type='SKIN')
    
    # - Set all vertices as root
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.object.skin_root_mark()
    
    # - Set correct radius
    skin_radius = edge_thickness * np.linalg.norm(np.max(nodes, axis=0) - np.min(nodes, axis = 0))
    bpy.ops.transform.skin_resize(value=(skin_radius, skin_radius, skin_radius))
    bpy.ops.mesh.select_all(action='DESELECT')
    
    
    bpy.ops.object.mode_set(mode='OBJECT')
    
    # Apply modifier
    bpy.ops.object.modifier_apply(modifier="Skin")
    
    # From vertex group to vertex colors
    vcol = obj.data.vertex_colors.new(name="float_value");
    for poly in obj.data.polygons:
        for loop in poly.loop_indices:
            vertindex = obj.data.loops[loop].vertex_index;   
            try:     
                weight = vg.weight(vertindex);
                vcol.data[loop].color = (weight, weight, weight, 1.0);
            except RuntimeError:
                vcol.data[loop].color = (0.0, 1, 0, 1.0);
                print("vertex not in group")


    # Apply color ramp material
    mat = bpy.data.materials.get("colormap_coolwarm")
    obj.active_material = mat

    # Clear out select
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = None
