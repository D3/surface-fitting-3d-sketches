import bpy
from mathutils import Euler
from math import pi

def setup_scene(envmap_path=None):

    # Init blender
    bpy.ops.wm.read_homefile()
    # render engine
    # bpy.context.scene.render.engine = 'CYCLES'
    bpy.context.scene.render.engine = 'BLENDER_EEVEE'
    bpy.context.scene.render.resolution_x = 1920 
    bpy.context.scene.render.resolution_y = 1080 
    # bpy.context.scene.cycles.film_transparent = True
    bpy.context.scene.render.film_transparent = True
    bpy.context.scene.cycles.samples = 128 
    bpy.context.scene.cycles.max_bounces = 6
    # bpy.context.scene.cycles.film_exposure = 1.5
    bpy.context.scene.view_settings.exposure = 1
    bpy.context.scene.view_settings.gamma = 0.5


    # bpy.data.scenes[0].view_layers['View Layer']['cycles']['use_denoising'] = 1

    # Environment lighting
    if envmap_path is not None:
        bpy.context.scene.world.use_nodes = True
        node_tex = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeTexEnvironment")
        node_tex.image = bpy.data.images.load(envmap_path)
        node_tree = bpy.context.scene.world.node_tree
        # Tweak saturation
        node_hsv = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeHueSaturation")
        node_hsv.inputs[1].default_value = 0.2 # Set saturation
        node_tree.links.new(node_tex.outputs['Color'], node_hsv.inputs['Color'])
        node_tree.links.new(node_hsv.outputs['Color'], node_tree.nodes['Background'].inputs['Color'])

    bpy.ops.object.select_all(action = 'SELECT')
    bpy.ops.object.delete()

def setup_occluding_compositing(
    # scene,
    objects_layer_name='Foreground',
    occluder_layer_name='Occluder',
    depth_offset=0.01,
    occluded_opacity=0.5):
    scene = bpy.context.scene
    scene.use_nodes = True

    tree = scene.node_tree

    # 2 Render Layers
    rl_objects_node = tree.nodes["Render Layers"]
    rl_objects_node.layer = objects_layer_name
    rl_occluder_node = tree.nodes.new("CompositorNodeRLayers")
    rl_occluder_node.layer = occluder_layer_name

    # Offset depth
    offset_node = tree.nodes.new("CompositorNodeMath")
    tree.links.new(rl_occluder_node.outputs['Depth'], offset_node.inputs[0])
    offset_node.inputs[1].default_value = depth_offset

    # Compare depth from objects layer with depth from occluder layer
    comparison_node = tree.nodes.new("CompositorNodeMath")
    comparison_node.operation = 'GREATER_THAN'
    tree.links.new(offset_node.outputs['Value'], comparison_node.inputs[0])
    tree.links.new(rl_objects_node.outputs['Depth'], comparison_node.inputs[1])

    # Set object opacity value when occluded
    objects_opacity_value_node = tree.nodes.new("CompositorNodeMapRange")
    tree.links.new(comparison_node.outputs[0], objects_opacity_value_node.inputs[0])
    objects_opacity_value_node.inputs[3].default_value = occluded_opacity
    objects_opacity_value_node.use_clamp = True

    set_opacity_value_node = tree.nodes.new("CompositorNodeSetAlpha")
    tree.links.new(rl_objects_node.outputs['Image'], set_opacity_value_node.inputs['Image'])
    tree.links.new(objects_opacity_value_node.outputs[0], set_opacity_value_node.inputs['Alpha'])

    # Link to output
    tree.links.new(set_opacity_value_node.outputs[0], tree.nodes["Composite"].inputs['Image'])


def setup_turntables(nb_frames=240):
    
    scene = bpy.context.scene
    # Render parameters
    scene.frame_end = nb_frames
    scene.render.fps = 24

    # Create empty
    bpy.ops.object.empty_add(type="PLAIN_AXES")
    empty = bpy.context.object
    bpy.ops.collection.objects_remove_all()
    # add it to the default collection
    bpy.data.collections['Collection'].objects.link(empty)

    # Set keyframes for rotation
    empty.keyframe_insert(data_path="rotation_euler", frame=1)
    empty.rotation_euler = Euler((0, 0, 2 * pi), 'XYZ')
    empty.keyframe_insert(data_path="rotation_euler", frame=nb_frames + 1)

    fcurves = empty.animation_data.action.fcurves
    for fcurve in fcurves:
        for kf in fcurve.keyframe_points:
            kf.interpolation = 'LINEAR'

    # Parent all non camera objects of the scene to the empty
    for ob in bpy.data.objects:
        if ob.type != 'CAMERA' and ob.type != 'EMPTY':
            ob.parent = empty

