import bpy
import bmesh
import numpy as np
import time
import sys
import math
import os
from mathutils import Matrix, Vector, Euler
import csv
import json
import argparse
import re
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))

from utils.paths import *
from figure_rendering.blender_utils import *
from utils.mesh_properties_io import unpack_mesh_properties_blender
from utils.math import get_bounds
from utils.mirroring import remove_symmetric_data, symmetrize_mesh
from figure_rendering.blender_setup_scene import setup_scene, setup_occluding_compositing, setup_turntables


BLENDER_CAMERA_DATA_PATH = os.path.join(ROOT_FOLDER, "scripts", "figure_rendering", "ressources", "blender_cameras.csv")
ENV_MAP_FILE_PATH = os.path.join(ROOT_FOLDER, "scripts", "figure_rendering", "ressources", "interior.exr")
MATERIALS_FILE_PATH = os.path.join(ROOT_FOLDER, "scripts", "figure_rendering", "ressources", "materials.blend")

STROKES_COLOR = (0.01, 0.01, 0.01, 1)
BOUNDARY_STROKES_COLOR = (0.012286, 0.208637, 0.904661, 1)
IGNORED_STROKES_COLOR = (0.98823529, 0.17254902, 0.01176471, 1)

DEFAULT_VIEW_LAYER_NAME = 'DefaultLayer'
OCCLUDER_VIEW_LAYER_NAME = 'OccluderLayer'


# CALL WITH : /Applications/Blender-2.93.app/Contents/MacOS/Blender -b -P blender_load_results.py


def load_sketch_mesh(sketch_name, sketch_obj_file, convert_to_curves, is_symmetric, marked_ignore, marked_boundary, thickness):
    obs = []
    mats = []
    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF')
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}_sketch"
    bpy.ops.object.transform_apply() # Apply all transforms

    center, span = get_bounds(get_mesh_coordinates(ob))

    # Masks
    pt_is_ignored = np.zeros(len(ob.data.vertices), dtype=bool)
    pt_is_ignored[marked_ignore] = True
    pt_is_boundary = np.zeros(len(ob.data.vertices), dtype=bool)
    pt_is_boundary[marked_boundary] = True

    # Symmetric? Cut down the middle and apply mirror modifier
    if is_symmetric:
        # - This is because the ignored or boundary pts might be marked only on one side, so we have to mirror them
        V = get_mesh_coordinates(ob)
        E = get_mesh_edges(ob)

        V, E, _, V_mask = remove_symmetric_data(V, E)
        replace_geometry(ob, V, E, [])

        pt_is_ignored = pt_is_ignored[V_mask]
        pt_is_boundary = pt_is_boundary[V_mask]

    vg_boundary = ob.vertex_groups.new(name="is_boundary")
    vg_boundary.add(np.flatnonzero(pt_is_boundary).tolist(), 1, 'REPLACE')

    vg_ignored = ob.vertex_groups.new(name="is_ignored")
    vg_ignored.add(np.flatnonzero(pt_is_ignored).tolist(), 1, 'REPLACE')
    

    if convert_to_curves:

        # Separate ignored pts and boundary pts into their own mesh object
        # - Separate ignored pts
        if np.any(pt_is_ignored):
            ignored_ob = separate_sketch_part(ob, np.flatnonzero(np.logical_not(pt_is_ignored)), f"{sketch_name}_ignored")
            obs.append(ignored_ob)
            mats.append("ignored-stroke-mat")
        # - Separate boundary pts
        if np.any(pt_is_boundary):
            boundary_ob = separate_sketch_part(ob, np.flatnonzero(np.logical_not(pt_is_boundary)), f"{sketch_name}_boundary")
            obs.append(boundary_ob)
            mats.append("boundary-stroke-mat")
        # - Separate main part
        # if np.any(np.logical_or(pt_is_ignored, pt_is_boundary)):
        if np.any(np.logical_not(np.logical_or(pt_is_ignored, pt_is_boundary))):
            # vertex_indices_main = np.append(marked_ignore, marked_boundary).astype(int)
            vertex_indices_main = np.flatnonzero(np.logical_or(pt_is_ignored, pt_is_boundary))
            ob_main = separate_sketch_part(ob, vertex_indices_main, f"{sketch_name}_sketch")
            # Delete old one
            bpy.data.objects.remove(ob, do_unlink=True)
            ob = ob_main
            ob.name = f"{sketch_name}_sketch"
        
        obs.insert(0, ob)
        mats.insert(0, "stroke-mat")
        

        for ob, mat in zip(obs, mats):
            sketch_mesh_to_tubes(ob, load_mat(MATERIALS_FILE_PATH, mat), size=thickness)

            if is_symmetric:
                # Mirror modifier
                bpy.context.view_layer.objects.active = ob
                bpy.ops.object.modifier_add(type='MIRROR')

    else:
        # for ob, mat in zip(obs, mats):
        # sketch_mesh_to_tubes(ob, load_mat(MATERIALS_FILE_PATH, 'stroke-mat'), size=0.002)

        bpy.context.view_layer.objects.active = ob

        bpy.ops.object.modifier_add(type='SKIN')
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        
        # Clean up mesh by merging coincident vertices
        # (coincident vertices may cause trouble for the skin modifier)
        bpy.ops.mesh.remove_doubles()

        bpy.ops.mesh.select_all(action='SELECT')

        bpy.ops.object.skin_root_mark()
        bpy.ops.object.mode_set(mode='OBJECT')

        for v in ob.data.skin_vertices[0].data:
            v.radius = thickness, thickness

        bpy.context.object.modifiers["Skin"].use_smooth_shade = True
        bpy.ops.object.modifier_apply(modifier='Skin')

        ob.active_material = load_mat(MATERIALS_FILE_PATH, 'stroke-mat')

        # Set different materials for different parts
        ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "boundary-stroke-mat")) # idx 1
        ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "ignored-stroke-mat")) # idx 2

        vcol = ob.data.vertex_colors.new()
        vg_boundary = ob.vertex_groups.get("is_boundary")
        vs_boundary = set([ v.index for v in ob.data.vertices if vg_boundary.index in [ vg.group for vg in v.groups ] ])

        vg_ignored = ob.vertex_groups.get("is_ignored")
        vs_ignored = set([ v.index for v in ob.data.vertices if vg_ignored.index in [ vg.group for vg in v.groups ] ])
        for poly in ob.data.polygons:
            for loop in poly.loop_indices:
                vertindex = ob.data.loops[loop].vertex_index
                if vertindex in vs_boundary:
                    vcol.data[loop].color = BOUNDARY_STROKES_COLOR
                elif vertindex in vs_ignored:
                    vcol.data[loop].color = IGNORED_STROKES_COLOR
                else:
                    vcol.data[loop].color = STROKES_COLOR

        if is_symmetric:
            bpy.context.view_layer.objects.active = ob
            bpy.ops.object.modifier_add(type='MIRROR')

        obs.append(ob)

    return obs, center, span

def load_proxy_mesh(proxy_obj_file, symmetrize=False, shade_smooth=True):
    # Load proxy mesh file
    bpy.ops.import_scene.obj(filepath=proxy_obj_file, split_mode='OFF')
    ob = bpy.context.selected_objects[0]
    bpy.ops.object.transform_apply() # Apply all transforms

    if symmetrize:
        V = get_mesh_coordinates(ob)
        F = get_mesh_faces(ob)

        V, F, _ = symmetrize_mesh(V, F)
        replace_geometry(ob, V, [], F)


    # Set material
    ob.active_material = load_mat(MATERIALS_FILE_PATH, "proxy-mat")

    # Shade smooth
    if shade_smooth:
        bpy.ops.object.select_all(action='DESELECT')
        ob.select_set(True)
        bpy.context.view_layer.objects.active = ob
        bpy.ops.object.shade_smooth()
    bpy.ops.object.select_all(action='DESELECT')

    return ob

def load_exported_ply_result(ply_file, per_vertex_colors=False, sharp_features=True, show_wireframe=False):

    # Load mesh file
    bpy.ops.import_mesh.ply(filepath=ply_file)
    ob = bpy.context.object

    ob.rotation_euler[0] = math.radians(90) # Correct axis orientations

    mesh = ob.data

    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = ob
    ob.select_set(True)
    bpy.ops.object.transform_apply() # Apply all transforms

    sharpness_by_vertex, vertex_is_seam, label_by_poly, degree_by_poly = unpack_mesh_properties_blender(mesh)
    label_color_layer = mesh.vertex_colors['Col']
    # degree_color_layer = mesh.vertex_colors.new(name="Degree")

    if not per_vertex_colors:
        set_per_face_colors(ob, np.column_stack([label_by_poly, degree_by_poly, np.zeros(len(label_by_poly))]), label_color_layer.name)
        # set_per_face_colors(ob, degree_by_poly, degree_color_layer.name)

    if sharp_features:
        mark_sharp(ob, sharpness_by_vertex)
        mark_seam(ob, vertex_is_seam)

    # Shade smooth
    bpy.ops.object.shade_smooth()

    bpy.ops.object.select_all(action='DESELECT')

    # Set material
    if show_wireframe:
        ob.active_material = load_mat(MATERIALS_FILE_PATH, "rainbow-labels-wire")
    else:
        ob.active_material = load_mat(MATERIALS_FILE_PATH, "rainbow-labels")

    # Also add other materials
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "shiny"))
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "textured"))
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "coolwarm"))
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "model-degree"))
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "deviations"))
    ob.data.materials.append(load_mat(MATERIALS_FILE_PATH, "rainbow-labels-wire"))


    return ob
    

def generate_occluder_mesh(result_mesh, collection):

    # Duplicate result mesh to create occluder mesh 1 and 2 (one for each camera)
    # occluder = result_mesh.copy()
    occluder_name = f"{result_mesh.name}_occluder"
    occluder = duplicate_object(result_mesh, occluder_name, collection)


    # Set material of occluder meshes
    # occluder_mat = load_mat(MATERIALS_FILE_PATH, "holdout")
    occluder_mat = new_emissive_mat("white", (1,1,1,1))
    occluder.active_material = occluder_mat
    # occluder_2.active_material = occluder_mat

    # Nudge them away from each of the cameras
    # _, _, back = camera.matrix_world.to_3x3().transposed()

    # bpy.ops.object.select_all(action='DESELECT')


    # bpy.context.view_layer.objects.active = occluder
    # bpy.ops.object.transform_apply()
    # occluder.matrix_world = Matrix.Translation(-back * displacement)
    

def setup_sketch_in_blender(
    sketch_name,
    sketch_info, 
    result_meshes_path=None, 
    camera_data=None, 
    load_sketch=True,
    curves_sketch=True, 
    load_proxy=True, 
    load_occluders=True, 
    shadow_catcher=True, 
    per_vertex_colors=False,
    sharp_features=True,
    hide=True,
    smooth_proxy=True,
    curves_thickness=0.001,
    camera_view_offset=0):
    # sketch_name = sketch_info['sketch_name']
    sketch_file_path = os.path.join(ROOT_FOLDER, sketch_info['sketch_file'])
    proxy_file_path = os.path.join(ROOT_FOLDER, sketch_info['proxy_file'])

    if bpy.data.collections.get(sketch_name) is not None:
        # Delete the existing collection
        collection = bpy.data.collections.get(sketch_name)

        for obj in collection.all_objects:
            bpy.data.objects.remove(obj, do_unlink=True)
            
        bpy.data.collections.remove(collection)

    # Create a collection with the name of the sketch
    collection = bpy.data.collections.new(sketch_name)
    bpy.context.scene.collection.children.link(collection)

    collection_default = bpy.data.collections.new(f"{sketch_name}_obj")
    collection.children.link(collection_default)

    if load_occluders:
        # Create a collection to hold the normal objects
        # --> Associated with default View Layer
        disable_in_view_layer(collection.name, collection_default, OCCLUDER_VIEW_LAYER_NAME)
        
        # Create a collection to hold the occluding object
        # --> Associated with Occluder View Layer
        collection_occluded = bpy.data.collections.new(f"{sketch_name}_occl")
        collection.children.link(collection_occluded)
        disable_in_view_layer(collection.name, collection_occluded, DEFAULT_VIEW_LAYER_NAME)


    # Load sketch
    if load_sketch:
        sketch_objs, center, span = load_sketch_mesh(
            sketch_name,
            sketch_file_path,
            curves_sketch,
            sketch_info['is_symmetric'],
            sketch_info['ignored_stroke_points'],
            sketch_info['on_border_stroke_points'],
            curves_thickness)
        # sketch_obj.name = f"{sketch_name}_sketch"
        # collection.objects.link(sketch_obj)
        for sketch_obj in sketch_objs:
            set_collection(sketch_obj, collection_default)

    # Load proxy mesh
    if load_proxy:
        proxy_mesh_obj = load_proxy_mesh(proxy_file_path, bool(sketch_info['is_symmetric']), shade_smooth=smooth_proxy)
        proxy_mesh_obj.name = f"{sketch_name}_proxy"
        # collection.objects.link(proxy_mesh_obj)
        set_collection(proxy_mesh_obj, collection_default)

    # Load result mesh
    if result_meshes_path is not None:
        for result_idx, result_mesh_path in enumerate(result_meshes_path):
            show_wireframe = False
            if "segmentation" in os.path.basename(result_mesh_path):
                show_wireframe = True
            mesh_object = load_exported_ply_result(result_mesh_path, per_vertex_colors, sharp_features=sharp_features, show_wireframe=show_wireframe)
            name = f"{sketch_name}_result"
            if result_idx > 0:
                name += f"_{result_idx}"
            mesh_object.name = name
            set_collection(mesh_object, collection_default)
    else:
        mesh_object = None

        # for camera in cameras_in_collection(collection):
        #     generate_occluder_mesh(sketch_name, camera)

    # Set up 2 cameras based on sketch info parameters
    # camera_rel_offset =  sketch_info['display_rel_offset']

    # Occluder mesh
    if load_occluders:
        generate_occluder_mesh(mesh_object, collection_occluded)

    # Position camera
    if camera_data is not None:
        for i in [1,2]:
            cam1_loc = eval(camera_data[f'cam{i}_loc'])
            cam1_rot = eval(camera_data[f'cam{i}_rot'])

            if camera_view_offset > 0:
                cam1_loc = np.array(cam1_loc)
                cam1_loc *= (1 + camera_view_offset)


            cam1_obj = create_camera(f"{sketch_name}_camera{i}", cam1_loc, cam1_rot)
            set_collection(cam1_obj, collection)

            # if camera_view_offset > 0:
            #     # Nudge it away from the center along view direction
            #     _, _, back = cam1_obj.matrix_world.to_3x3().transposed()

            #     t = Matrix.Translation(back * camera_view_offset)
            #     cam1_obj.matrix_world = t @ cam1_obj.matrix_world

            # return

            # if load_occluders and mesh_object is not None:
            #     generate_occluder_mesh(mesh_object, collection, cam1_obj)

            # cam2_obj = create_camera(f"{sketch_name}_camera2", cam2_loc, cam2_rot)
            # set_collection(cam2_obj, collection)
            # if load_occluders and mesh_object is not None:
            #     generate_occluder_mesh(mesh_object, collection, cam2_obj)


    # Create a shadow catcher plane
    if shadow_catcher:
        invisibleGround(location=(0, 0, (center - span)[2]), col=collection)

    if hide:
        hide_col(collection)

    return

# def replace_result_mesh(sketch_name, result_mesh_path):
#     # Find correct collection
#     collection = bpy.data.collections[sketch_name]

#     mesh_object = load_exported_ply_result(result_mesh_path)
#     mesh_object.name = f"{sketch_name}_result"
#     collection.objects.link(mesh_object)

#     # Redo the occluder meshes
#     for camera in cameras_in_collection(collection):
#         generate_occluder_mesh(sketch_name, camera)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--folder', help='Name of the result mesh folder', type=str, default=None)
    parser.add_argument('-s', '--sketch', help='Pattern to select sketch(es)', type=str, default=".+")
    parser.add_argument('--sketch-file-path', help='Sketch path to override the default in metadata', type=str, default=None)
    parser.add_argument('--blend-file', help='Name of the blend file', type=str, default=None)
    parser.add_argument('-i', help='Insert in existing file without clearing', dest='insert_in_file', action='store_true', default=False)
    parser.add_argument('-m', help='Minimal mode: load only the mesh and sketch', dest='load_all', action='store_false', default=True)
    parser.add_argument('--allow-duplicates', help='Allow loading multiple times the same sketch/proxy', dest='load_duplicates', action='store_true', default=False)
    # parser.add_argument('--vertex-colors', help='Vertex colors mode', dest='vertex_colors', action='store_true', default=False)
    parser.add_argument('--smooth', help='No sharp features', dest='sharp_features', action='store_false', default=True)
    # parser.add_argument('--segment-fit', help='Load segment fit result (segmentation on proxy, surface patches)', dest='segment_fit', action='store_true', default=False)
    parser.add_argument('--no-curves', help='Do not convert strokes to extruded curves', dest='convert_to_curves', action='store_false', default=True)
    parser.add_argument('-t', '--thickness', help='Curve thickness', type=float, default=0.001)
    # parser.add_argument('--turntables', help='Prepare for turntable animations', dest='turntables', action='store_true', default=False)
    # parser.add_argument('--view-offset', help='Camera view offset', dest='view_offset', type=float, default=0.0)
    # parser.add_argument('--trimmed-proxies', help='Load trimmed proxies for open boundary sketches', dest='trimmed_proxies', action='store_true', default=False)

    # args, unknown = parser.parse_known_args()

    # get the args passed to blender after "--", all of which are ignored by
    # blender so scripts may receive their own arguments
    argv = sys.argv
    if "--" not in argv:
        argv = []  # as if no args are passed
    else:
        argv = argv[argv.index("--") + 1:]  # get all args after "--"

    args = parser.parse_args(argv)

    # Default logs folder is last modified folder
    folder_path = max([os.path.join(MESH_FOLDER, d) for d in os.listdir(MESH_FOLDER) if os.path.isdir(os.path.join(MESH_FOLDER, d))], key=os.path.getmtime)
    folder_name = os.path.basename(folder_path)
    if args.folder is not None:
        folder_name = args.folder.replace("/", ":")
    result_meshes_folder = os.path.join(MESH_FOLDER, folder_name)

    blend_file_name = folder_name

    if args.blend_file is not None:
        blend_file_name = args.blend_file

    blend_file_path = os.path.join(BLENDER_FILES_PATH, f"{blend_file_name}.blend")


    if args.insert_in_file:
        bpy.ops.wm.open_mainfile(filepath=blend_file_path)
    else:
        # Init blender
        setup_scene(ENV_MAP_FILE_PATH)

        # deactivate all default view layers (speed up rendering)
        for l in bpy.context.scene.view_layers:
            l.use = False

        # Create occluder view layer
        bpy.context.scene.view_layers.new(name=OCCLUDER_VIEW_LAYER_NAME)
        bpy.context.scene.view_layers.new(name=DEFAULT_VIEW_LAYER_NAME)

        # Set up compositing
        setup_occluding_compositing(
            objects_layer_name=DEFAULT_VIEW_LAYER_NAME,
            occluder_layer_name=OCCLUDER_VIEW_LAYER_NAME,
            depth_offset=0.02,
            occluded_opacity=0.3)




    camera_data_per_sketch = {}
    with open(BLENDER_CAMERA_DATA_PATH, mode='r', encoding='utf-8-sig') as csv_file:
        metadata = csv.DictReader(csv_file, delimiter=";")
        for row in metadata:
            camera_data_per_sketch[row['sketch_name']] = row

    loaded_sketches = set()
    loaded_proxies = set()

    # for root, dirs, files in os.walk(result_meshes_folder):
    #     print(files)
    files = []
    for item in os.listdir(result_meshes_folder):
        if os.path.isfile(os.path.join(result_meshes_folder, item)) and os.path.splitext(item)[1] == '.ply':
            files.append(item)

    for f in sorted(files):

        file_base_name, extension = os.path.splitext(f)

        if extension == '.ply' and re.search(args.sketch, file_base_name):

            with open(os.path.join(result_meshes_folder, get_parameter_file_name(f))) as json_data:
                parameter_data = json.load(json_data)

            if parameter_data['sketch_name'] in camera_data_per_sketch:
                camera_data = camera_data_per_sketch[parameter_data['sketch_name']]
            else:
                camera_data = None

            # If we want the trimmed proxies, modify the proxy file path in parameter_data
            # if args.trimmed_proxies:
            #     proxy_file_path = parameter_data['proxy_file'].replace("cleaned", "trimmed")
            #     if os.path.exists(os.path.join(ROOT_FOLDER, proxy_file_path)):
            #         print("Loading trimmed proxy", proxy_file_path)
            #         parameter_data['proxy_file'] = proxy_file_path

            # Load only the first occurence of a given sketch file or proxy file (convenient in result folders on variations of the same sketch)
            if args.sketch_file_path is not None:
                sketch_file = args.sketch_file_path
                parameter_data['sketch_file'] = sketch_file
            load_sketch = args.load_duplicates or (parameter_data['sketch_file'] not in loaded_sketches)
            load_proxy = args.load_duplicates or (parameter_data['proxy_file'] not in loaded_proxies)

            loaded_sketches.add(parameter_data['sketch_file'])
            loaded_proxies.add(parameter_data['proxy_file'])

            # if args.segment_fit:
            #     result_paths = [
            #         os.path.join(result_meshes_folder, f),
            #         os.path.join(result_meshes_folder, file_base_name, f"{file_base_name}_models.ply"),
            #         os.path.join(result_meshes_folder, file_base_name, f"{file_base_name}_segmentation.ply")
            #     ]

            # else:
            result_paths = [os.path.join(result_meshes_folder, f)]

            # camera_view_offset = args.view_offset
            camera_view_offset = None

            setup_sketch_in_blender(
                file_base_name,
                parameter_data,
                result_paths,
                camera_data,
                curves_sketch=args.convert_to_curves,
                shadow_catcher=False,
                load_sketch=load_sketch,
                load_proxy=args.load_all and load_proxy,
                load_occluders=args.load_all and load_sketch,
                per_vertex_colors = False,
                sharp_features=args.sharp_features,
                curves_thickness=args.thickness,
                camera_view_offset=camera_view_offset
                )

            bpy.ops.wm.save_mainfile(filepath=blend_file_path)

    # if args.turntables:
    #     setup_turntables(nb_frames=240)

    bpy.ops.wm.save_mainfile(filepath=blend_file_path)