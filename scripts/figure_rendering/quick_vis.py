import polyscope as ps
import meshio
import argparse
import os
import sys
from pathlib import Path
import re
import json
from pygel3d import hmesh
import numpy as np

sys.path.append(str(Path(__file__).absolute().parent.parent))

from utils.paths import MESH_FOLDER, get_parameter_file_name
from utils.loader import try_load_obj_data
from utils.pygel import get_faces
from utils.mesh_properties_io import unpack_mesh_properies

ps.init()
def quick_vis(sketch_info, result_ply):
    # Load data
    data_points, segments = try_load_obj_data(sketch_info["sketch_file"])
    ignored_pts_idx = sketch_info['ignored_stroke_points']
    pts_mask = np.ones(len(data_points), dtype=bool)
    pts_mask[ignored_pts_idx] = False
    proxy_mesh = hmesh.load(sketch_info["proxy_file"])

    # Visualize sketch
    ps_sketch = ps.register_curve_network("sketch", data_points, segments, color=(0, 0, 0), radius=0.002)
    ps_sketch.add_scalar_quantity("ignored?", pts_mask)

    # Visualize proxy
    ps_proxy_mesh = ps.register_surface_mesh("proxy mesh", proxy_mesh.positions(), get_faces(proxy_mesh), enabled=False)

    # Visualize result
    result_mesh = meshio.read(result_ply)
    V_res = result_mesh.points
    F_res = result_mesh.cells[0].data

    labels, models_degrees, is_seam, is_sharp = unpack_mesh_properies(result_mesh.point_data)

    ps_res = ps.register_surface_mesh("result", V_res, F_res)
    ps_res.add_scalar_quantity("labels", labels, cmap="rainbow", enabled=True)
    ps_res.add_scalar_quantity("fitted model degree", models_degrees, cmap="blues", enabled=False)

    ps.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--folder', help='Name of the result mesh folder', type=str, default=None)
    parser.add_argument('-s', '--sketch', help='Pattern to select sketch(es)', type=str, default=".+")
  
    args = parser.parse_args()

    # Default logs folder is last modified folder
    folder_path = max([os.path.join(MESH_FOLDER, d) for d in os.listdir(MESH_FOLDER) if os.path.isdir(os.path.join(MESH_FOLDER, d))], key=os.path.getmtime)
    folder_name = os.path.basename(folder_path)
    if args.folder is not None:
        folder_name = args.folder.replace("/", ":")
    result_meshes_folder = os.path.join(MESH_FOLDER, folder_name)

    files = []
    for item in os.listdir(result_meshes_folder):
        if os.path.isfile(os.path.join(result_meshes_folder, item)) and os.path.splitext(item)[1] == '.ply':
            files.append(item)

    for f in sorted(files):

        file_base_name, extension = os.path.splitext(f)

        if extension == '.ply' and re.search(args.sketch, file_base_name):

            with open(os.path.join(result_meshes_folder, get_parameter_file_name(f))) as json_data:
                parameter_data = json.load(json_data)

            quick_vis(parameter_data, os.path.join(result_meshes_folder, f))