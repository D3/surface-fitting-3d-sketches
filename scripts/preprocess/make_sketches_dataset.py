import os
import sys
import re
import argparse
import csv
import json
import numpy as np
import polyscope as ps

# sys.path.append('/Users/emilie/Documents/Work/3D sketch data/scripts')
from sketch_parsing.obj_format import read_obj, write_obj
from sketch_parsing.utils import polylines_to_obj, polylines_to_vertices_line_edges

# sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

from utils.paths import SKETCH_OBJ_FOLDER, SKETCH_XYZ_FOLDER, METADATA_FILE_PATH, ROOT_FOLDER

metadata_file_path = METADATA_FILE_PATH


def read_polylines_from_obj(file_path):
    V = np.empty((0,3))
    lines = []
    E = np.empty((0,2), dtype=int)
    with open(file_path) as file:
        for i, line in enumerate(file):
            els = line.split()
            if els[0] == 'v':
                p = np.array(els[1:]).astype(float)
                V = np.row_stack([V, p])
            elif els[0] == 'l':
                l = np.array(els[1:], dtype=int)
                lines.append(l - 1)
                edges = np.column_stack([l[:-1], l[1:]]) - 1
                E = np.row_stack([E, edges])

    # From the list of edges E, form polylines
    lines = []
    edge_is_assigned = np.zeros(len(E), dtype=bool)
    for e in range(len(E)):
        if edge_is_assigned[e]:
            continue
        line = E[e]
        edge_is_assigned[e] = True
        # Grow line from end
        end_node = line[-1]
        while(np.any(E[np.logical_not(edge_is_assigned)] == end_node)):
            matching_edge_idx = np.flatnonzero(np.any(E[np.logical_not(edge_is_assigned)] == end_node, axis = 1))[0]
            next_edge = E[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_edge_idx = np.arange(len(E))[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_node = next_edge[next_edge != end_node]
            line = np.append(line, next_node)
            end_node = next_node
            edge_is_assigned[next_edge_idx] = True
        # Grow line from start
        start_node = line[0]
        while(np.any(E[np.logical_not(edge_is_assigned)] == start_node)):
            matching_edge_idx = np.flatnonzero(np.any(E[np.logical_not(edge_is_assigned)] == start_node, axis = 1))[0]
            next_edge = E[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_edge_idx = np.arange(len(E))[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_node = next_edge[next_edge != start_node]
            line = np.insert(line, 0, next_node)
            start_node = next_node
            edge_is_assigned[next_edge_idx] = True

        lines.append(line)

    polylines = [V[l] for l in lines]
    return polylines


parser = argparse.ArgumentParser()

parser.add_argument('--output-path', help='Path of the output folder', type=str, default=None)

args = parser.parse_args()

ps.init()

if args.output_path is None:
    output_path = os.path.join(ROOT_FOLDER, "input_sketches", "dataset")
else:
    output_path = args.output_path

with open(metadata_file_path, mode='r', encoding='utf-8-sig') as csv_file:
    metadata = csv.DictReader(csv_file, delimiter=";")

    for i, row in enumerate(metadata):
        file_path = os.path.join(ROOT_FOLDER, row['sketch_file'])
        # Convert it to an OBJ where each line corresponds to a polyline
        polylines = read_polylines_from_obj(file_path)

        V, _, E = read_obj(file_path)


        ps.register_curve_network("sketch", V, E)
        total_v_count = 0
        for idx, polyline in enumerate(polylines):
            ps.register_curve_network(f"curve {idx}", polyline, edges='line')
            total_v_count += len(polyline)
        # ps.show()

        print(len(V), total_v_count)

        ps.remove_all_structures()

        # Save it with display name
        out_file_path = os.path.join(output_path, f"{row['display_name']}.obj")
        V, lines = polylines_to_vertices_line_edges(polylines)
        write_obj(out_file_path, V, lines)