import sys
import os
import argparse
import re
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils.paths import VIPSS_PROXIES_FOLDER, SKETCH_XYZ_FOLDER, ROOT_FOLDER


vipss_executable_path = Path(ROOT_FOLDER) / "external" / "VIPSS" / "vipss" / "vipss"


parser = argparse.ArgumentParser()

parser.add_argument('-s', '--sketch', help='Name of the sketch', type=str, default=".+")
parser.add_argument('-l', '--lambda-vipss', help='VIPSS lambda parameter', type=float, default=0.001)
parser.add_argument('-r', '--resolution-vipss', help='VIPSS resolution parameter', type=int, default=20)
args = parser.parse_args()

# Extract arguments
sketch_name_pattern = args.sketch

output_folder_path = VIPSS_PROXIES_FOLDER
if not os.path.exists(output_folder_path):
    os.makedirs(output_folder_path)

for root, dirs, files in os.walk(SKETCH_XYZ_FOLDER):
    for f in files:
        if f.split(".")[-1] == 'xyz':
            if re.search(args.sketch, f):
                print(f"Running VIPSS for sketch {f}")
                command = f"{vipss_executable_path} -i {os.path.join(SKETCH_XYZ_FOLDER, f)} -o {output_folder_path}/ -l {args.lambda_vipss} -s {args.resolution_vipss}"
                print(command)
                stream = os.system(command)