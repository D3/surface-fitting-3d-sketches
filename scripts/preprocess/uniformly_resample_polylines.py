import os
import sys
import re
import argparse
import csv
import json
import numpy as np
import polyscope as ps
from pathlib import Path

from sketch_parsing.xyz_format import write_xyz
from sketch_parsing.obj_format import read_obj
from simplify_xyz import simplify_point_cloud

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils.paths import SKETCH_OBJ_FOLDER, SKETCH_XYZ_FOLDER, METADATA_FILE_PATH, ROOT_FOLDER

ps.init()

def read_polylines_from_obj(file_path):
    V = np.empty((0,3))
    lines = []
    E = np.empty((0,2), dtype=int)
    with open(file_path) as file:
        for i, line in enumerate(file):
            els = line.split()
            if els[0] == 'v':
                p = np.array(els[1:]).astype(np.float)
                V = np.row_stack([V, p])
            elif els[0] == 'l':
                l = np.array(els[1:], dtype=int)
                lines.append(l - 1)
                edges = np.column_stack([l[:-1], l[1:]]) - 1
                E = np.row_stack([E, edges])

    # From the list of edges E, form polylines
    lines = []
    edge_is_assigned = np.zeros(len(E), dtype=bool)
    for e in range(len(E)):
        if edge_is_assigned[e]:
            continue
        line = E[e]
        edge_is_assigned[e] = True
        # Grow line from end
        end_node = line[-1]
        while(np.any(E[np.logical_not(edge_is_assigned)] == end_node)):
            matching_edge_idx = np.flatnonzero(np.any(E[np.logical_not(edge_is_assigned)] == end_node, axis = 1))[0]
            next_edge = E[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_edge_idx = np.arange(len(E))[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_node = next_edge[next_edge != end_node]
            line = np.append(line, next_node)
            end_node = next_node
            edge_is_assigned[next_edge_idx] = True
        # Grow line from start
        start_node = line[0]
        while(np.any(E[np.logical_not(edge_is_assigned)] == start_node)):
            matching_edge_idx = np.flatnonzero(np.any(E[np.logical_not(edge_is_assigned)] == start_node, axis = 1))[0]
            next_edge = E[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_edge_idx = np.arange(len(E))[np.logical_not(edge_is_assigned)][matching_edge_idx]
            next_node = next_edge[next_edge != start_node]
            line = np.insert(line, 0, next_node)
            start_node = next_node
            edge_is_assigned[next_edge_idx] = True

        lines.append(line)

    polylines = [V[l] for l in lines]
    return polylines


def write_polylines_to_obj(polylines, out_file_path):

    # Write the OBJ file
    with open(out_file_path, 'w') as writer:
        for polyline in polylines:
            for v in polyline:
                writer.write("v {} {} {}\n".format(v[0], v[1], v[2]))

        # for l in lines:
        vidx = 1 # OBJ indices start at 1
        for polyline in polylines:
            txt = "l"
            for vi in np.arange(len(polyline)) + vidx:
                txt += " {}".format(vi)
            vidx += len(polyline) 
            writer.write(txt + "\n")


def uniformly_resample_polylines(polylines, target_edge_length):
    """
    Uniformly resamples a given polyline using a piecewise-linear parametric function.
    The number of points is chosen automatically such that the distance between a pair of adjacent points
    is as close as possible to the given target edge length.

    The algorithm is described in https://stackoverflow.com/a/4053147/1606707

    :param polylines: list of #V x {2,3} numpy matrices representing the input polylines.
    :param target_edge_length: target edge length of output polylines.
    :return: output polylines.
    """
    resampled_polylines = []

    for polyline in polylines:

        # number of vertices and edges in the input polyline
        n_verts_in = polyline.shape[0]
        n_edges_in = n_verts_in - 1

        # number of coordinates
        dim = polyline.shape[1]

        # edge lengths
        edge_vectors = polyline[1:, :] - polyline[:-1, :]
        edge_lengths = np.linalg.norm(edge_vectors, axis=1)

        # cumulative edge lengths
        cumulative_lengths = np.zeros((n_verts_in,), np.float32)
        cumulative_lengths[1:] = np.cumsum(edge_lengths)

        # total length
        length = cumulative_lengths[-1]

        # number of new segments
        n_edges_out = round(length / target_edge_length)
        if n_edges_out == 0:
            n_edges_out = 1
        real_edge_length = length / n_edges_out
        n_edges_out = int(n_edges_out)
        n_verts_out = n_edges_out + 1

        if n_edges_out < 1:
            n_edges_out = 1

        # prepare the resampled polyline
        resampled_polyline = np.zeros((n_edges_out + 1, dim))

        # sample uniformly
        k = 0  # index of the (old) polyline edge
        t = 0.0  # arc length parameter in [0, length]

        # loop over new points
        for i in range(n_verts_out):

            # if t is too big: move on to the next interval
            while k < n_edges_in - 1 and t > cumulative_lengths[k + 1]:
                k += 1

            # normalized parameter in [0,1]
            u = (t - cumulative_lengths[k]) / (cumulative_lengths[k + 1] - cumulative_lengths[k])

            # compute the new point via linear interpolation
            resampled_polyline[i, :] = (1 - u) * polyline[k, :] + u * polyline[k + 1, :]

            # move on
            t += real_edge_length

        resampled_polylines.append(resampled_polyline)

    return resampled_polylines

metadata_file_path = METADATA_FILE_PATH

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch (pattern)', type=str, default=".+")
    parser.add_argument('--export', help='Export (and overwrite)', dest='export', action='store_true', default=False)

    args = parser.parse_args()

    print("CAUTION: Resampling completely messes up vertex indexing on the strokes!")
    print("CAUTION: If you set export to true, your OBJ files WILL be overwritten!")

    with open(metadata_file_path, mode='r', encoding='utf-8-sig') as csv_file:
        metadata = csv.DictReader(csv_file, delimiter=";")

        for i, row in enumerate(metadata):
            if re.search(args.sketch, row['sketch_name']):
                if bool(int(row['use_manual_proxy'])):
                    print(f"For sketch {row['sketch_name']} metadata indicates that we will use a manually created proxy, so we don't create VIPSS input data.")
                    continue
                file_path = os.path.join(SKETCH_OBJ_FOLDER, f"{row['sketch_name']}.obj")
                polylines = read_polylines_from_obj(file_path)

                # for idx, p in enumerate(polylines):
                #     ps.register_curve_network(f"initial stroke {idx}", p.astype(float), edges='line')

                # ps.show()

                resampled_polylines = uniformly_resample_polylines(polylines, 0.01)

                for idx, p in enumerate(resampled_polylines):
                    # ps.register_curve_network(f"resampled stroke {idx}", p, edges='line')
                    ps.register_point_cloud(f"resampled stroke pts {idx}", p)

                ps.show()

                if args.export:
                    write_polylines_to_obj(resampled_polylines, file_path)
