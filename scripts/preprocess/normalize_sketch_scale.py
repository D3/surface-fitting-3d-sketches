import os
import numpy as np
import re
import argparse
import sys
from pathlib import Path

from sketch_parsing.obj_format import read_obj, write_obj

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils.paths import SKETCH_OBJ_FOLDER, ORIGINAL_SKETCH_OBJ_FOLDER

def rescale_sketch(target_bb_diagonal_length, input_file_path, output_folder_path=SKETCH_OBJ_FOLDER) :
    V, lines, _ = read_obj(input_file_path)

    initial_bb_diag_length = np.linalg.norm(np.max(V, axis = 0) - np.min(V, axis = 0))

    scale_factor = target_bb_diagonal_length / initial_bb_diag_length

    V = V * scale_factor

    center = np.mean(V, axis = 0)

    # center[0] = 0

    V = V - center
    print(f"Scaling by factor = {scale_factor}, translating by: {-center}")

    file_name = os.path.basename(input_file_path)

    if not os.path.exists(output_folder_path):
        os.makedirs(output_folder_path)
        
    write_obj(os.path.join(output_folder_path, file_name), V, lines)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch (pattern)', type=str, default=".+")

    args = parser.parse_args()

    for root, dirs, files in os.walk(os.path.join(ORIGINAL_SKETCH_OBJ_FOLDER)):
        for f in files:
            if f.split(".")[-1] == 'obj':
                if re.search(args.sketch, f):
                    file_path = os.path.join(root, f)
                    rescale_sketch(1.0, file_path, output_folder_path=SKETCH_OBJ_FOLDER)