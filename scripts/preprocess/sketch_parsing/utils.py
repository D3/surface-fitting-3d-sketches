import numpy as np
import polyscope as ps

from formats.obj_format import write_obj


def add_edge_indices_for_stroke(indices, node_count):
    N = node_count - 1
    start_idx = 0
    if len(indices) > 0:
        start_idx = indices[-1, -1] + 1
    left = np.arange(start_idx, start_idx + N).reshape(-1,1)
    right = left + 1
    return np.append(indices, np.concatenate((left, right), axis = 1), axis = 0)

def polylines_to_vertices_line_edges(polylines):
    vertices = np.empty((0, 3))
    line_edges = []

    for polyline in polylines:
        vertices = np.row_stack([vertices, polyline])
        line_edges.append(np.arange(len(vertices) - len(polyline), len(vertices)))

    return vertices, line_edges

def polylines_to_graph(polylines):
    V = np.empty((0,3))
    E = np.empty((0,2), dtype=int)

    for p in polylines:
        V = np.row_stack([V, p])
        E = add_edge_indices_for_stroke(E, len(p))

    return V, E

def polylines_to_obj(polylines, save_path):
    vertices, line_edges = polylines_to_vertices_line_edges(polylines)

    write_obj(save_path, vertices, line_edges)

    # Write the OBJ file
    # with open(save_path, 'w') as writer:
    #     for v in vertices:
    #         writer.write("v {} {} {}\n".format(v[0], v[1], v[2]))

    #     for l in line_edges:
    #         txt = "l"
    #         for vidx in l:
    #             txt += " {}".format(vidx)
    #         writer.write(txt + "\n")


def visualize_polylines(polylines):
    V, E = polylines_to_graph(polylines)
    ps.init()

    ps.register_curve_network("sketch", V, E)
    ps.register_point_cloud("vertices", V)

    ps.show()