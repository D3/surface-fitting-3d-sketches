import numpy as np
import os

def read_xyz(file_path):
    V = np.empty((0,3))
    with open(file_path) as file:
        for line in file:
            els = line.split()
            p = np.array(els).astype(np.float)
            V = np.row_stack([V, p])
    return V

def write_xyz(out_file_path, V):
    xyz_lines = []

    for v in V:
        xyz_lines.append("{} {} {}\n".format(v[0], v[1], v[2]))

    with open(out_file_path,'w') as out:
        out.writelines(xyz_lines)
