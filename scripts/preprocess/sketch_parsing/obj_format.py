import numpy as np
import os

def read_obj(file_path):
    V = np.empty((0,3))
    lines = []
    E = np.empty((0,2))
    with open(file_path) as file:
        for i, line in enumerate(file):
            els = line.split()
            if els[0] == 'v':
                p = np.array(els[1:]).astype(np.float)
                V = np.row_stack([V, p])
            elif els[0] == 'l':
                l = np.array(els[1:], dtype=int) - 1 # OBJ indices start at 1
                lines.append(l)
                edges = np.column_stack([l[:-1], l[1:]])
                E = np.row_stack([E, edges])
    return V, lines, E

def write_obj(out_file_path, V, lines):

    # Write the OBJ file
    with open(out_file_path, 'w') as writer:
        for v in V:
            writer.write("v {} {} {}\n".format(v[0], v[1], v[2]))

        for l in lines:
            txt = "l"
            for vidx in l:
                txt += " {}".format(vidx + 1) # OBJ indices start at 1
            writer.write(txt + "\n")
