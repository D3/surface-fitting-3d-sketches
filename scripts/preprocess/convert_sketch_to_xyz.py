import os
import sys
import re
import argparse
import csv
import json
import numpy as np
import polyscope as ps
from pathlib import Path

from sketch_parsing.xyz_format import write_xyz
from sketch_parsing.obj_format import read_obj
from simplify_xyz import simplify_point_cloud

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils.paths import SKETCH_OBJ_FOLDER, SKETCH_XYZ_FOLDER, METADATA_FILE_PATH, ROOT_FOLDER

ps.init()

def obj_to_xyz(obj_file_path, ignored_pts, symmetric, simplify=True, xyz_folder_path=SKETCH_XYZ_FOLDER, xyz_file_name=None):
    V, _ , _= read_obj(obj_file_path)

    ps.register_point_cloud("all pts", V)

    # Remove ignored points
    if len(ignored_pts) > 0:
        mask = np.ones(len(V), dtype=bool)
        mask[ignored_pts] = False
        V = V[mask]
    ps.register_point_cloud("pts kept", V)

    ps.show()

    print(len(V), "vertices in sketch")

    if simplify:
        V_simplified = simplify_point_cloud(V, symmetric)

        ps.register_point_cloud("pts simplified", V_simplified)
        ps.show()

        V = V_simplified

    if not os.path.exists(xyz_folder_path):
        os.makedirs(xyz_folder_path)

    # Export a .xyz file with points from input strokes      
    if xyz_file_name:
        file_name = xyz_file_name + ".xyz"
    else:
        file_name = os.path.splitext(os.path.basename(obj_file_path))[0] + ".xyz"
    write_xyz(os.path.join(xyz_folder_path, file_name), V)

    print("done with " + file_name)
    ps.remove_all_structures()

metadata_file_path = METADATA_FILE_PATH

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch (pattern)', type=str, default=".+")
    parser.add_argument('--no-simplify', help='Do not apply simplification', dest='simplify', action='store_false', default=True)

    args = parser.parse_args()

    with open(metadata_file_path, mode='r', encoding='utf-8-sig') as csv_file:
        metadata = csv.DictReader(csv_file, delimiter=";")

        for i, row in enumerate(metadata):
            if re.search(args.sketch, row['sketch_name']):
                if bool(int(row['use_manual_proxy'])):
                    print(f"For sketch {row['sketch_name']} metadata indicates that we will use a manually created proxy, so we don't create VIPSS input data.")
                    continue
                file_path = os.path.join(SKETCH_OBJ_FOLDER, f"{row['sketch_name']}.obj")
                ignored_stroke_points = [] if len(row['ignored_stroke_points']) == 0 else json.loads(row['ignored_stroke_points'])
                symmetric = bool(int(row['is_symmetric']))
                obj_to_xyz(file_path, ignored_stroke_points, symmetric, simplify=args.simplify, xyz_file_name=row["sketch_name"])
