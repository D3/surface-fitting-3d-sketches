import os
import sys
import csv
import re
import polyscope as ps
import numpy as np
import argparse
from pathlib import Path

from CGAL.CGAL_Kernel import Point_3
from CGAL.CGAL_Kernel import Vector_3
from CGAL.CGAL_Point_set_3 import *
from CGAL.CGAL_Point_set_processing_3 import *


from sketch_parsing.xyz_format import read_xyz, write_xyz

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils.paths import SKETCH_XYZ_FOLDER, METADATA_FILE_PATH


def simplify_point_cloud(V, symmetric):

    if symmetric:
        # Slightly offset the symmetric points, to avoid numerical issues due to coincident points
        # (typically that happens when there are 2 near coincident curves lying on the mirror)
        mirrored_points = np.flatnonzero(V[:, 0] < 0)
        V[mirrored_points] += np.column_stack([
                                    - 1e-3 * np.ones(len(mirrored_points)),
                                    np.zeros(len(mirrored_points)),
                                    np.zeros(len(mirrored_points))
                                    ])

    # print(V)
    w = Point_set_3()

    for v in V:
        w.insert(Point_3(v[0], v[1], v[2]))

    cell_size = 0.01 * np.linalg.norm(np.max(V, axis = 0) - np.min(V, axis = 0))
    res = grid_simplify_point_set(w, cell_size)


    kept_indices = []
    for i in w.indices():
        kept_indices.append(i)

    # print(kept_indices)
    # print(np.unique(np.linalg.norm(V[kept_indices], axis = 1), return_counts=True))

    unique_pts, counts = np.unique(V[kept_indices], axis = 0, return_counts=True)

    print("Nb of points initial = {}".format(len(V)))
    print("Nb of points kept = {}".format(len(kept_indices)))

    # ps.register_point_cloud("initial", V)
    # ps.register_point_cloud("kept", V[kept_indices])

    # ps.show()

    return V[kept_indices]

# ps.init()
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch (pattern)', type=str, default=".+")

    args = parser.parse_args()
    with open(METADATA_FILE_PATH, mode='r', encoding='utf-8-sig') as csv_file:
        metadata = csv.DictReader(csv_file, delimiter=";")

        for i, row in enumerate(metadata):
            print(row['sketch_name'])
            if re.search(args.sketch, row['sketch_name']):
                sketch_file_path = os.path.join(SKETCH_XYZ_FOLDER, row['sketch_name'] + ".xyz")
                symmetric = int(row['is_symmetric'])
                simplify_point_cloud(read_xyz(sketch_file_path), symmetric)
