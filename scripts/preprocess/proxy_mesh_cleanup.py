import numpy as np
import polyscope as ps
from pygel3d import hmesh
import sys
import os
import re
import argparse
import csv
import scipy
import pymesh
import json
from pathlib import Path

from proxy_to_strokes import attract_proxy_to_strokes

sys.path.append(str(Path(__file__).absolute().parent.parent))

from main.mesh_optimization import Laplacian_umbrella, zero_rows
from utils.pygel import get_faces, split_halfedge
from utils.mirroring import cut_mesh, symmetrize_mesh, should_remove
from utils.loader import try_load_obj_data
from utils.math import remove_mesh_vertices, remove_data_points
from utils.paths import ROOT_FOLDER, METADATA_FILE_PATH, CLEANED_PROXIES_FOLDER, VIPSS_PROXIES_FOLDER, MANUAL_PROXIES_FOLDER, SKETCH_OBJ_FOLDER

# TODO: replace this by your path to the Instant Meshes executable.
# This one is correct for Mac, if you build Instant Meshes yourself:
instant_mesh_executable = os.path.join(ROOT_FOLDER, "external/instant-meshes/Instant\ Meshes.app/Contents/MacOS/Instant\ Meshes")

# If downloaded as precompiled binary:
# instant_mesh_executable = "/Applications/Instant\ Meshes.app/Contents/MacOS/Instant\ Meshes"



def simple_smoothing(gel_mesh, w_smooth):
    L, on_boundary_vertices = Laplacian_umbrella(gel_mesh, flat=True)

    V_0 = gel_mesh.positions()
    A_s = w_smooth * L.T @ L
    A = A_s + scipy.sparse.identity(len(V_0) * 3)
    B = V_0.flatten()

    V = (scipy.sparse.linalg.spsolve(A, B)).reshape((-1, 3))

    V[on_boundary_vertices] = V_0[on_boundary_vertices]

    V_0[:] = V

def taubin_smoothing_step(gel_mesh, w_smooth, N_steps = 100):
    L, on_boundary_vertices = Laplacian_umbrella(gel_mesh, flat=False)

    V_0 = gel_mesh.positions()
    # A_s = w_smooth * L.T @ L
    # A = A_s + scipy.sparse.identity(len(V_0) * 3)
    # B = V_0.flatten()

    mu = w_smooth + 0.0001
    # mu = 0
    lamb = w_smooth

    V = V_0.copy()

    for i in range(N_steps):
        # print(f"Smoothing step {i}")
        V = (scipy.sparse.identity(len(V_0)) - mu * L) @ (scipy.sparse.identity(len(V_0)) + lamb * L) @ V

    V[on_boundary_vertices] = V_0[on_boundary_vertices]

    V_0[:] = V

def remove_isolated_vertices(V, F):
    assigned_vertices = np.unique(F)
    print(f"Removing {len(V) - len(assigned_vertices)} vertices")
    V = V[assigned_vertices]
    F = np.searchsorted(assigned_vertices, F)
    return V, F


def cleanup_mesh(
    input_mesh_path,
    output_file_path,
    symmetrize,
    open_boundary,
    min_bound, max_bound,
    target_edge_length=None,
    attract_to_strokes=False, stroke_points=None):

    mesh = pymesh.load_mesh(input_mesh_path)

    V0 = mesh.vertices
    F0 = mesh.faces

    # If open boundary, trim
    if open_boundary:
        should_remove = np.zeros(len(V0), dtype=bool)
        is_out_of_bounds = np.any(
                            np.array([
                                np.any(
                                    np.array([
                                        V0[:, 0] < min_bound[0],
                                        V0[:, 1] < min_bound[1],
                                        V0[:, 2] < min_bound[2]
                                    ]).T,
                                    axis = 1
                                ),
                                np.any(
                                    np.array([
                                        V0[:, 0] > max_bound[0],
                                        V0[:, 1] > max_bound[1],
                                        V0[:, 2] > max_bound[2]
                                    ]).T,
                                    axis = 1
                                )
                            ]).T,
                            axis = 1
                        )

        should_remove[is_out_of_bounds] = True

        V0, F0, _ = remove_mesh_vertices(V0, F0, should_remove)

    gel_proxy_mesh = hmesh.Manifold.from_triangles(V0, F0)
    # gel_proxy_mesh = hmesh.ply_load(input_mesh_path)

    V0 = gel_proxy_mesh.positions()
    F0 = get_faces(gel_proxy_mesh)

    ps.register_surface_mesh("initial surface", V0, F0, enabled=False)

    hmesh.quadric_simplify(gel_proxy_mesh, keep_fraction=0.8)
    hmesh.remove_needles(gel_proxy_mesh)
    hmesh.maximize_min_angle(gel_proxy_mesh)
    # print("Subdividing {} time(s)...".format(subdivision_level))
    # for i in range(subdivision_level):
    #     hmesh.root3_subdivide(gel_proxy_mesh)
    #     hmesh.maximize_min_angle(gel_proxy_mesh)

    gel_proxy_mesh.cleanup()

    V = gel_proxy_mesh.positions()
    F = get_faces(gel_proxy_mesh)

    hmesh.obj_save(output_file_path, gel_proxy_mesh)

    # Compute avg edge length to preserve it
    if target_edge_length is None:
        avg_edge_length = 0
        he_count = 0

        for hid in gel_proxy_mesh.halfedges():
            avg_edge_length += gel_proxy_mesh.edge_length(hid)
            he_count += 1

        target_edge_length = avg_edge_length / he_count

        print("Avg edge length =", target_edge_length)

    # Run instant mesh
    command = f"{instant_mesh_executable} {output_file_path} -S 3 -r 6 -p 6 -s {target_edge_length} -d -o {output_file_path}"
    print(command)
    stream = os.popen(command)

    print(stream.read())

    gel_proxy_mesh = hmesh.load(output_file_path)

    # Check if that's a manifold
    if hmesh.valid(gel_proxy_mesh):
        print("Seems to be a valid manifold!")
    else:
        print("ERROR: not a valid manifold! Can't generate a valid proxy mesh.")
        return

    V = gel_proxy_mesh.positions()
    F = get_faces(gel_proxy_mesh)

    # gel_proxy_mesh.cleanup()

    # V, F = remove_isolated_vertices(V, F)

    # ps.register_surface_mesh("instant mesh output", V, F, enabled=False)

    # gel_proxy_mesh = hmesh.Manifold.from_triangles(V, F)

    # ps.show()

    # if attract_to_strokes:
    #     # Attract the mesh to the strokes
    #     gel_proxy_mesh = attract_proxy_to_strokes(gel_proxy_mesh, stroke_points, symmetry=bool(symmetrize), smoothness=1000)

    #     hmesh.obj_save(output_file_path, gel_proxy_mesh)

    #     # Run instant mesh
    #     command = f"{instant_mesh_executable} {output_file_path} -S 3 -r 6 -p 6 -s {target_edge_length} -d -o {output_file_path}"
    #     print(command)
    #     stream = os.popen(command)

    #     print(stream.read())

    #     gel_proxy_mesh = hmesh.load(output_file_path)
    #     V = gel_proxy_mesh.positions()
    #     F = get_faces(gel_proxy_mesh)

    #     ps.register_surface_mesh("attracted", gel_proxy_mesh.positions(), F, enabled=False)
    # else:
    #     # Smooth
    #     print("smoothing mesh")
    #     simple_smoothing(gel_proxy_mesh, w_smooth=50)
    #     ps.register_surface_mesh("smoothed", gel_proxy_mesh.positions(), F, enabled=False)
    #     ps.register_point_cloud("smoothed V", gel_proxy_mesh.positions(), enabled=False)


    if symmetrize:
        print("symmetrizing mesh")
        for h in gel_proxy_mesh.halfedges():
            vA = gel_proxy_mesh.incident_vertex(h)
            vB = gel_proxy_mesh.incident_vertex(gel_proxy_mesh.opposite_halfedge(h))

            closest_to_mirror = [vA, vB][np.argmin(np.abs(V[[vA, vB], 0]))]

            if np.abs(V[closest_to_mirror, 0]) < target_edge_length * 0.5 :
                V[closest_to_mirror, 0] = 0
            else:
                if V[vA, 0] * V[vB, 0] < 0:
                    # Split this edge
                    new_v = split_halfedge(gel_proxy_mesh, h)
                    V = gel_proxy_mesh.positions()
                    V[new_v, 0] = 0

        V = gel_proxy_mesh.positions()
        F = get_faces(gel_proxy_mesh)

        V, F, _ = cut_mesh(V, F)

        half_mesh = hmesh.Manifold.from_triangles(V, F)

        # hmesh.quadric_simplify(half_mesh, keep_fraction=0.8)
        hmesh.remove_needles(half_mesh)
        hmesh.maximize_min_angle(half_mesh)
        # hmesh.root3_subdivide(half_mesh)
        # hmesh.maximize_min_angle(half_mesh)

        half_mesh.cleanup()

        V = half_mesh.positions()
        F = get_faces(half_mesh)

        gel_proxy_mesh = half_mesh



    ps.register_surface_mesh("cleaned up", gel_proxy_mesh.positions(), F, enabled=False)
    ps_cleaned_up_V = ps.register_point_cloud("cleaned up V", gel_proxy_mesh.positions(), enabled=False)

    cleaned_up_V = gel_proxy_mesh.positions().copy()

    if attract_to_strokes:
        # Attract the mesh to the strokes
        gel_proxy_mesh, stroke_points_proj = attract_proxy_to_strokes(gel_proxy_mesh, stroke_points, symmetry=bool(symmetrize), smoothness=100)
        ps.register_surface_mesh("attracted", gel_proxy_mesh.positions(), F, enabled=False)
        # ps.register_point_cloud("projections", stroke_points_proj)
        # ps.show()


        # taubin_smoothing_step(gel_proxy_mesh, w_smooth = 0.9, N_steps=1000)

    else:
        # Smooth
        print("smoothing mesh")
        simple_smoothing(gel_proxy_mesh, w_smooth=50)

    ps.register_surface_mesh("final", gel_proxy_mesh.positions(), F)
    ps_final_V = ps.register_point_cloud("final V", gel_proxy_mesh.positions(), enabled=False)
    ps_cleaned_up_V.add_vector_quantity("displacement", gel_proxy_mesh.positions() - cleaned_up_V, length=1)


    hmesh.obj_save(output_file_path, gel_proxy_mesh)

parser = argparse.ArgumentParser()

parser.add_argument('--sketch', help='Name of the sketch', type=str, default=".+")
parser.add_argument('--input-file', help='Provide a specific input mesh file path', type=str, default=None)
parser.add_argument('--filtering-key', help='Data key that the filtering operates on (default is sketch_name)', type=str, default="sketch_name")
parser.add_argument('--output-file-name', help='Provide a specific output file name', type=str, default=None)
parser.add_argument('--symmetric', help='Override is_symmetric flag from metadata', dest='is_symmetric', action='store_true', default=None)
parser.add_argument('--not-symmetric', help='Override is_symmetric flag from metadata', dest='is_symmetric', action='store_false', default=None)
parser.add_argument('--attracted', help='Override attract_to_strokes flag from metadata', dest='attract_to_strokes', action='store_true', default=None)
parser.add_argument('--not-attracted', help='Override attract_to_strokes flag from metadata', dest='attract_to_strokes', action='store_false', default=None)
parser.add_argument('--target-edge-length', help='Override target_edge_length value from metadata', type=float, default=None)
parser.add_argument('--metadata-path', help='Override metadata file path', type=str, default=None)

args = parser.parse_args()

# Extract arguments
sketch_name_pattern = args.sketch

metadata_file_path = METADATA_FILE_PATH
if args.metadata_path is not None:
    metadata_full_path = os.path.join(ROOT_FOLDER, args.metadata_path)
    if os.path.isfile(metadata_full_path):
        metadata_file_path = metadata_full_path


ps.init()
with open(metadata_file_path, mode='r', encoding='utf-8-sig') as csv_file:
    metadata = csv.DictReader(csv_file, delimiter=";")

    for i, row in enumerate(metadata):
        print(row['sketch_name'])

        sketch_file_path = os.path.join(SKETCH_OBJ_FOLDER, f"{row['sketch_name']}.obj")
        use_manual_proxy = bool(int(row['use_manual_proxy']))
        proxy_file = os.path.join(VIPSS_PROXIES_FOLDER, f"{row['sketch_name']}_surface.ply")
        if use_manual_proxy:
            proxy_file = os.path.join(MANUAL_PROXIES_FOLDER, f"{row['sketch_name']}.obj")
        if args.input_file is not None:
            proxy_file = args.input_file
        if proxy_file is not None and len(proxy_file) > 0 and re.search(sketch_name_pattern, row[args.filtering_key]):
            if not os.path.exists(proxy_file):
                print(f"ERROR: file {proxy_file} does not exist.")
                continue

            V, E = try_load_obj_data(sketch_file_path)

            ignored_stroke_points = [] if len(row['ignored_stroke_points']) == 0 else json.loads(row['ignored_stroke_points'])
            V, E, mask_V, mask_E = remove_data_points(V, E, ignored_stroke_points)

            ps.register_point_cloud('sketch points', V)
            ps.register_curve_network('sketch', V, E)
            # ps.show()
            
            sketch_min_bound = np.min(V, axis = 0)
            sketch_max_bound = np.max(V, axis = 0)

            span = sketch_max_bound - sketch_min_bound
            sketch_min_bound -= 0.2 * span
            sketch_max_bound += 0.2 * span

            is_symmetric = int(row['is_symmetric']) if args.is_symmetric is None else args.is_symmetric

            if is_symmetric:
                V = V[np.logical_not(should_remove(V)), :]

            ps.register_point_cloud('sketch points', V)

            # ps.show()

            output_file_name = row["sketch_name"] + ".obj"
            if args.output_file_name is not None:
                output_file_name = args.output_file_name + ".obj"
            output_file_path = os.path.join(CLEANED_PROXIES_FOLDER, output_file_name)

            if not os.path.exists(CLEANED_PROXIES_FOLDER):
                os.makedirs(CLEANED_PROXIES_FOLDER)

            cleanup_mesh(
                proxy_file,
                output_file_path,
                is_symmetric,
                int(row['open_boundary']),
                sketch_min_bound, sketch_max_bound,
                target_edge_length=float(row['proxy_resolution']) if args.target_edge_length is None else args.target_edge_length,
                attract_to_strokes=use_manual_proxy if args.attract_to_strokes is None else args.attract_to_strokes,
                stroke_points=V
            )

            ps.show()

