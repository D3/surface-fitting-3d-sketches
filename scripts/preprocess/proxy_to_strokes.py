import numpy as np
import scipy
# import igl
import math
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent))

from utils.pygel import get_faces
from utils.mirroring import is_on_mirror
from utils.math import index_into_flat_3d
from utils.projection import closest_pts
from main.mesh_optimization import Laplacian_umbrella, Laplacian_cotan, weigh_rows, zero_rows

def barycentric_weights_matrix_and_point_vector(V, F, data_points, triangle_ids, barycentric_coordinates, flat=False):

    if flat:
        P = data_points.flatten()

        all_b_coords = barycentric_coordinates.flatten()
        all_v_ids = F[triangle_ids].flatten()

        data = np.repeat(all_b_coords, 3)
        rows = np.tile([0,1,2], len(data_points)*3) + 3*np.repeat(np.arange(len(data_points)), 9)
        cols = index_into_flat_3d(all_v_ids)

        B = scipy.sparse.csr_matrix((data, (rows, cols)), shape=(3 * len(data_points), 3 * len(V)))

    else:

        P = data_points

        all_b_coords = barycentric_coordinates.flatten()
        all_v_ids = F[triangle_ids].flatten()

        data = all_b_coords
        rows = np.repeat(np.arange(len(data_points)), 3)
        cols = all_v_ids

        B = scipy.sparse.csr_matrix((data, (rows, cols)), shape=(len(data_points), len(V)))

    return B, P

def optimize_mesh_for_strokes(mesh, B, stroke_points, w_fit, w_smooth, V_is_on_mirror=None):

    symmetry = V_is_on_mirror is not None

    L, boundary_vertices_idx = Laplacian_umbrella(mesh, flat=True, ignore_boundaries=False)
    # L = Laplacian_cotan(V, F)

    if symmetry:
        V_mirror = np.flatnonzero(V_is_on_mirror)
        # Remove on-mirror vertices from boundary vertices list
        boundary_vertices_idx = boundary_vertices_idx[np.logical_not(V_is_on_mirror[boundary_vertices_idx])]

    # Do not smooth the boundary vertices (they are fixed)
    L = zero_rows(L, boundary_vertices_idx, flat=True)

    identity_weight = np.zeros(3 * len(mesh.vertices()))
    identity_weight[index_into_flat_3d(boundary_vertices_idx)] = 1

    # On-mirror vertices should keep the same X coordinate
    if symmetry:
        identity_weight[3 * V_mirror] = 1 # Only x coordinate

    identity_weighted = scipy.sparse.identity(3 * len(mesh.vertices()))
    identity_weighted = weigh_rows(identity_weighted, identity_weight)

    A = w_smooth * L.T @ L + w_fit * B.T @ B 
    if symmetry:
        A = zero_rows(A, 3 * V_mirror)
    A += identity_weighted
    rhs = w_fit * (B.T @ stroke_points).flatten().reshape((-1,1)) + identity_weight[:, None] * mesh.positions().flatten().reshape((-1, 1))

    print("start linear solve")

    X = scipy.sparse.linalg.spsolve(A, rhs).reshape((-1,3))

    X[boundary_vertices_idx] = mesh.positions()[boundary_vertices_idx]

    # Put back on-mirror vertices to be exactly on the mirror
    if symmetry:
        X[V_mirror, 0] = 0

    print("end linear solve")

    return X

def attract_proxy_to_strokes(mesh, data_points, smoothness = 1, symmetry=False):
    V = mesh.positions()
    F = get_faces(mesh)

    projs, triangle_ids, barycentric_coords = closest_pts(V, F, data_points)

    if symmetry:
        V_on_mirror = is_on_mirror(V)
    else:
        V_on_mirror = None
    
    # Remove constraints near boundary to preserve boundary shape
    mask = []
    for f in triangle_ids:
        mask_f = True
        for vi in mesh.circulate_face(f, mode='v'):
            if mesh.is_vertex_at_boundary(vi):
                if symmetry and V_on_mirror[vi]:
                    continue
                mask_f = False
                break
        mask.append(mask_f)

    mask = np.array(mask)

    triangle_ids = triangle_ids[mask]
    barycentric_coords = barycentric_coords[mask]
    data_points = data_points[mask]

    # dP = data_points - projs

    B, P = barycentric_weights_matrix_and_point_vector(V, F, data_points, triangle_ids, barycentric_coords, flat=True)
    # bb_diag = np.linalg.norm(np.max(V, axis = 0) - np.min(V, axis = 0))
    # w_fit = 1 / np.mean(np.linalg.norm(dP, axis=1))**2
    w_fit = 1 / len(data_points)
    smoothness /= len(V)
    opt_verts = optimize_mesh_for_strokes(mesh, B, P, w_fit, smoothness, V_on_mirror)

    # Update proxy geometry with optimized proxy
    V[:] = opt_verts

    return mesh, projs