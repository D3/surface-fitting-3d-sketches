import numpy as np
from scipy.sparse.csgraph import connected_components

from utils.pygel import split_halfedge, find_boundary_edges, find_boundary_faces, get_faces, min_angle_energy_delta, get_edges
from utils.labelling import get_subsets_from_labels
from utils.graph import affinity_matrix_sparse, get_edge_key, affinity_matrix_sparse


def insert_boundary_vertices(proxy_mesh, labels, stroke_position_by_edge, in_out_labelling, vertex_is_on_mirror):
    # Pre-process proxy mesh
    #  - Detect all edges that have endpoints with 2 different labels
    #    and all faces that have vertices with 3 different labels
    #  - Insert new vertices by splitting those faces
    #  - Insert new vertices by splitting those edges
    #  - Assign labels to those new vertices according to the labels of neighbors (either 2 or 3 different labels)
    #    This is done by updating final_subsets and adding new vertex indices to the corresponding subset

    # _, unique_final_labels = np.unique(labels, return_inverse=True)
    
    # Set all outside vertices to a label
    outside_label = np.max(labels) + 1
    labels_with_outside = np.copy(labels)
    labels_with_outside[in_out_labelling] = outside_label

    boundary_he_id, boundary_he_labels = find_boundary_edges(labels_with_outside, proxy_mesh)
    boundary_face_id, boundary_face_labels = find_boundary_faces(labels_with_outside, proxy_mesh)

    final_subsets, assigned_labels = get_subsets_from_labels(labels)

    # Ignore outside label in the subsets
    if assigned_labels[-1] == outside_label:
        final_subsets.pop()
        assigned_labels.pop()

    boundary_vertices_id = []
    boundary_vertices_subsets = [[] for l in assigned_labels]

    boundary_v_id = 0

    # labels_per_boundary_vertex = []

    # stroke_position_by_boundary_vertex = {}
    attracted_vertices_id = []
    attracted_vertices_target = np.empty((0,3))
    attraction_is_mandatory = []

    # for idx, s in enumerate(final_subsets):
    #     print("Subset {} has {} elements".format(idx, len(s)))

    # Insert new vertices at faces
    for fid, f_labels in zip(boundary_face_id, boundary_face_labels):
        # Except if it's a face with an outside label
        # if outside_label in f_labels:
        #     continue
        vid = proxy_mesh.split_face_by_vertex(fid)
        boundary_vertices_id.append(vid)
        # labels_per_boundary_vertex.append(f_labels)
        # Add point to the subsets
        for l in f_labels:
            if l != outside_label:
                final_subsets[l].append(vid)
                boundary_vertices_subsets[l].append(boundary_v_id)

        in_out_labelling = np.append(in_out_labelling, np.any(np.array(f_labels) == outside_label))
        vertex_is_on_mirror = np.append(vertex_is_on_mirror, False)

        boundary_v_id += 1

    # Insert new vertices at edges
    for hid, h_labels in zip(boundary_he_id, boundary_he_labels):

        edge = [proxy_mesh.incident_vertex(hid), proxy_mesh.incident_vertex(proxy_mesh.opposite_halfedge(hid))]
        e_key = get_edge_key(edge)

        vid = split_halfedge(proxy_mesh, hid)
        boundary_vertices_id.append(vid)
        # labels_per_boundary_vertex.append(h_labels)
        # Add point to the subsets
        for l in h_labels:
            if l != outside_label:
                final_subsets[l].append(vid)
                boundary_vertices_subsets[l].append(boundary_v_id)

        # Does this edge have an associated stroke point?
        # print(e_key)

        if e_key in stroke_position_by_edge:
            # if len(stroke_position_by_edge[e_key]) == 1 or outside_label in h_labels:
                # stroke_position_by_boundary_vertex[vid] = stroke_position_by_edge[e_key][0]
            attracted_vertices_id.append(vid)
            target_vertex = np.mean(stroke_position_by_edge[e_key], axis = 0)
            attracted_vertices_target = np.row_stack([attracted_vertices_target, target_vertex])
            if outside_label in h_labels:
                attraction_is_mandatory.append(True)
            else:
                attraction_is_mandatory.append(False)

        in_out_labelling = np.append(in_out_labelling, np.any(np.array(h_labels) == outside_label))
        vertex_is_on_mirror = np.append(vertex_is_on_mirror, np.all(vertex_is_on_mirror[edge]))

        boundary_v_id += 1

    return boundary_vertices_id, boundary_vertices_subsets, final_subsets, np.array(attracted_vertices_id, dtype=int), attracted_vertices_target, np.array(attraction_is_mandatory, dtype=bool), in_out_labelling, vertex_is_on_mirror


def remove_islands_from_labelling(proxy_mesh, labels):
    new_labels = labels.copy()

    # Labelling based approach - analyze connected components of all regions
    edges, _ = get_edges(proxy_mesh)
    affinity_matrix_mesh = affinity_matrix_sparse(len(proxy_mesh.vertices()), edges, np.ones(len(edges)))

    unique_labels = np.unique(new_labels)


    for l in unique_labels:
        subset_l = np.flatnonzero(labels == l)
        sub_affinity_matrix = affinity_matrix_mesh[subset_l[:, None], subset_l]

        n_components, labels_components = connected_components(sub_affinity_matrix, directed=False)

        # print(f"label {l} has {n_components} connected components")

        for c in range(n_components):
            subset_c = np.flatnonzero(labels_components == c)
            component_affinity_matrix = sub_affinity_matrix[subset_c[:, None], subset_c].toarray()
            neighbors_count = np.count_nonzero(component_affinity_matrix, axis = 0)
            # If all neighbors count are < 3, this connected component has no area
            if np.max(neighbors_count) < 3:
                # Find a replacement label
                neighboring_labels = []
                for v in subset_l[subset_c]:
                    for vnid in proxy_mesh.circulate_vertex(v, mode='v'):
                        if new_labels[vnid] != l:
                            neighboring_labels.append(new_labels[vnid])
                if len(neighboring_labels) == 0:
                    print("couldn't replace island label")
                    replacement_label = l
                else:
                    replacement_options, counts = np.unique(neighboring_labels, return_counts=True)
                    replacement_label = replacement_options[np.argmax(counts)]
                    print(f"will replace a connected component ({len(subset_c)} vertices) of label {l} with label {replacement_label}")

                # Replace their label
                new_labels[subset_l[subset_c]] = replacement_label
            

    # F = get_faces(proxy_mesh)

    # already_seen_halfedges = set()

    # for hid in proxy_mesh.halfedges():
    #     opp_h = proxy_mesh.opposite_halfedge(hid)
    #     if hid in already_seen_halfedges or opp_h in already_seen_halfedges:
    #         continue
    #     edge_h = [proxy_mesh.incident_vertex(hid), proxy_mesh.incident_vertex(opp_h)]
    #     # Look at edge labels
    #     labels_h = new_labels[edge_h]
    #     if len(np.unique(labels_h)) == 1:
    #         label_h = labels_h[0]
    #         # Verify that at least one of the corner vertices from this edge has the same label
    #         corner_a = proxy_mesh.incident_vertex(proxy_mesh.next_halfedge(hid))
    #         corner_b = proxy_mesh.incident_vertex(proxy_mesh.next_halfedge(opp_h))

    #         labels_corners = new_labels[[corner_a, corner_b]]
    #         if label_h not in labels_corners:
    #             # This edge is an island, change its labels



    #     already_seen_halfedges.add(hid)
    #     already_seen_halfedges.add(opp_h)

    # vertices_queue = []
    # for vid in proxy_mesh.vertices():
    #     if proxy_mesh.is_vertex_at_boundary(vid):
    #         continue
    #     vertices_queue.append(vid)

    # print(new_labels.shape)

    # while len(vertices_queue) > 0:
    #     vid = vertices_queue.pop(0)
    #     label_v = new_labels[vid]
    #     # neighboring_labels = []
    #     neighbors = []
    #     for vnid in proxy_mesh.circulate_vertex(vid, mode='v'):
    #         neighbors.append(vnid)

    #     # for fid in proxy_mesh.circulate_vertex(vid, mode='f'):
    #     #     opp_edge = F[fid]
    #     #     opp_edge = opp_edge[opp_edge != vid]
    #     #     labels_opp_edge = new_labels[opp_edge]
    #     #     neighboring_labels.extend(labels_opp_edge)

    #     neighboring_labels = new_labels[neighbors]
    #     different_neighboring_labels = neighboring_labels[neighboring_labels != label_v]
    #     if len(neighboring_labels) - len(different_neighboring_labels) < 2:
    #         if len(different_neighboring_labels) > 0:
    #             new_labels[vid] = np.sort(different_neighboring_labels)[0]
    #             if len(neighboring_labels) - len(different_neighboring_labels) == 1:
    #                 vertices_queue.append(neighbors[np.flatnonzero(neighboring_labels == label_v).item()])
    #         else:
    #             print("couldn't replace island label")

    # Only remove 1-node islands
    # for vid in proxy_mesh.vertices():
    #     label_v = new_labels[vid]
    #     neighboring_labels = []
    #     for fid in proxy_mesh.circulate_vertex(vid, mode='f'):
    #         opp_edge = F[fid]
    #         opp_edge = opp_edge[opp_edge != vid]
    #         labels_opp_edge = new_labels[opp_edge]
    #         neighboring_labels.extend(labels_opp_edge)
    #     if label_v not in neighboring_labels:
    #         if len(neighboring_labels) > 0:
    #             new_labels[vid] = neighboring_labels[0]
    #         else:
    #             print("couldn't replace island label")

    return new_labels

def locally_refine_mesh(proxy_mesh, labels, models, subsets, boundary_vertices_id, max_error):

    # Local mesh refinement
    # For each face
    # - Is it of a uniform label? (all 3 vertices have the same label)
    # - Is it too big?
    # If yes, insert a vertex at its barycenter

    # Then make topology better with edge flips

    new_vertices = []

    for f in proxy_mesh.faces():
        v_indices = [vi for vi in proxy_mesh.circulate_face(f, mode='v')]
        if np.any(np.array(v_indices) >= len(labels)):
            continue
        if len(np.unique(labels[v_indices])) > 1:
            continue
        
        # if proxy_mesh.area(f) < mean_face_area:
        #     continue

        label_v = labels[v_indices][0]

        c = np.array(proxy_mesh.centre(f))
        # c = np.mean(V_opt[face_vertices(proxy_mesh, f)], axis = 0)
        err = models[label_v].compute_residuals(c, approx='first_order')

        if err < max_error:
            continue

        v_new = proxy_mesh.split_face_by_vertex(f)
        
        subsets[label_v].append(v_new)

        labels = np.append(labels, label_v)

        new_vertices.append(v_new)
    
    print("Inserted {} new vertices during local refinement".format(len(new_vertices)))

    # Edge flips
    for vi in new_vertices:
        # Check all corner edges
        for hn in proxy_mesh.circulate_vertex(vi, mode='h'):
            h_opp = proxy_mesh.next_halfedge(hn)
            # Ignore boundary vertices
            if proxy_mesh.incident_vertex(h_opp) in boundary_vertices_id:
                continue
            if proxy_mesh.is_halfedge_at_boundary(h_opp):
                continue
            # Is it optimal to flip it?
            if min_angle_energy_delta(proxy_mesh, h_opp) > 0 :
                proxy_mesh.flip_edge(h_opp)

    return labels, subsets