import sys
from enum import IntEnum, auto
import time
import multiprocessing
import concurrent.futures
import numpy as np
import json
import networkx as nx

import polyscope as ps

# Add path to pygco
sys.path.append('../external/pygco')
import pygco

from main.implicit_polynomial_surface_fit import least_squares_fit_implicit, least_squares_fit_gradient_one
from main.propose_models import fit_model_to_nodes, propose_new_models_by_merge, refit_models, propose_models_lower_degree, propose_new_model_at, sample_subsets_by_geodesic_distance, sample_subsets_randomly, propose_models_for_subsets, sample_subsets_by_clustering, propose_new_models_by_split, find_partition
from main.proxy_graph import normalized_min_cut

from utils.labelling import *
from utils.visualize import plot_energy, polygonize_models, visualize, visualize_labelling, visualize_models_by_projection, ps_clear, visualize_nodes
from utils.segmentation_to_mesh import mesh_by_projection
from utils.math import bounding_box_diagonal, get_bounds


# Initialization methods
class InitialProposalStratregy(IntEnum):
    RANDOM = 0
    GEODESIC_SMOOTH = 1
    CLUSTERING = 2
    MULTI_CLUSTERING = 3
    MULTI_CLUSTERING2 = 4

# error classes
class SegmentFitError(Exception):
    def __init__(self, msg=''):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)

class NotInitialized(SegmentFitError):
    pass

class IndexOutOfBoundError(SegmentFitError):
    pass

class ErrorInParameters(SegmentFitError):
    pass

class InputParameters:
    def __init__(self, dist_approx, w_all, w_fidelity, w_smooth, w_label, delta, delta_ratio, fidelity_normalizer, smooth_normalizer, lambda_reg, w_proxy_vertices_fit, c_layer, w_normals_fit):
        self.dist_approx = dist_approx
        self._w_all = w_all
        self._w_fidelity = w_fidelity
        self._w_smooth = w_smooth
        self._w_label = w_label
        self._delta = delta
        self._delta_ratio = delta_ratio
        self._fidelity_normalizer = fidelity_normalizer
        self._smooth_normalizer = smooth_normalizer
        self._label_normalizer = 1
        self.lambda_reg = lambda_reg
        self.w_proxy_vertices_fit = w_proxy_vertices_fit
        self.c_layer = c_layer
        self.w_normals_fit = w_normals_fit

    @property
    def w_smooth(self):
        return self._w_all * self._w_smooth / self._smooth_normalizer

    @property
    def w_fidelity(self):
        return self._w_all * self._w_fidelity / self._fidelity_normalizer

    @property
    def w_label(self):
        return self._w_all * self._w_label / self._label_normalizer

    def set_label_normalizer(self, value):
        self._label_normalizer = value

    def to_dict(self):
        return {
            'dist_approx': self.dist_approx,
            'w_all': self._w_all,
            'w_fidelity': self._w_fidelity,
            'w_smooth': self._w_smooth,
            'w_label': self._w_label,
            'fidelity_normalizer': self._fidelity_normalizer,
            'smooth_normalizer': self._smooth_normalizer,
            'delta': self._delta,
            'delta_bb_frac': self._delta_ratio,
            'lambda_reg': self.lambda_reg,
            'c_layer': self.c_layer,
            'w_normals_fit': self.w_normals_fit
        }

class SegmentFitLogs:

    def __init__(self):
        self.models_refitted_ratios = []
        self.labelling_times = []
        self.fitting_times = []

    def record_models_refitted(self, nb_refitted, total_nb):
        self.models_refitted_ratios.append(nb_refitted / total_nb)

    def get_initialization_time(self):
        t = 0
        if len(self.fitting_times) > 0:
            t += self.fitting_times[0]
        if len(self.labelling_times) > 0:
            t += self.labelling_times[0]
        return t

    def get_iterations_time(self):
        total_time = 0
        for i in range(1, len(self.labelling_times)):
            total_time += self.labelling_times[i]
        for i in range(1, len(self.fitting_times)):
            total_time += self.fitting_times[i]
        return total_time

    def get_runtime(self):
        total_time = 0
        for t in self.labelling_times:
            total_time += t
        for t in self.fitting_times:
            total_time += t
        return total_time

class SegmentFit:

    # def __init__(self, nodes, graph_edges, graph_edges_weight, proxy, dist_approx, w_fidelity, w_smooth, w_label, delta):
    def __init__(self, proxy, dist_approx, w_all, w_fidelity, w_smooth, w_label, delta_bb_frac, lambda_reg, w_proxy_vertices_fit, w_normals_fit):
        # Proxy object that stores all necessary informations
        self.proxy = proxy

        # Set a "maximum deviation distance" beyond which we clamp the deviation
        # (this is to avoid problems in graph cut algorithm due to risk of overflowing integer values)
        self.max_dist = 10 * delta_bb_frac * bounding_box_diagonal(self.proxy.data_points)
        # print("max dist = {}".format(self.max_dist))
        print("max dist squared = {}".format(self.max_dist ** 2))

        c_layer = bounding_box_diagonal(self.proxy.data_points) * 0.04

        self.logs = SegmentFitLogs()

        self.update_parameters(w_all, w_fidelity, w_smooth, w_label, delta_bb_frac, dist_approx, lambda_reg, w_proxy_vertices_fit, c_layer, w_normals_fit)

        self.initialize()

        # Multithreading executor (to compute all per-model energies at once)
        self.executor = concurrent.futures.ThreadPoolExecutor(multiprocessing.cpu_count())


    def update_parameters(self, w_all=None, w_fidelity=None, w_smooth=None, w_label=None, delta_bb_frac=None, dist_approx=None, lambda_reg=None, w_proxy_vertices_fit=None, c_layer=None, w_normals_fit=None):
        # TODO: maybe the parameters shouldn't be optional? This will fail if self.params is not initialized
        if w_all is None:
            w_all = self.params._w_all
        if w_fidelity is None:
            w_fidelity = self.params._w_fidelity
        if w_smooth is None:
            w_smooth = self.params._w_smooth
        if w_label is None:
            w_label = self.params._w_label
        # Small distance delta
        if delta_bb_frac is None:
            delta = self.params._delta
            delta_ratio = self.params._delta_ratio
        else:
            delta = delta_bb_frac * bounding_box_diagonal(self.proxy.data_points)
            delta_ratio = delta_bb_frac
            # delta = delta_bb_frac * np.max(np.max(self.proxy.data_points, axis = 0) - np.min(self.proxy.data_points, axis = 0))
        if dist_approx is None:
            dist_approx = self.params.dist_approx
        if lambda_reg is None:
            lambda_reg = self.params.lambda_reg
        if w_proxy_vertices_fit is None:
            w_proxy_vertices_fit = self.params.w_proxy_vertices_fit
        if c_layer is None:
            c_layer = self.params.c_layer
        if w_normals_fit is None:
            w_normals_fit = self.params.w_normals_fit

        # Compute normalizers for each energy
        fidelity_normalizer = self.proxy.get_total_points_count() * delta**2
        smooth_normalizer = np.sum(self.proxy.edges_weight)

        # w_normalizer = len(np.flatnonzero(np.logical_not(self.proxy.node_has_data)))
        # w_normals_fit = w_normals_fit / w_normalizer
        # print("w_normals_fit = ", w_normals_fit)

        # Update parameters
        self.params = InputParameters(
            dist_approx,
            w_all, w_fidelity, w_smooth, w_label,
            delta, delta_ratio, fidelity_normalizer, smooth_normalizer,
            lambda_reg, w_proxy_vertices_fit, c_layer, w_normals_fit)

        # print("w_f = {}, w_s = {}".format(self.params.w_fidelity, self.params.w_smooth))

    def initialize(self, improve_existing=False):

        if improve_existing and len(self.models_progression) > 0:
            print("Starting from previous result!")
            # Add the previously assigned models to the proposals
            assigned_models_idx = np.unique(self.labels_progression[-1])

            # Set a penalty cost for all previously unassigned models
            for idx, m in enumerate(self.models_progression[-1]):
                if idx not in assigned_models_idx:
                    m.set_penalty(1)
            # assigned_models = (np.array(self.models_progression[-1])[assigned_models_idx]).tolist()
            self.new_models_proposal = self.models_progression[-1] + self.new_models_proposal # add potential new proposals at the end
            self.initial_labels = self.labels_progression[-1]
        else:
            self.new_models_proposal = None
            self.initial_labels = None

        # Clear data
        self.labels_progression = []
        self.models_progression = []
        self.energy_progression = []

        # self.current_fidelity_energy_matrix = None

        self.fidelity_energy_penalties = None

        self.converged = False

    def initialize_models(
        self,
        L0, sample_ratio_bounds,
        initial_proposal_strategy=InitialProposalStratregy.MULTI_CLUSTERING,
        init_lambda_factor=1,
        init_normals_factor=1,
        random_seed=None, 
        degree=4):
        time_fitting_start = time.perf_counter()
        if initial_proposal_strategy == InitialProposalStratregy.RANDOM:
            pts_idx_subsets = sample_subsets_randomly(
                self.proxy,
                L0,
                sample_ratio_bounds,
                random_seed=random_seed
            )
        elif initial_proposal_strategy == InitialProposalStratregy.GEODESIC_SMOOTH:
            pts_idx_subsets = sample_subsets_by_geodesic_distance(
                self.proxy,
                L0, sample_ratio_bounds,
                random_seed
            )
        elif initial_proposal_strategy == InitialProposalStratregy.CLUSTERING:
            pts_idx_subsets = sample_subsets_by_clustering(self.proxy, L0, random_seed)
        elif initial_proposal_strategy == InitialProposalStratregy.MULTI_CLUSTERING:
            pts_idx_subsets = []
            Ls = [L0 - i * 10 for i in range(L0 // 10)]
            # Ls.append(5)
            for L in Ls:
                pts_idx_subsets.extend(sample_subsets_by_clustering(self.proxy, L, random_seed))
        elif initial_proposal_strategy == InitialProposalStratregy.MULTI_CLUSTERING2:
            pts_idx_subsets = []
            Ls = [L0 - i * 10 for i in range(L0 // 10 - 1)]
            for L in Ls:
                pts_idx_subsets.extend(sample_subsets_by_clustering(self.proxy, L, random_seed))
        else:
            raise ErrorInParameters(f"Initial proposal strategy {initial_proposal_strategy} is unknown.")

        models = propose_models_for_subsets(
            self.proxy,
            pts_idx_subsets,
            init_lambda_factor * self.params.w_normals_fit,
            init_normals_factor * self.params.lambda_reg,
            degree=degree)
        
        # self.models_progression.append(models)
        self.new_models_proposal = models
        self.fidelity_energy_penalties = np.zeros((self.proxy.nodes_count, len(models)))

        # Update label normalizer based on initial models
        # initial_label_cost = 0
        # for m in models:
        #     initial_label_cost += m.cost

        # self.params.set_label_normalizer(initial_label_cost)

        self.logs.fitting_times.append(time.perf_counter() - time_fitting_start)

        return pts_idx_subsets

    def initialize_with_state(self, labels, models, fidelity_energy_penalties=None):
        self.new_models_proposal = models
        self.initial_labels = labels
        if fidelity_energy_penalties is None:
            self.fidelity_energy_penalties = np.zeros((self.proxy.nodes_count, len(self.new_models_proposal)))
        else:
            self.fidelity_energy_penalties = fidelity_energy_penalties

        # We are currently in a converged state:
        # Subsequent user interactions (if any) will drive us out of this state
        self.converged = True

    def optimize_labelling(self):
        time_labelling_start = time.perf_counter()
        # if len(self.models_progression) < 1:
        if self.new_models_proposal is None:
            raise(NotInitialized('SegmentFit must be initialized with some models first.'))

        # models = self.models_progression[-1]
        models = self.new_models_proposal

        nb_labels = len(models)

        # Build the "smooth" matrix (1 - Id) of dimension (nb_labels, nb_labels)
        smooth = (1 - np.eye(nb_labels)).astype(np.float)

        # Build the unary term matrix
        # unary = self.params.w_fidelity * self.proxy.compute_fidelity_energy_matrix(models, self.max_dist ** 2, dist_approx=self.params.dist_approx)
        time0 = time.perf_counter()
        # self.fidelity_energy_matrix()
        unary = self.params.w_fidelity * self.fidelity_energy_matrix()
        time1 = time.perf_counter()

        print("time to compute unary matrix = ", time1 - time0)

        print("Unary energy max =", np.max(unary))

        scaled_edge_weights = self.proxy.edges_weight * self.params.w_smooth

        print("Edge weights, min = {}, max = {}".format(np.min(scaled_edge_weights), np.max(scaled_edge_weights)))

        if len(self.labels_progression) > 0:
            init_labels = self.labels_progression[-1]
        else:
            if self.initial_labels is not None:
                print("Initialized labels with a previous result.")
                init_labels = self.initial_labels
            else:
                init_labels = None

        # Label costs
        N_labels = len(models)

        # label_costs = w_labels * np.array([model.degree for model in models])
        # label_costs = None
        # TODO: should we take the regularization cost as label cost?
        label_costs = self.params.w_label * np.array([model.cost for model in models])
        # print(label_costs)

        print("Label costs, min = {}, max = {}".format(np.min(label_costs), np.max(label_costs)))

        if init_labels is not None:
            print("Starting with energy:")
            start_energy = self.compute_energy(unary, init_labels, scaled_edge_weights, label_costs=label_costs)  
            if len(self.energy_progression) > 0:
                # Compare this energy with last energy
                dE_fit = self.energy_progression[-1] - start_energy
                print("dE_fit = {}".format(dE_fit))
                if dE_fit < 0:
                    print("Energy increase after fit")

        if unary.shape[1] > 1:
            time0 = time.perf_counter()
            print("start graph cut")
            # Graph cut
            # TODO: debug pygco down_weight_factor. When set to None, the graph cut seems to not take smooth cost into account
            new_labels, energy = pygco.cut_general_graph(
                # len(self.proxy.nodes),
                # len(models),
                self.proxy.edges, scaled_edge_weights,
                unary,
                # unary_data, unary_rows, unary_cols,
                smooth, label_costs,
                n_iter=-1, algorithm="expansion", init_labels=init_labels, down_weight_factor=1)
            print("end graph cut")
            time1 = time.perf_counter()
            print("graph cut runtime = {}".format(time1 - time0))

            print("Energy =", energy)
        else:
            print("Only 1 model left, labelling has to be this model for everything")
            new_labels = np.zeros(len(unary), dtype=int)

        print("Ending with energy:")
        total_energy = self.compute_energy(unary, new_labels, scaled_edge_weights, label_costs=label_costs)

        selected_labels = np.unique(new_labels)
        print("Selected labels =", selected_labels)
        models_degree = [m.degree for idx, m in enumerate(models) if idx in selected_labels ]
        print("Models degree =", models_degree)

        # print("Degree of remaining models =", [models[idx].degree for idx in np.unique(new_labels)])
        # Is it converged?
        if self.is_converged(new_labels):
            print("Segment and fit converged.")
            self.converged = True

            # Clear new models proposals
            self.new_models_proposal = []
        # else:
        self.energy_progression.append(total_energy)

        self.labels_progression.append(new_labels)
        self.models_progression.append(models)

        self.shed_unassigned_models()

        # assigned_labels, reindexed_labels = np.unique(new_labels, return_inverse=True)

        # # Keep only assigned models
        # self.models_progression.append(np.array(models)[assigned_labels].tolist())

        # # Reindex labels to match the models that are kept
        # self.labels_progression.append(reindexed_labels)
        # self.current_fidelity_energy_matrix = np.take(self.current_fidelity_energy_matrix, assigned_labels, axis=1)
        # self.fidelity_energy_penalties = np.take(self.fidelity_energy_penalties, assigned_labels, axis=1)
        
        # self.labels_progression.append(new_labels)
        # self.models_progression.append(models)


        self.logs.labelling_times.append(time.perf_counter() - time_labelling_start)

    def propose_models(self, max_model_degree, propose_lower_degree=False, propose_new=False, propose_new_by_split=False, propose_new_by_merge=False):
        if (len(self.labels_progression) < 1) or (len(self.models_progression) < 1):
            raise NotInitialized('SegmentFit must be initialized with some models and optimal labels first.')

        time_fitting_start = time.perf_counter()

        # WARNING: Here we expect that the index of each model in the list correspond to an
        # ASSIGNED label (ie, all model indices are present in self.labels_progression[-1])

        subsets, unique_labels = get_subsets_from_labels(self.labels_progression[-1])

        refitted = refit_models(
            self.proxy,
            subsets,
            # unique_labels,
            self.models_progression[-1],
            self.params.w_normals_fit,
            self.params.lambda_reg)

        print(f"Refitted models = {len(refitted)}, cols in matrix = {self.fidelity_energy_penalties.shape[1]}")

        new_models, parent_labels = self.accept_refitted_proposals(refitted, subsets)

        if propose_lower_degree:
            new_proposals, original_labels = propose_models_lower_degree(
                self.proxy,
                subsets,
                # unique_labels,
                self.models_progression[-1],
                self.params.w_normals_fit,
                self.params.lambda_reg
            )
            new_models.extend(new_proposals)
            parent_labels.extend(original_labels)

        # if self.proxy.mirror:
        #     symmetric_proposals = propose_symmetric_models(
        #         self.proxy,
        #         subsets,
        #         unique_labels,
        #         self.models_progression[-1],
        #         self.params.c_layer,
        #         self.params.lambda_reg
        #     )

        #     all_new_proposals.extend(symmetric_proposals)

        if propose_new:
            # Find point with worse error
            energy_by_node = self.get_fidelity_energy_by_node(-1)
            idx_worse = np.argmax(energy_by_node)
            
            new_model = propose_new_model_at(self.proxy, idx_worse, self.params.c_layer, self.params.lambda_reg, 0.1)

            if new_model is not None:
                new_models.append(new_model)
                parent_labels.append(-1)
                # all_new_proposals.append(new_model)
                # self.fidelity_energy_penalties = np.column_stack([
                #     self.fidelity_energy_penalties,
                #     np.zeros((self.fidelity_energy_penalties.shape[0], 1))
                # ])

        if propose_new_by_merge:
            merged_models, merged_labels = propose_new_models_by_merge(
                self.proxy,
                self.labels_progression[-1],
                min(3, len(unique_labels)),
                self.params.lambda_reg,
                self.params.w_normals_fit,
                self.models_progression[-1],
                max_model_degree)
            new_models.extend(merged_models)
            parent_labels.extend(merged_labels)
            # all_new_proposals.extend(merged_models)
            # for merged_l in merged_labels:
            #     self.fidelity_energy_penalties = np.column_stack([
            #             self.fidelity_energy_penalties,
            #             np.max(self.fidelity_energy_penalties[:, merged_l], axis = 1)
            #         ])

        # print(f"new proposals before split = {len(all_new_proposals)}")
        if propose_new_by_split:

            for l in range(len(subsets)):
                # TODO: fix input of self.compute_energy_for_model(...)

                # if not model_was_refit[idx]:
                #     # If we kept the model from previous iteration, no need to propose new models for it
                #     continue

                # subset_id = get_subset_id_from_label(unique_labels, l)
                subset_to_split = subsets[l]

                # Compute energy for old model
                # pts_subset, _ = self.proxy.get_data_for_nodes(subset_to_split)
                
                energy_m = self.compute_energy_for_model(new_models[l], subset_to_split)
                # print("previous model total energy = {}".format(energy_m))

                new_proposals = propose_new_models_by_split(
                    self.proxy,
                    subset_to_split,
                    # new_models[l],
                    self.params.lambda_reg,
                    self.params.w_normals_fit,
                    refitted[l].degree
                    # 4
                )

                # if e_new_total < energy_m:
                    # print("Accepting 2 new model proposals")
                # all_new_proposals.extend(new_proposals)
                # self.fidelity_energy_penalties = np.column_stack([
                #     self.fidelity_energy_penalties,
                #     self.fidelity_energy_penalties[:, l]
                # ])
                new_models.extend(new_proposals)
                parent_labels.extend([l] * len(new_proposals))

        # print(f"new proposals after split = {len(all_new_proposals)}")
        # Prevent adding duplicate models with new proposals

        # for model in all_new_proposals:
        #     if model not in new_models:
        #         new_models.append(model)
        #     else:
        #         print("Prevented model duplication.")

        # new_models.extend(all_new_proposals)

        # Update models proposals
        # self.fidelity_energy_penalties = np.empty((self.fidelity_energy_penalties.shape[0], 0))
        # - First put the refitted models
        self.new_models_proposal = refitted
        # for model, parent_label in zip(refitted, unique_labels):
        #     self.add_model_proposal(model, parent_label)
        # - Then add the new proposals
        for model, parent_label in zip(new_models, parent_labels):
            self.add_model_proposal(model, parent_label)

        # self.fidelity_energy_penalties = np.column_stack([self.fidelity_energy_penalties, np.zeros((self.fidelity_energy_penalties.shape[0], len(new_models) - self.fidelity_energy_penalties.shape[1]))])

        self.logs.fitting_times.append(time.perf_counter() - time_fitting_start)

        print("Models available = {}, refitted = {}, proposed new = {}".format(
            len(self.new_models_proposal),
            len(refitted),
            len(new_models)))

    def accept_refitted_proposals(self, refitted, subsets):

        model_was_refit_counter = 0

        # Here again we assume that all models in this list are assigned to some nodes
        new_models = []
        parent_labels = []

        def _accept(refitted_list, new_models_list, labels_list, subset, label):
            old_m = self.models_progression[-1][label]
            new_m = refitted_list[label]

            # if self.params.w_proxy_vertices_fit > 0:
            #     pts_subset, _ = self.proxy.get_data_for_nodes(subset)
            #     W = np.ones(len(pts_subset))
            #     proxy_vertices = self.proxy.nodes_positions[subset]
            #     pts_subset = np.row_stack([pts_subset, proxy_vertices])
            #     W = np.append(W, self.params.w_proxy_vertices_fit * np.ones(len(proxy_vertices)))

            new_energy = self.compute_energy_for_model(new_m, subset)
            previous_energy = self.compute_energy_for_model(old_m, subset)

            # If we decrease energy, update the model
            if new_energy >= previous_energy:
                print("Keeping previous model {} instead of re-fit.".format(label))
                print(f"Old energy = {previous_energy}, New energy = {new_energy}")
                refitted_list[label] = old_m # RESTORE THE OLD MODEL

                # Still add the new model as a proposal
                new_models_list.append(new_m)
                labels_list.append(label)

            else:
                print("Refitted model {}".format(label))
                # model_was_refit_counter += 1

        futures = []
        for l in range(len(subsets)):
            args = (_accept,
                    refitted,
                    new_models,
                    parent_labels,
                    subsets[l],
                    l)
            futures.append(self.executor.submit(*args))
        concurrent.futures.wait(futures)

        # # Decide for each of the refitted models, whether we accept it or prefer keeping the previous one
        # for l in range(len(subsets)):
        #     new_m = refitted[l]
        #     old_m = self.models_progression[-1][l]

        #     # subset_l = subsets[get_subset_id_from_label(unique_labels, l)]
        #     subset_l = subsets[l]
        #     # print(subset_l)
        #     pts_subset, _ = self.proxy.get_data_for_nodes(subset_l)

        #     W = np.ones(len(pts_subset))

        #     if self.params.w_proxy_vertices_fit > 0:
        #         proxy_vertices = self.proxy.nodes_positions[subset_l]
        #         pts_subset = np.row_stack([pts_subset, proxy_vertices])
        #         W = np.append(W, self.params.w_proxy_vertices_fit * np.ones(len(proxy_vertices)))

        #     # new_energy = self.compute_energy_for_model(new_m, pts_subset, W)
        #     new_energy = self.compute_energy_for_model(new_m, subset_l)
        #     # print("New energy = {}".format(new_energy))
        #     # previous_energy = self.compute_energy_for_model(old_m, pts_subset, W)
        #     previous_energy = self.compute_energy_for_model(old_m, subset_l)

        #     # print("Previous energy = {}".format(previous_energy))

        #     # If we decrease energy, update the model
        #     if new_energy >= previous_energy:
        #         print("Keeping previous model {} instead of re-fit.".format(l))
        #         print(f"Old energy = {previous_energy}, New energy = {new_energy}")
        #         refitted[l] = old_m

        #         # Still add the new model as a proposal
        #         new_models.append(new_m)
        #         parent_labels.append(l)

        #     else:
        #         print("Refitted model {}".format(l))
        #         model_was_refit_counter += 1

        self.logs.record_models_refitted(model_was_refit_counter, len(subsets))

        return new_models, parent_labels


    def add_model_proposal(self, model, source_label):
        if model not in self.new_models_proposal:
            self.new_models_proposal.append(model)
            if isinstance(source_label, list) :
                # Take the max            
                penalties = np.max(self.fidelity_energy_penalties[:, source_label], axis = 1)
            elif source_label > -1:
                penalties = self.fidelity_energy_penalties[:, source_label]
            else:
                penalties = np.zeros((self.proxy.nodes_count, 1))
            self.fidelity_energy_penalties = np.column_stack([
                self.fidelity_energy_penalties,
                penalties
            ])
        else:
            print("Prevented model duplication.")

    def shed_unassigned_models(self):
        assigned_labels, reindexed_labels = np.unique(self.labels_progression[-1], return_inverse=True)

        self.labels_progression[-1] = reindexed_labels

        # Keep only assigned models
        self.models_progression[-1] = np.array(self.models_progression[-1])[assigned_labels].tolist()

        # Reindex labels to match the models that are kept
        # self.current_fidelity_energy_matrix = np.take(self.current_fidelity_energy_matrix, assigned_labels, axis=1)
        self.fidelity_energy_penalties = np.take(self.fidelity_energy_penalties, assigned_labels, axis=1)
    
    def propose_here(self, proxy_vertices_selected):
        # proxy_vertices_selected = proxy_vertices_selected[self.proxy.proxy_vertex_is_node[proxy_vertices_selected]]
        # nodes_selected = self.proxy.vertex_idx_to_node_idx_map[proxy_vertices_selected]
        nodes_selected = np.unique(self.proxy.closest_node_by_vertex[proxy_vertices_selected])

        # import polyscope as ps

        # ps.register_point_cloud("nodes selected", self.proxy.nodes_positions[nodes_selected])

        cent, bounds = get_bounds(self.proxy.nodes_positions[nodes_selected], padding=0.5)

        old_labels = self.labels_progression[-1][nodes_selected]
        # old_models = np.array(self.models_progression[-1])[old_labels]
        # degree = np.max([m.degree for m in old_models])

        # pts_subset, _ = self.proxy.get_data_for_nodes(nodes_selected)

        # ps.register_point_cloud("pts selected", pts_subset)
        # ps.show()

        # for d in range(1,5):
        new_model = fit_model_to_nodes(
            self.proxy,
            nodes_selected,
            self.params.lambda_reg,
            self.params.w_normals_fit,
            degree=4,
            min_nb_pts=3
        )

        if new_model is None:
            print("[INTERACTION] couldn't propose a new model, not enough points")
            return

        # new_model.set_penalty(-new_model.cost)
        # V, F = new_model.polygonize(cent, bounds)

        # ps.register_surface_mesh(f"proposal {np.random.rand()}", V, F, enabled=False)

        new_model_label = len(self.models_progression[-1])
        self.models_progression[-1].append(new_model)

        new_model_penalties = np.zeros((self.fidelity_energy_penalties.shape[0], 1))

        self.fidelity_energy_penalties = np.column_stack([
            self.fidelity_energy_penalties,
            new_model_penalties
        ])

        # Old model assigned to these nodes: penalize it
        all_other_labels = np.arange(new_model_label)
        self.penalize_models(nodes_selected, all_other_labels)

        # Assign this model as initial labels
        self.labels_progression[-1][nodes_selected] = new_model_label

        # Some models may have become unassigned following the relabelling
        self.shed_unassigned_models()

        self.converged = False

        # ps.show()

    def propose_merge(self, selected_vertices):
        # User interaction: merge
        # - Find the corresponding regions to merge (a set of nodes), given the selected vertices
        nodes_of_merge = []
        nodes_selected = np.unique(self.proxy.closest_node_by_vertex[selected_vertices])
        labelling = self.labels_progression[-1]
        labels_selected = np.unique(labelling[nodes_selected])
        node_is_selected = np.zeros(len(labelling), dtype=bool)
        node_is_selected[nodes_selected] = True
        # - For each label, check if it has multiple connected components
        # - If yes, take only the nodes of connected component(s) that have at least 1 selected vertex
        for l in labels_selected:
            subset_l = np.flatnonzero(labelling == l)
            n_components, labels_components = self.proxy.connected_components(subset_l)

            if n_components == 1:
                nodes_of_merge.extend(subset_l)
            else:
                for l_c in range(n_components):
                    nodes_c = subset_l[labels_components == l_c]
                    # nodes_c_selected = nodes_c[node_is_selected[nodes_c]]
                    if np.any(node_is_selected[nodes_c]):
                        print(f"There are selected vertices in component {l_c}")
                        nodes_of_merge.extend(nodes_c)
                    # ps.register_point_cloud(f"comp {l_c}", self.proxy.nodes_positions[nodes_c])
                    
        # ps.register_point_cloud("nodes selected", self.proxy.nodes_positions[nodes_of_merge])
        # # ps.show()
        # pts_subset, _ = self.proxy.get_data_for_nodes(nodes_of_merge)

        # ps.register_point_cloud("pts selected", pts_subset)
        # ps.show()

        # - Propose a model for the selected nodes
        new_model = fit_model_to_nodes(
            self.proxy,
            nodes_of_merge,
            self.params.lambda_reg,
            self.params.w_normals_fit,
            4
        )

        if new_model is None:
            print("[INTERACTION] couldn't propose a new model, not enough points")
            return

        # center, span = get_bounds(self.proxy.nodes_positions[nodes_of_merge], padding=0.5)
        # V, F = new_model.polygonize(center, span)
        # ps.register_surface_mesh("new model", V, F)
        # ps.show()

        new_model_label = len(self.models_progression[-1])
        self.models_progression[-1].append(new_model)

        new_model_penalties = np.max(self.fidelity_energy_penalties[:, labels_selected], axis = 1)

        self.fidelity_energy_penalties = np.column_stack([
            self.fidelity_energy_penalties,
            new_model_penalties
        ])

        # Old model assigned to these nodes: penalize it
        all_other_labels = np.arange(new_model_label)
        self.penalize_models(nodes_of_merge, all_other_labels)
        # self.penalize_models(nodes_selected, all_other_labels)

        # Assign this model as initial labels
        self.labels_progression[-1][nodes_of_merge] = new_model_label
        # self.labels_progression[-1][nodes_selected] = new_model_label

        # Some models may have become unassigned following the relabelling
        self.shed_unassigned_models()

        self.converged = False

    def propose_after_new_stroke(self, new_stroke_points):
        nodes_covered = self.proxy.add_stroke(new_stroke_points)

        # Find zone covered by the region containing the newly added stroke
        old_labels = np.unique(self.labels_progression[-1][nodes_covered])
        print(old_labels)
        
        region = np.flatnonzero(np.isin(self.labels_progression[-1], old_labels))

        region_labels = find_partition(self.proxy, region, N=2)

        # ps_region = ps.register_point_cloud("region", self.proxy.nodes_positions[region])
        # ps_region.add_scalar_quantity("labelling", region_labels)
        # ps.show()

        for l in np.unique(region_labels):
            sub_region = region[region_labels == l]
            # pts_subset, _ = self.proxy.get_data_for_nodes(sub_region)
            # ps.register_point_cloud("pts", pts_subset)
            # - Propose a model for the sub_region nodes
            new_model = fit_model_to_nodes(
                self.proxy,
                sub_region,
                self.params.lambda_reg,
                self.params.w_normals_fit,
                4
            )

            if new_model is None:
                print("[INTERACTION] couldn't propose a new model, not enough points")
                continue

            # center, span = get_bounds(self.proxy.nodes_positions[sub_region], padding=0.5)
            # V, F = new_model.polygonize(center, span)
            # ps.register_surface_mesh("new model", V, F)
            # ps.show()

            new_model_label = len(self.models_progression[-1])
            self.models_progression[-1].append(new_model)

            new_model_penalties = np.max(self.fidelity_energy_penalties[:, old_labels], axis = 1)

            self.fidelity_energy_penalties = np.column_stack([
                self.fidelity_energy_penalties,
                new_model_penalties
            ])

            # Old model assigned to these nodes: penalize it
            self.penalize_models(sub_region, old_labels)

            # Assign the new models as initial labels
            self.labels_progression[-1][sub_region] = new_model_label

        # Some models may have become unassigned following the relabelling
        self.shed_unassigned_models()

        self.converged = False


    
    def penalize_models(self, nodes, labels_idx):
        self.fidelity_energy_penalties[np.repeat(nodes, len(labels_idx)), np.tile(labels_idx, len(nodes))] = self.max_dist**2

    def update_stroke_sharpness(self, sharpness_by_segment):
        if not self.proxy:
            print("Error: Proxy is undefined.")
            return

        weight_diff = self.proxy.update_stroke_sharpness(sharpness_by_segment)

        if weight_diff is not None and len(self.models_progression) > 0:
            # Inspect last result to "split" segments that span across a "sharper" stroke
            # An edge crosses a sharper stroke if its weight diff is a positive value
            edges_with_lower_weight = np.flatnonzero(weight_diff > 0)

            last_labelling = self.labels_progression[-1]
            last_models = self.models_progression[-1]
            edges = self.proxy.edges

            all_conflicting_labels = []
            conflicting_edges_idx_per_label = {}

            for e_idx in edges_with_lower_weight:
                # Check label of endpoint vertices
                endpoint_labels = last_labelling[edges[e_idx]]

                if endpoint_labels[0] == endpoint_labels[1]:
                    all_conflicting_labels.append(endpoint_labels[0])
                    # all_conflicting_edges_idx.append(e_idx)
                    if endpoint_labels[0] not in conflicting_edges_idx_per_label:
                        conflicting_edges_idx_per_label[endpoint_labels[0]] = []
                    conflicting_edges_idx_per_label[endpoint_labels[0]].append(e_idx)
            conflicting_labels, indices = np.unique(all_conflicting_labels, return_index=True)
            # conflicting_edges = np.array(all_conflicting_edges_idx)[indices]

            print("conflicting labels =", conflicting_labels)

            new_per_node_label = np.zeros(self.proxy.nodes_count)

            debug_label_idx = 1

            new_models = []

            # for l, e_idx in zip(conflicting_labels, conflicting_edges):
            for l in conflicting_labels:
                conflicting_edges_idx = conflicting_edges_idx_per_label[l]

                # Split the subset of nodes labelled with l into 2 sets
                # - Indices subset
                indices_subset_l = np.flatnonzero(last_labelling == l)

                # Create graph
                G = nx.Graph()

                for e_idx, e in enumerate(self.proxy.edges):
                    # if the edge connects vertices from the current subset
                    if np.any([np.any(e[0] == indices_subset_l), np.any(e[1] == indices_subset_l)]):
                        if e_idx in conflicting_edges_idx:
                            weight = 0.1 * self.proxy.edge_length(e_idx)
                            # weight = 0.1 * self.proxy.edges_weight[e_idx]
                        else:
                            weight = 100 * self.proxy.edge_length(e_idx)
                            # weight = 100 * self.proxy.edges_weight[e_idx]
                        print("Adding edge {}-{}, weight = {}".format(e[0], e[1], weight))
                        G.add_edge(e[0],e[1],weight=weight)

                set_A, set_B = normalized_min_cut(G)
                # TODO: replace by spectral clustering N=2?

                print("Label {}, subsets = {} and {}".format(l, set_A, set_B))

                # Graph cut
                # sets = list(subsets[0]), list(subsets[1])
                sets = [set_A, set_B]

                # Determine which set has lowest fitting error with model_l
                model_l = last_models[l]

                nodes_idx_sets = []
                errors_sets = []

                for nodes_idx in sets:
                    # Maybe ignore tiny sets?
                    # nodes_idx = indices_subset_l[s_indices]
                    pts, _ = self.proxy.get_data_for_nodes(nodes_idx)

                    error = self.compute_energy_for_model(model_l, nodes_idx)
                    error /= len(nodes_idx)
                    errors_sets.append(error)

                    nodes_idx_sets.append(nodes_idx)

                    print("Error = {} ({} pts)".format(error, len(nodes_idx)))

                    # Fit a new model to those nodes and add it to the list of models
                    normal_cstr_points, normal_cstr_normals = self.proxy.get_proxy_data_for_nodes(nodes_idx)

                    new_model = least_squares_fit_gradient_one(
                        pts,
                        normal_cstr_points,
                        normal_cstr_normals,
                        w_normals=self.params.w_normals_fit,
                        lambda_reg=self.params.lambda_reg, degree=model_l.degree)

                    # new_model = least_squares_fit_implicit(
                    #     pts,
                    #     normals,
                    #     lambda_reg=self.params.lambda_reg, degree=model_l.degree
                    #     )

                    center, span = get_bounds(pts, padding=0.01)
                    new_models.append(new_model.polygonize(center, span))
                    self.new_models_proposal.append(new_model)

                worse_fitted_set_idx = np.argmax(errors_sets)
                best_fitted_set_idx = np.argmin(errors_sets)

                # new_per_node_label[s] = 10
                # new_per_node_label[t] = 11

                new_per_node_label[nodes_idx_sets[worse_fitted_set_idx]] = debug_label_idx
                new_per_node_label[nodes_idx_sets[best_fitted_set_idx]] = debug_label_idx + 1

                debug_label_idx += 2
                
                # Penalize that old model
                model_l.set_penalty(1)


            per_vertex_label = np.zeros(len(self.proxy.V))
            per_vertex_label[self.proxy.kept_vertices_idx] = new_per_node_label

            return per_vertex_label, new_models
        
        else:
            return np.zeros(self.proxy.nodes_count), []

    def is_converged(self, new_labels):
        if len(self.labels_progression) < 1:
            return False

        # Compare last 2 labellings: if no inlier change, the procedure converged
        difference = np.abs(self.labels_progression[-1] - new_labels)

        # Count number of changes
        nb_changes = len(np.flatnonzero(difference))

        print("Nb of vertices with a changed label = {}".format(nb_changes))

        return nb_changes == 0

    def compute_energy(self, unary_matrix, labels, edges_weight, label_costs=None):
        unary_energy = compute_unary_energy(labels, unary_matrix)

        smooth_energy = compute_smooth_energy(labels, self.proxy.edges, edges_weight)
        if label_costs is not None:
            label_energy = compute_label_energy(labels, label_costs)
        else:
            label_energy = 0
        print("Unary energy =", unary_energy)
        print("Smooth energy =", smooth_energy)
        print("Label energy =", label_energy)

        print("unweighted: unary = {}, smooth = {}, label = {}".format(unary_energy / self.params.w_fidelity, smooth_energy / self.params.w_smooth, label_energy / self.params.w_label))

        total = unary_energy + smooth_energy + label_energy

        print("Energy =", total)

        return total

    # def compute_energy_for_model(self, model, pts_subset, W=None):
    def compute_energy_for_model(self, model, subset):
        # if W is None:
        #     W = np.ones(len(pts_subset))
        # print(model.compute_residuals(pts_subset, approx=self.params.dist_approx).shape)
        # print(W.shape)
        # energy_due_to_model = self.params.w_fidelity * np.sum(np.clip(W * model.compute_residuals(pts_subset, approx=self.params.dist_approx)**2, 0, self.max_dist ** 2)) + self.params.w_label * model.cost
        energy_due_to_model = self.params.w_fidelity \
                                * np.sum(
                                    self.proxy.compute_fidelity_energy_by_node(
                                        model,
                                        dist_max=self.max_dist,
                                        dist_approx=self.params.dist_approx,
                                        w_proxy_vertices=self.params.w_proxy_vertices_fit)[subset]) \
                                + self.params.w_label * model.cost
        return energy_due_to_model

    def compute_worse_residual_for_model(self, model, pts_subset):
        return np.max(model.compute_residuals(pts_subset, approx=self.params.dist_approx))

    def compute_fidelity_energy_for_model(self, model):
        # return np.clip(self.proxy.compute_fidelity_energy_by_node(model, w_proxy_vertices=self.params.w_proxy_vertices_fit, dist_approx=self.params.dist_approx), 0, U_max)
        return self.proxy.compute_fidelity_energy_by_node(
                        model,
                        dist_max=self.max_dist,
                        w_proxy_vertices=self.params.w_proxy_vertices_fit,
                        dist_approx=self.params.dist_approx)

    # def update_fidelity_energy_matrix(self):
    #     models = self.new_models_proposal
    #     # if self.current_fidelity_energy_matrix is None:
    #     U = np.zeros((self.proxy.nodes_count, len(models)))

    #     for j in range(len(models)):
    #         U[ : , j] = self.compute_fidelity_energy_for_model(models[j])

    #     self.current_fidelity_energy_matrix = U + self.fidelity_energy_penalties

    #     return self.current_fidelity_energy_matrix


    def fidelity_energy_matrix(self):
        models = self.new_models_proposal

        U = np.zeros((self.proxy.nodes_count, len(models)))

        def _fill_column(energy_matrix, model, j):
            energy_matrix[:, j] = self.compute_fidelity_energy_for_model(model)

        futures = []
        for j in range(len(models)):
            args = (_fill_column,
                    U,
                    models[j],
                    j)
            futures.append(self.executor.submit(*args))
        concurrent.futures.wait(futures)

        return U + self.fidelity_energy_penalties

    # def update_fidelity_energy_matrix(self):
    #     models = self.new_models_proposal
    #     if self.current_fidelity_energy_matrix is None or len(self.models_progression) < 1:
    #         U = np.zeros((self.proxy.nodes_count, len(models)))

    #         for j in range(len(models)):
    #             U[ : , j] = self.compute_fidelity_energy_for_model(models[j])

    #         self.current_fidelity_energy_matrix = U + self.fidelity_energy_penalties

    #     else:
    #         print(self.current_fidelity_energy_matrix.shape)
    #         # Only update columns if necessary and add new ones
    #         old_models = self.models_progression[-1]
    #         for j in range(len(models)):
    #             if j < self.current_fidelity_energy_matrix.shape[1]:
    #                 # Check against hash to know if we should update the corresponding column
    #                 old_model = old_models[j]
    #                 if old_model != models[j]:
    #                     print("recompute for model {}".format(j))
    #                     self.current_fidelity_energy_matrix[ : , j] = self.compute_fidelity_energy_for_model(models[j])
    #                 else:
    #                     print("avoiding to update for model {}".format(j))
    #             else:
    #                 print("adding column for model {}".format(j))
    #                 new_column = self.compute_fidelity_energy_for_model(models[j])
    #                 self.current_fidelity_energy_matrix = np.column_stack([self.current_fidelity_energy_matrix, new_column])
    #         print(self.current_fidelity_energy_matrix.shape)

    #     return self.current_fidelity_energy_matrix

    # def update_penalties_matrix(self):
    #     models = self.new_models_proposal
    #     if self.fidelity_energy_penalties is None:
    #         self.fidelity_energy_penalties = np.zeros((self.proxy.nodes_count, len(models)))
    #     # Otherwise just add enough zero columns

    def get_models_mesh(self, iteration, only_assigned_models=True):
        if np.abs(iteration) > len(self.models_progression):
            raise IndexOutOfBoundError('Index {} is out of bounds ({} iterations available)'.format(iteration, len(self.models_progression)))
        all_models = self.models_progression[iteration]
        labels = self.labels_progression[iteration]

        subsets, assigned_labels = get_subsets_from_labels(labels)

        models = []
        # Compute bounding boxes for each model
        centers = []
        spans = []
        for l, m in enumerate(all_models):
            subset_idx_l = get_subset_id_from_label(assigned_labels, l)
            if subset_idx_l != -1:
                points_l, _ = self.proxy.get_data_for_nodes(subsets[subset_idx_l])
                center_l, span_l = get_bounds(points_l, padding=0.1)
                models.append(m)
                centers.append(center_l)
                spans.append(span_l)
            else:
                if not only_assigned_models:
                    models.append(m)
                    centers.append(self.proxy.center)
                    spans.append(self.proxy.span)

        return polygonize_models(models, centers, spans)


    def get_proposed_models_mesh(self, center, span):
        if self.new_models_proposal is None:
            raise NotInitialized('No model proposal was made yet, the SegmentFit method must be initialized first.')

        return polygonize_models(self.new_models_proposal, [center]*len(self.new_models_proposal), [span]*len(self.new_models_proposal))
        

    def get_labelling(self, iteration):
        if np.abs(iteration) > len(self.labels_progression):
            raise IndexOutOfBoundError('Index {} is out of bounds ({} iterations available)'.format(iteration, len(self.labels_progression)))

        return self.labels_progression[iteration]

    def visualize_step(self, iteration=-1, translation=np.array([0,0,0]), only_assigned_models=True):
        labels = self.get_labelling(iteration)
        Vs, Fs = self.get_models_mesh(iteration, only_assigned_models=only_assigned_models)
        fidelity_energy_by_pt = self.get_fidelity_energy_by_node(iteration)

        # labels_proxy = self.propagate_labels_to_all_proxy(iteration)

        # return visualize_labelling(self.proxy.V, labels_proxy)
        # structs = visualize_nodes(self.proxy, labels, fidelity_energy_by_pt)

        # return structs

        return visualize(self.proxy, labels, Vs, Fs, fidelity_energy_by_pt, translation, only_assigned_models)
        
    def visualize_step_by_projection(self, iteration=-1, translation=np.array([0,0,0]), mode='ps'):
        # labels = self.get_labelling(iteration)

        all_labels = self.propagate_labels_to_all_proxy(iteration)

        if mode == 'ps':
            # return visualize_models_by_projection(self.proxy.mesh, self.proxy.V, self.proxy.F, self.models_progression[iteration], all_labels, is_on_mirror=self.proxy.vertex_is_on_mirror)
            return visualize_models_by_projection(self.proxy.mesh, self.proxy.V, self.proxy.F, self.models_progression[iteration], all_labels, is_on_mirror=self.proxy.vertex_is_on_mirror, in_out_labels=self.proxy.in_out_labelling)
        else:
            V_opt, vertex_labels = mesh_by_projection(self.proxy.mesh, all_labels, self.models_progression[iteration], self.proxy.vertex_is_on_mirror, self.proxy.in_out_labelling)
            return V_opt, self.proxy.F, vertex_labels


    def get_fidelity_energy_by_node(self, iteration):
        if np.abs(iteration) > len(self.labels_progression):
            raise IndexOutOfBoundError('Index {} is out of bounds ({} iterations available)'.format(iteration, len(self.labels_progression)))

        # Compute fidelity energy for each node, for the labelling of iteration
        labels = self.labels_progression[iteration]
        models = self.models_progression[iteration]

        # energy_matrix = compute_unary(self.data_points, self.data_points_indices_by_node, models, self.max_dist**2)
        # energy_matrix = self.compute_fidelity_energy_matrix(models, self.max_dist ** 2, dist_approx=self.params.dist_approx, w_proxy_vertices=self.params.w_proxy_vertices_fit)
        energy_matrix = self.fidelity_energy_matrix() # COSTLY!

        energy_by_node = energy_matrix[range(len(labels)), labels]

        return energy_by_node

    def plot_energy_progression(self):
        plot_energy(self.energy_progression)

    def propagate_labels_to_all_proxy(self, iteration=-1):
        if np.abs(iteration) > len(self.labels_progression):
            raise IndexOutOfBoundError('Index {} is out of bounds ({} iterations available)'.format(iteration, len(self.labels_progression)))

        labels = self.labels_progression[iteration]

        # return self.proxy.propagate_labels_to_all_proxy(labels)
        return self.proxy.property_by_vertex(labels)


    def export_log(self, filepath, additional_properties={}):
        sf_dict = {}

        # Main info
        sf_dict['converged'] = self.converged
        sf_dict['runtime'] = self.logs.get_runtime()
        sf_dict['initialization_time'] = self.logs.get_initialization_time()
        sf_dict['iterations_time'] = self.logs.get_iterations_time()
        sf_dict['node_count'] = self.proxy.nodes_count
        # sf_dict['proxy_points'] = self.proxy.V.tolist()
        # sf_dict['in_out_labelling'] = self.proxy.in_out_labelling.tolist()
        sf_dict['vertices_outside'] = np.flatnonzero(self.proxy.in_out_labelling).tolist()
        # sf_dict['edges'] = self.proxy.edges.tolist()
        # sf_dict['edges_weight'] = self.proxy.edges_weight.tolist()
        # sf_dict['seed'] = random_seed

        sf_dict['stroke_position_by_edge'] = self.proxy.get_stroke_point_by_edge()
        # sf_dict['stroke_index_by_edge'] = self.proxy.get_stroke_index_by_edge()

        sf_dict.update(additional_properties)

        # Parameters
        # sf_dict['parameters'] = self.params.to_dict()

        # States
        sf_dict_states = []

        for models, labels, energy in zip(self.models_progression, self.labels_progression, self.energy_progression):
            propagated_labels = self.proxy.property_by_vertex(labels)
            # propagated_labels = self.proxy.propagate_labels_to_all_proxy(labels)
            state = {
                'models': [m.to_dict() for m in models],
                'labels': propagated_labels.tolist(),
                'energy': energy
            }

            sf_dict_states.append(state)

        sf_dict['states'] = sf_dict_states

        with open(filepath, "w") as f:
            json.dump(sf_dict, f, indent=2)


    def print(self):
        print('─' * 10)
        print("Segment and fit report:")

        print("Nb of iterations = {}".format(len(self.energy_progression)))

        for it, (models, labels, energy) in enumerate(zip(self.models_progression, self.labels_progression, self.energy_progression)):
            print("Iteration {}:".format(it))
            print("\tEnergy = {}".format(energy))
            print("\tAvailable model count = {}".format(len(models)))
            print("\tAssigned model count = {}".format(len(np.unique(labels))))

            # print("\tModels degrees = {}".format([m.degree for m in models]))

        print("Energy progression = {}".format(self.energy_progression))

        print("Runtimes")

        for it, (t_label, t_fit) in enumerate(zip(self.logs.labelling_times,self.logs.fitting_times)):
            print("Iteration {} : labelling = {} s, fitting = {} s".format(it, t_label, t_fit))

        print("Ratio of refitted models")
        for it, r in enumerate(self.logs.models_refitted_ratios):
            print("Iteration {} : {}".format(it, r))

        print('─' * 10)