import potpourri3d as pp3d
import numpy as np
import math
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components
import networkx as nx

from utils.pygel import get_faces, get_normals
from utils.visualize import visualize_graph
from utils.math import get_bounds, remove_mesh_vertices
from utils.mirroring import is_on_mirror
from utils.graph import get_edge_from_key, get_edge_key, incidence_matrix, affinity_matrix, affinity_matrix_sparse

from main.mesh_optimization import optimize_mesh, smooth_proxy_trimmed_boundary

class Proxy:

    def __init__(
        self,
        proxy_mesh, mirror, index_map,
        proxy_graph_mesh,
        vertex_cells,
        stroke_data_points,
        proxy_normals,
        crossing_segments_id,
        edges,
        per_edge_cut_costs,
        edge_crossings_by_edge,
        edge_crossings_by_mesh_edge,
        segment_sharpness,
        w0, ws, edge_length_pow, in_out_labelling=None):

        # self._proxy = proxy_mesh
        self.V = proxy_mesh.positions()
        self.F = get_faces(proxy_mesh)
        self.mesh = proxy_mesh
        self.graph_mesh = proxy_graph_mesh

        self.mirror = mirror

        self.mesh_normals = get_normals(proxy_mesh)

        self.vertex_idx_to_node_idx_map = index_map
        self.proxy_vertex_is_node = index_map > -1
        self.kept_vertices_idx = np.flatnonzero(self.proxy_vertex_is_node)

        self.data_points = stroke_data_points
        self.normals = proxy_normals
        self.set_indices_by_node(vertex_cells)
        # self.indices_by_node = vertex_cells

        if mirror:
            self.vertex_is_on_mirror = is_on_mirror(self.V)
            self.on_mirror_nodes = np.flatnonzero(self.vertex_is_on_mirror[self.kept_vertices_idx])
        else:
            self.vertex_is_on_mirror = None
            self.on_mirror_nodes = []

        # self.nodes = vertex_cells
        self.edges = edges
        self.per_edge_cut_costs = per_edge_cut_costs
        self.segment_sharpness = segment_sharpness

        # self.edge_crossings_by_edge is a dictionary:
        # graph_edge_key -->
        #        [
        #           index of corresponding segment in self.crossing_segments
        #        ])
        self.edge_crossings_by_edge = edge_crossings_by_edge
        self.edge_crossings_by_mesh_edge = edge_crossings_by_mesh_edge
        self.user_defined_weights = np.zeros(len(self.edges))

        self.w0 = w0
        self.ws = ws
        self.edge_length_pow = edge_length_pow
        self.edges_weight = None
        self.crossing_segments = crossing_segments_id
        self.update_edge_weights() # Compute edge weights for the first time

        # Compute the mapping: vertex --> closest node
        self.closest_node_by_vertex = self.propagate_labels_to_all_proxy(np.arange(len(self.nodes_positions)))

        # self.nodes_pt_assignment = np.zeros((len(vertex_cells), len(self.data_points)))
        # self.node_has_data = np.zeros(len(self.indices_by_node), dtype=bool)
        # row_indices = np.empty(0, dtype=int)
        # col_indices = np.empty(0, dtype=int)
        # for i, node_indices in enumerate(self.indices_by_node):
        #     row_indices = np.append(row_indices, np.repeat(i, len(node_indices)))
        #     col_indices = np.append(col_indices, node_indices)
        #     if len(node_indices) > 0:
        #         self.node_has_data[i] = True

        # self.nodes_pt_assignment = csr_matrix((np.ones(len(row_indices)), (np.array(row_indices), np.array(col_indices))), shape=(len(vertex_cells), len(self.data_points)))

        self.distance_solver = pp3d.MeshHeatMethodDistanceSolver(self.V, self.F)

        self.center, self.span = get_bounds(self.V)

        self.in_out_labelling = in_out_labelling

        self.node_in_out_labelling = in_out_labelling[self.kept_vertices_idx]

    # TODO: put this as properties defined at initialisation? They don't change afterwards anyway
    
    @property
    def nodes_count(self):
        return len(self.indices_by_node)
    
    @property
    def nodes_positions(self):
        return self.V[self.kept_vertices_idx]

    @property
    def nodes_normals(self):
        return self.mesh_normals[self.kept_vertices_idx]

    @property
    def node_is_on_mirror(self):
        if self.vertex_is_on_mirror is None:
            return None
        else:
            return self.vertex_is_on_mirror[self.kept_vertices_idx]

    def set_indices_by_node(self, indices_by_node):
        self.indices_by_node = indices_by_node
        self.node_has_data = np.zeros(len(indices_by_node), dtype=bool)
        row_indices = np.empty(0, dtype=int)
        col_indices = np.empty(0, dtype=int)
        for i, node_indices in enumerate(indices_by_node):
            row_indices = np.append(row_indices, np.repeat(i, len(node_indices)))
            col_indices = np.append(col_indices, node_indices)
            if len(node_indices) > 0:
                self.node_has_data[i] = True

        self.nodes_pt_assignment = csr_matrix((np.ones(len(row_indices)), (np.array(row_indices), np.array(col_indices))), shape=(len(indices_by_node), len(self.data_points)))


    def get_proxy_data_for_nodes(self, node_indices, ignore_near_strokes=True):
        node_indices = np.array(node_indices, dtype=int)
        if ignore_near_strokes:
            normal_data_subset = node_indices[np.logical_not(self.node_has_data[node_indices])]
            if len(normal_data_subset) > 0:
                # In case there is no stroke-less node, return data for all nodes
                node_indices = normal_data_subset
        return self.nodes_positions[node_indices], self.nodes_normals[node_indices]


    def edge_length(self, e_idx):
        e = self.edges[e_idx]
        nodes_pos = self.nodes_positions[e]
        return np.linalg.norm(nodes_pos[0] - nodes_pos[1])

    def get_total_points_count(self):
        return len(self.data_points)

    def get_indices_for_node(self, node_idx):
        return self.indices_by_node[node_idx]

    def get_indices_for_vertex(self, vertex_idx):
        node_idx = self.vertex_idx_to_node_idx_map[vertex_idx]
        if node_idx == -1:
            return []
        else:
            return self.get_indices_for_node(node_idx)

    def get_indices_for_nodes(self, nodes_idx):
        indices = np.hstack(self.indices_by_node[nodes_idx]).astype(int).flatten()
        return indices
        # print(len(indices))
        # unique_indices = np.unique(indices)
        # print("unique points = {}".format(len(unique_indices)))
        # return unique_indices

    def get_data_for_nodes(self, nodes_idx):
        data_indices = self.get_indices_for_nodes(nodes_idx)
        # points = self.data_points[data_indices]
        # normal = least_squares_fit_plane(points).normal()
        # normals = np.tile(normal, len(points)).reshape((len(points), 3))
        # return points, normals
        return self.data_points[data_indices], self.normals[data_indices]

    def compute_geodesic_distances(self, source_vertex_id):
        return self.distance_solver.compute_distance(source_vertex_id)

    def compute_geodesic_distances_multi(self, source_vertices_id):
        return self.distance_solver.compute_distance_multisource(list(source_vertices_id))

    def node_to_vertex_idx(self, node_idx):
        vertex_idx = np.flatnonzero(self.vertex_idx_to_node_idx_map == node_idx)[0]
        return vertex_idx

    def property_by_vertex(self, property_by_node):
        return property_by_node[self.closest_node_by_vertex]


    def compute_fidelity_energy_by_node(self, model, dist_max=None, w_proxy_vertices=0, dist_approx='first_order'):

        # Compute residual for all data points at once, because it's faster
        all_residuals = model.compute_residuals(self.data_points, approx=dist_approx) ** 2
        # Assign sum for each cell
        E = self.nodes_pt_assignment.dot(all_residuals)

        if dist_max is not None:
            # Limit values by clamping
            E = np.clip(E, 0, dist_max ** 2)

        # object_size = np.mean(np.max(self.data_points, axis = 0)- np.min(self.data_points, axis = 0))
        # c_p = 0.05
        # c = c_p * object_size

        # all_residuals_normals = (c - model.compute_residuals(self.data_points + c * self.normals, approx=dist_approx))**2

        # E += 0.5 * self.nodes_pt_assignment @ all_residuals_normals

        # Add cost for proxy vertices residuals
        if w_proxy_vertices > 0:
            E += w_proxy_vertices * (model.compute_residuals(self.nodes_positions, approx=dist_approx) ** 2)

        # w_normal = 0.1 / len(self.nodes_positions)
        # E_normal = w_normal * (np.ones(len(self.nodes_positions)) - np.einsum('ij,ij->i', model.normal_at(self.nodes_positions), self.mesh_normals[self.kept_vertices_idx])) ** 2
        # E_normal[self.node_has_data] = 0
        # E += E_normal

        # If symmetric: add cost for surface normal alignment to mirror plane at on-mirror nodes
        # if self.mirror:
        #     w_smooth_mirror = (dist_max) ** 2
        #     # on_mirror_nodes = np.flatnonzero(self.node_is_on_mirror)
        #     on_mirror_nodes_normals = model.normal_at(self.nodes_positions[self.on_mirror_nodes])
        #     on_mirror_nodes_cost = (on_mirror_nodes_normals @ np.array([1, 0, 0]))**2

        #     E[self.on_mirror_nodes] += w_smooth_mirror * on_mirror_nodes_cost

        # Zero out for nodes that are outside
        E[self.node_in_out_labelling] = 0

        return E

    def get_closest_node(self, vertex_id):
        dists = self.compute_geodesic_distances(vertex_id)
        dists_nodes = dists[self.kept_vertices_idx]

        closest_id = np.argmin(dists_nodes)
        # node_closest_id = self.kept_vertices_idx[closest_id]

        return closest_id


    def update_stroke_sharpness(self, new_segment_sharpness):
        # Update self.edges_weight according to new values

        self.segment_sharpness = new_segment_sharpness

        return self.update_edge_weights()

    def update_edge_weight_boost(self, vertex_group_indices, smoothness_value):
        # Update self.user_defined_weights
        # - First find the nodes corresponding to the vertices from the group
        node_group_indices = np.unique(self.closest_node_by_vertex[vertex_group_indices])

        # - Find edges that connect those nodes
        group_edges = []

        for e_idx, e in enumerate(self.edges):
            if np.any(node_group_indices == e[0]) and np.any(node_group_indices == e[1]):
                group_edges.append(e_idx)

        # print(group_edges)

        # - Update user_defined_weights
        self.user_defined_weights[group_edges] = smoothness_value

        self.update_edge_weights()
        return

    def add_stroke(self, stroke_points):
        # Project the stroke onto the proxy mesh
        nodes_covered = project_stroke(self, stroke_points)

        self.update_edge_weights()

        return nodes_covered

    def update_edge_weights(self):
        print("w_0 = {}, w_s = {}, pow = {}".format(self.w0, self.ws, self.edge_length_pow))
        old_weights = self.edges_weight

        self.edges_weight = compute_edge_weights(
            # self.graph_mesh,
            # self.vertex_idx_to_node_idx_map,
            self.nodes_positions,
            self.edges,
            self.per_edge_cut_costs,
            self.edge_crossings_by_edge,
            self.crossing_segments,
            self.segment_sharpness,
            self.user_defined_weights,
            self.w0, self.ws, self.edge_length_pow)

        self.affinity_matrix = affinity_matrix(self.nodes_count, self.edges, self.edges_weight)
        self.affinity_matrix_sparse = affinity_matrix_sparse(len(self.nodes_positions), self.edges, self.edges_weight)

        # Return the difference between old and new weights
        if old_weights is not None:
            # changed_edges = np.flatnonzero(old_weights - self.edges_weight)
            # import polyscope as ps
            # ps.register_point_cloud("changed edges", self.nodes_positions[(self.edges[changed_edges]).flatten()])
            return old_weights - self.edges_weight


    def segment_by_min_cut(self, s, t, indices=None):
        # s and t are indexed in indices, and not in the whole list of nodes
        if indices is None:
            indices = np.arange(self.nodes_count) # Take all nodes
        # Build networkx graph
        G = nx.Graph()
        sub_affinity_matrix = self.affinity_matrix[np.array(indices)[:, None], np.array(indices)]

        for j in range(len(indices)):
            for i in range(j):
                if sub_affinity_matrix[i,j] > 0:
                    G.add_edge(i,j,capacity=sub_affinity_matrix[i,j])

        cut_cost, subsets = nx.minimum_cut(G, s, t)

        # print(cut_cost)
        # print(subsets)

        return list(subsets[0]), list(subsets[1])

    def propagate_labels_to_all_proxy(self, labels_for_nodes):

        labels_proxy = np.ones(len(self.V)) * -1
        distances_to_source = np.zeros(len(self.V))

        # Set labels to nodes
        vertices_with_label = []
        for nidx, label in enumerate(labels_for_nodes):
            vidx = self.node_to_vertex_idx(nidx)
            labels_proxy[vidx] = label
            vertices_with_label.append(vidx)

        # Propagate labels to all vertices
        vertices_to_check_queue = vertices_with_label

        while len(vertices_to_check_queue) > 0:
            vidx = vertices_to_check_queue.pop(0)
            # distance = distance_to_source.pop()
            # print("checking vertex {}".format(vidx))
            label = labels_proxy[vidx]
            distance_v = distances_to_source[vidx]

            # Do all the neighbors have a label?
            for nidx in self.mesh.circulate_vertex(vidx, mode='v'):
                vn_dist = np.linalg.norm(self.V[vidx] - self.V[nidx])
                if labels_proxy[nidx] == -1:
                    labels_proxy[nidx] = label
                    distances_to_source[nidx] = distance_v + vn_dist
                    vertices_to_check_queue.append(nidx)
                else:
                    # If the distance is better than previous distance, replace label
                    if (distance_v + vn_dist < distances_to_source[nidx]):
                        labels_proxy[nidx] = label
                        distances_to_source[nidx] = distance_v + vn_dist
                        vertices_to_check_queue.append(nidx)

        
        print("Vertices without labels: {}".format(len(np.flatnonzero(labels_proxy < 0))))

        # Return an array of integer where each element corresponds to the label of the proxy vertex of same index
        return labels_proxy.astype(int)

    def connected_components(self, node_subset):
        sub_affinity_matrix = self.affinity_matrix[np.array(node_subset)[:, None], np.array(node_subset)]
        n_components, labels = connected_components(sub_affinity_matrix, directed=False)
        return n_components, labels

    def get_stroke_point_by_edge(self):
        stroke_position_by_edge = {}
        for e_key in self.edge_crossings_by_mesh_edge:
            stroke_position_by_edge[e_key] = self.data_points[self.edge_crossings_by_mesh_edge[e_key]].tolist()

        return stroke_position_by_edge

    def get_boundary_cropped_proxy(self):
        all_V = self.V
        all_F = self.F

        if np.any(self.in_out_labelling):

            trimmed_V, trimmed_F = smooth_proxy_trimmed_boundary(self.mesh, self.in_out_labelling, self.get_stroke_point_by_edge())

            return trimmed_V, trimmed_F

        return all_V, all_F


    def visualize(self):
        return visualize_graph(self.nodes_positions, self.edges, self.edges_weight, self.indices_by_node, self.data_points, self.mirror)






def compute_edge_weights(
        # proxy_graph_mesh,
        # vertex_idx_to_node_idx_map,
        node_positions, edges,
        per_edge_cut_costs,
        edge_crossings_by_edge, crossing_segments, segments_sharpness,
        user_defined_weights,
        w0, ws, edge_length_pow):
    
    edge_weights = []
    edge_lengths = []

    _, span = get_bounds(node_positions, padding=0)
    bounding_box_dim = np.max(span)

    for e in edges:
        e_positions = node_positions[e]
        e_length = np.linalg.norm(e_positions[0] - e_positions[1])
        e_key = get_edge_key(e)
        weight = w0
        if e_key in edge_crossings_by_edge and len(edge_crossings_by_edge[e_key]) > 0:
            crossed_segments = [crossing_segments[ec] for ec in edge_crossings_by_edge[e_key]]
            # print(crossed_segments)
            max_sharpness = np.max(segments_sharpness[crossed_segments])
            weight = ws * (1 - max_sharpness)

            # min_weight = np.min(per_segment_weight[crossed_segments])
            # weight = ws * min_weight

        # edge_lengths.append(e_length)
        
        # Compute distance between corners of the adjacent faces
        # v0 = np.flatnonzero(vertex_idx_to_node_idx_map == e[0])[0]
        # v1 = np.flatnonzero(vertex_idx_to_node_idx_map == e[1])[0]
        # he = get_halfedge(proxy_graph_mesh, v0, v1)
        # cA = proxy_graph_mesh.incident_vertex(proxy_graph_mesh.next_halfedge(he))
        # cB = proxy_graph_mesh.incident_vertex(proxy_graph_mesh.next_halfedge(proxy_graph_mesh.opposite_halfedge(he)))
        # edge_lengths.append(np.linalg.norm(proxy_graph_mesh.positions()[cA] - proxy_graph_mesh.positions()[cB]))

        edge_weights.append(weight)

    max_edge_cost = np.max(per_edge_cut_costs)

    edge_weights = np.max(np.array([edge_weights, user_defined_weights]), axis = 0)

    edge_weights = np.multiply(
        edge_weights,
        np.power(np.array(per_edge_cut_costs) / max_edge_cost, edge_length_pow)
    )


    return edge_weights




def normalized_min_cut(graph):
    """Clusters graph nodes according to normalized minimum cut algorithm.
    All nodes must have at least 1 edge. Uses zero as decision boundary. 
    
    Parameters
    -----------
        graph: a networkx graph to cluster
        
    Returns
    -----------
        vector containing -1 or 1 for every node
    References
    ----------
        J. Shi and J. Malik, *Normalized Cuts and Image Segmentation*, 
        IEEE Transactions on Pattern Analysis and Machine Learning, vol. 22, pp. 888-905
    """
    m_adjacency = np.array(nx.to_numpy_matrix(graph))

    D = np.diag(np.sum(m_adjacency, 0))
    D_half_inv = np.diag(1.0 / np.sqrt(np.sum(m_adjacency, 0)))
    M = np.dot(D_half_inv, np.dot((D - m_adjacency), D_half_inv))

    (w, v) = np.linalg.eig(M)
    #find index of second smallest eigenvalue
    index = np.argsort(w)[1]

    v_partition = v[:, index]
    v_partition = np.sign(v_partition)
    
    # Subsets of nodes
    set_A_indices = np.flatnonzero(v_partition < 0)
    set_B_indices = np.flatnonzero(v_partition >= 0)
    
    nodes_A = np.array(graph.nodes())[set_A_indices]
    nodes_B = np.array(graph.nodes())[set_B_indices]
    
    return nodes_A, nodes_B

from utils.projection import *


def project_stroke(proxy, stroke_points):
    # Update proxy.data_points
    # Update proxy.indices_by_node
    # Update proxy.edge_crossings_by_edge
    # Update proxy.crossing_segments
    # Update proxy.segment_sharpness (set to default = 0 for new segments)

    # Return which nodes are covered by the stroke

    mesh = proxy.graph_mesh
    V = mesh.positions()
    F = get_faces(mesh)

    G = mesh_dual_graph(mesh)

    projections, triangle_ids, barycentric_coords = closest_pts(V, F, stroke_points)

    new_stroke_data_points = np.empty((0,3))
    new_proxy_normals = np.empty((0,3))
    new_edge_crossings_by_edge = {}
    new_indices_by_node = {}

    new_crossing_segments = []

    nodes_covered = []

    for i in range(len(stroke_points) - 1):
        s_idx = i
        vA = i
        vB = i + 1
        pA = stroke_points[vA]
        pB = stroke_points[vB]
        endpoint_faces = triangle_ids[[vA, vB]]

        path = nx.shortest_path(G, source=endpoint_faces[0], target=endpoint_faces[1], weight="weight")
        he_path = halfedge_path_from_face_path(mesh, path)

        if len(he_path) == 0:
                continue

        du = 1.0 / len(he_path)
        for i_he, he in enumerate(he_path):
            edge = np.sort(np.array([mesh.incident_vertex(he), mesh.incident_vertex(mesh.opposite_halfedge(he))]))
            u = du * i_he
            pos_3d = pA * (1 - u) + pB * u # uniformly interpolate the input strokes
            pos_proxy = np.mean(mesh.positions()[edge], axis = 0)
            stroke_weight = 1 # default weight

            ec_id = len(new_stroke_data_points)

            new_stroke_data_points = np.row_stack([new_stroke_data_points, pos_3d])
            ec_normal = np.mean(proxy.mesh_normals[edge], axis = 0)
            ec_normal /= np.linalg.norm(ec_normal)
            new_proxy_normals = np.row_stack([new_proxy_normals, ec_normal])

            # crossing_segments_id = np.append(crossing_segments_id, s_idx)
            new_crossing_segments.append(s_idx)

            # graph_edge = proxy.vertex_idx_to_node_idx_map[edge]
            graph_edge = edge
            edge_key = get_edge_key(graph_edge)
            if edge_key not in new_edge_crossings_by_edge:
                new_edge_crossings_by_edge[edge_key] = []
            new_edge_crossings_by_edge[edge_key].append(ec_id)


    for vi in mesh.vertices():
        vertex_crossings = []
        if proxy.in_out_labelling[vi]:
            # Do not associate sketch data to nodes that are outside the sketch
            continue
        for vn in mesh.circulate_vertex(vi, mode='v'):
            edge = [vi, vn]
            e_key = get_edge_key(edge)
            if e_key in new_edge_crossings_by_edge:
                edge_crossings = new_edge_crossings_by_edge[e_key]
                # Find closest crossing, if any
                # if len(edge_crossings) > 0:
                #     pos = mesh.positions()[vi]
                #     c = edge_crossings[0]
                #     for i in range(1, len(edge_crossings)):
                #         if np.linalg.norm(new_stroke_data_points[edge_crossings[i]] - pos) < np.linalg.norm(new_stroke_data_points[c] - pos):
                #             c = edge_crossings[i]
                vertex_crossings.extend(edge_crossings)
        if len(vertex_crossings) > 0:
            # node_idx = proxy.vertex_idx_to_node_idx_map[vi]
            node_idx = vi
            new_indices_by_node[node_idx] = vertex_crossings

    # Update proxy.data_points
    starting_data_points_idx = len(proxy.data_points)
    proxy.data_points = np.row_stack([proxy.data_points, new_stroke_data_points])
    proxy.normals = np.row_stack([proxy.normals, new_proxy_normals])

    # Update proxy.indices_by_node
    indices_by_node = proxy.indices_by_node.copy()
    for node_idx, vertex_crossings in new_indices_by_node.items():
        vertex_crossings = (np.array(vertex_crossings, dtype=int) + starting_data_points_idx).tolist()
        indices_by_node[node_idx].extend(vertex_crossings)
        nodes_covered.append(node_idx)
    proxy.set_indices_by_node(indices_by_node)

    # Update proxy.edge_crossings_by_edge
    starting_segment_idx = len(proxy.crossing_segments)
    for e_key, crossings in new_edge_crossings_by_edge.items():
        if e_key not in proxy.edge_crossings_by_edge:
            proxy.edge_crossings_by_edge[e_key] = []
        crossings_reindexed = (np.array(crossings) + starting_segment_idx).tolist()
        proxy.edge_crossings_by_edge[e_key].extend(crossings_reindexed)

    # Update proxy.crossing_segments
    proxy.crossing_segments = np.append(proxy.crossing_segments, np.array(new_crossing_segments, dtype=int) + starting_segment_idx)
    # Update proxy.segment_sharpness (set to default = 0 for new segments)
    n_new_segments = len(stroke_points) - 1
    proxy.segment_sharpness = np.append(proxy.segment_sharpness, np.zeros(n_new_segments))

    return nodes_covered