from numpy.core.numeric import cross
from pygel3d import hmesh
import polyscope as ps
import numpy as np
import os
import networkx as nx
from scipy.spatial import KDTree
import potpourri3d as pp3d

from utils.pygel import get_faces, get_edges, get_normals, get_halfedge
from main.proxy_graph import Proxy
from utils.graph import get_edge_key
from main.adaptive_proxy_mesh import simplify_graph
from utils.mirroring import is_on_mirror, is_in_mirror_plane, should_remove
from utils.math import bounding_box_diagonal, get_bounds
from utils.projection import closest_pts, mesh_dual_graph, path_length, halfedge_path_from_face_path, mesh_primal_graph


def build_proxy(
    mesh, 
    mirror_symmetry, 
    data_points, 
    segments, 
    segment_weights, 
    w0, 
    ws, 
    edge_length_pow=0.5, 
    adaptive_proxy_mesh=False, 
    exclude_path_ratio=2, 
    point_is_on_border=None):

    G = mesh_dual_graph(mesh)

    V = mesh.positions()
    F = get_faces(mesh)

    projections, triangle_ids, barycentric_coords = closest_pts(V, F, data_points)
    edge_crossing_positions = np.empty((0,3))
    # path_length_by_ec = np.empty((0,1))
    edge_crossings_by_edge = {}
    edge_crossings_by_vertex = {}

    # edge_crossing_edges = np.empty((0,2))

    stroke_data_points = np.empty((0,3))
    proxy_normals = np.empty((0, 3))
    crossing_segments_id = np.empty((0, 1), dtype=int)

    all_proxy_mesh_normals = get_normals(mesh)

    # crossing_segments_by_edge = {}

    default_segment_sharpness = np.zeros(len(segments))


    if mirror_symmetry:
        data_point_is_on_mirror = np.logical_or(
            is_on_mirror(data_points, threshold=bounding_box_diagonal(data_points) * 0.005),
            should_remove(data_points, threshold=0)
        )

        # Restrict to points of valence 1 (stroke endpoint)
        valence_by_data_point = np.zeros(len(data_points))
        for s in segments:
            valence_by_data_point[s[0]] += 1
            valence_by_data_point[s[1]] += 1

        stroke_endpoint_is_on_mirror = np.logical_and(data_point_is_on_mirror, valence_by_data_point < 2)

        # Build a kd-tree with points from on-mirror faces barycenter
        mesh_vertex_is_on_mirror = is_on_mirror(V)

        face_is_mirror_adjacent = np.any(mesh_vertex_is_on_mirror[F].astype(int), axis = 1)
        faces_on_mirror = np.flatnonzero(np.sum(mesh_vertex_is_on_mirror[F].astype(int), axis = 1) == 2)

        faces_barycenter = [mesh.centre(fid) for fid in faces_on_mirror]

        on_mirror_faces_kd_tree = KDTree(faces_barycenter)

        # import polyscope as ps
        # ps_mesh = ps.register_surface_mesh("proxy mesh debug", V, F)
        # ps_mesh.add_scalar_quantity("on mirror", mesh_vertex_is_on_mirror, defined_on='vertices')
        # ps_sketch = ps.register_point_cloud("sketch debug", data_points)
        # ps_sketch.add_scalar_quantity("is on mirror", data_point_is_on_mirror)
        # ps_sketch.add_scalar_quantity("endpoint is on mirror", stroke_endpoint_is_on_mirror)

        # ps.show()
    else:
        mesh_vertex_is_on_mirror = None

    print("[BUILDING PROXY] tracing all stroke segments")

    on_mirror_stroke_segments = []

    for s_idx, (segment, weight) in enumerate(zip(segments, segment_weights)):
        # Which faces do they fall on?
        endpoint_faces = triangle_ids[segment]

        pA, pB = data_points[segment]

        # Is any of the end points on the mirror?
        if mirror_symmetry:
            # if both points project to faces adjacent to the mirror
            # do not trace (we will associate data points to the vertices later)
            if np.all(face_is_mirror_adjacent[endpoint_faces]) and is_in_mirror_plane(pB - pA):
                on_mirror_stroke_segments.append(s_idx)
                continue

            additional_on_mirror_edge = None
            for k, pid in enumerate(segment):
                if stroke_endpoint_is_on_mirror[pid]:
                    # Find the nearest on-mirror proxy mesh face
                    _, nearest_on_mirror_face = on_mirror_faces_kd_tree.query(data_points[pid])
                    endpoint_faces[k] = faces_on_mirror[nearest_on_mirror_face]
                    on_mirror_face_indices = F[faces_on_mirror[nearest_on_mirror_face]]
                    on_mirror_vertex_indices = on_mirror_face_indices[mesh_vertex_is_on_mirror[on_mirror_face_indices]]
                    additional_on_mirror_edge = get_halfedge(mesh, on_mirror_vertex_indices[0], on_mirror_vertex_indices[1])

        # print(endpoint_faces[0], endpoint_faces[1])
        path = nx.shortest_path(G, source=endpoint_faces[0], target=endpoint_faces[1], weight="weight")

        length_path_e = path_length(mesh, path)

        # print(length_path_e)

        # Accept or refuse the edge path depending on whether it has a reasonable length
        segment_length = np.linalg.norm(pA - pB)
        if length_path_e > exclude_path_ratio * segment_length:
            print("[BUILDING PROXY] a projected segment is too long, it is ignored")
        else:
            # Store edge crossings
            he_path = halfedge_path_from_face_path(mesh, path)
            if mirror_symmetry and additional_on_mirror_edge is not None:
                # print("additional on mirror edge crossed")
                # v_he_a = mesh.incident_vertex(additional_on_mirror_edge)
                # v_he_b = mesh.incident_vertex(mesh.opposite_halfedge(additional_on_mirror_edge))
                # ps.register_point_cloud("crossed mirror edge", V[[v_he_a, v_he_b]])
                # ps.show()
                he_path = np.append(he_path, additional_on_mirror_edge)

            if len(he_path) == 0:
                continue

            du = 1.0 / len(he_path)
            for i_he, he in enumerate(he_path):
                edge = np.sort(np.array([mesh.incident_vertex(he), mesh.incident_vertex(mesh.opposite_halfedge(he))]))
                u = du * i_he
                pos_3d = pA * (1 - u) + pB * u # uniformly interpolate the input strokes
                pos_proxy = np.mean(mesh.positions()[edge], axis = 0)
                stroke_weight = weight

                # ec = EdgeCrossing(edge, s_idx, t, len(stroke_data_points), pos_proxy, stroke_weight)

                ec_id = len(stroke_data_points)

                stroke_data_points = np.row_stack([stroke_data_points, pos_3d])
                ec_normal = np.mean(all_proxy_mesh_normals[edge], axis = 0)
                ec_normal /= np.linalg.norm(ec_normal)
                proxy_normals = np.row_stack([proxy_normals, ec_normal])

                crossing_segments_id = np.append(crossing_segments_id, s_idx)

                # Store crossing
                for vi in edge:
                    # vertices_with_data.add(vi)
                    if vi not in edge_crossings_by_vertex:
                        edge_crossings_by_vertex[vi] = []
                    edge_crossings_by_vertex[vi].append(ec_id)

                edge_key = get_edge_key(edge)
                if edge_key not in edge_crossings_by_edge:
                    edge_crossings_by_edge[edge_key] = []
                edge_crossings_by_edge[edge_key].append(ec_id)

                edge_crossing_positions = np.row_stack([edge_crossing_positions, pos_proxy])

                # Store edges
                # if i_he > 0:
                #     edge_crossing_edges = np.row_stack([edge_crossing_edges, [len(edge_crossing_positions) - 2, len(edge_crossing_positions) - 1]])

                # Store which segments cross each edge
                # e_key = get_edge_key(edge)
                # if e_key not in crossing_segments_by_edge:
                #     crossing_segments_by_edge[e_key] = []
                # crossing_segments_by_edge[e_key].append(s_idx)

    # ps_ec = ps.register_point_cloud("edge crossings", edge_crossing_positions)
    # # ps_ec.add_scalar_quantity("length", path_length_by_ec)

    # ps.register_curve_network("edge crossing network", edge_crossing_positions, edge_crossing_edges)

    # ps.register_point_cloud("projection on opt proxy", projections)

    # already_seen_edge_set = set()

    # per_edge_quantity = []

    # for f in F:
    #     for i in range(3):
    #         vA = f[i]
    #         vB = f[(i + 1) % 3]

    #         edge_key = get_edge_key([vA, vB])

    #         if edge_key in already_seen_edge_set:
    #             continue

    #         # otherwise add edge-wise quantity to the list
    #         edge_value = np.min(data_points[:, 2]) - 1
    #         if edge_key in edge_crossings_by_edge:
    #             edge_value = stroke_data_points[edge_crossings_by_edge[edge_key][0]][2]
    #         per_edge_quantity.append(edge_value)

    #         already_seen_edge_set.add(edge_key)

    # print(len(per_edge_quantity))

    # print(len(get_edges(mesh)[0]))

    # ps_proxy = ps.register_surface_mesh("proxy visualize sketch projection", V, F)
    # ps_proxy.add_scalar_quantity("edge crossing stroke index", np.array(per_edge_quantity), defined_on='edges')

    # ps_stroke_points = ps.register_point_cloud("stroke data points", stroke_data_points)
    # ps_stroke_points.add_scalar_quantity("id", stroke_data_points[:, 2])

    # ps.show()

    # Label in/out
    if point_is_on_border is not None and np.any(point_is_on_border):
        label_in_out = label_inside_outside(mesh, segments, triangle_ids, edge_crossings_by_edge, crossing_segments_id, point_is_on_border, vertex_is_on_mirror=mesh_vertex_is_on_mirror)
    else:
        label_in_out = np.zeros(len(mesh.positions()), dtype=bool)


    print("[BUILDING PROXY] Creating vertex cells objects")

    # From the edge crossings, build the graph
    vertex_cells = []

    # for vi in mesh.vertices():
    #     if vi in edge_crossings_by_vertex:
    #         crossings = edge_crossings_by_vertex[vi]
    #     else:
    #         crossings = []
    #     # node = VertexCell(vi, crossings)
    #     vertex_cells.append(crossings)

    for vi in mesh.vertices():
        vertex_crossings = []
        if label_in_out[vi]:
            # Do not associate sketch data to nodes that are outside the sketch
            vertex_cells.append([])
            continue
        for vn in mesh.circulate_vertex(vi, mode='v'):
            edge = [vi, vn]
            e_key = get_edge_key(edge)
            if e_key in edge_crossings_by_edge:
                edge_crossings = edge_crossings_by_edge[e_key]
                # Find closest crossing, if any
                if len(edge_crossings) > 0:
                    pos = mesh.positions()[vi]
                    c = edge_crossings[0]
                    for i in range(1, len(edge_crossings)):
                        if np.linalg.norm(stroke_data_points[edge_crossings[i]] - pos) < np.linalg.norm(stroke_data_points[c] - pos):
                            c = edge_crossings[i]
                    vertex_crossings.append(c)

        # node = VertexCell(vi, vertex_crossings)
        # vertex_cells.append(node)
        vertex_cells.append(vertex_crossings)

    vertex_cells = np.array(vertex_cells, dtype="object")

    # Add data from stroke segments that lie on the mirror
    if len(on_mirror_stroke_segments) > 0:
        G_primal = mesh_primal_graph(mesh)
        for s_idx in on_mirror_stroke_segments:
            endpoint_faces = triangle_ids[segments[s_idx]]
            fA, fB = endpoint_faces

            if fA == fB:
                continue

            vA_on_mirror = F[fA][np.flatnonzero(mesh_vertex_is_on_mirror[F[fA]])][0]
            vB_on_mirror = F[fB][np.flatnonzero(mesh_vertex_is_on_mirror[F[fB]])][0]

            vertex_path = nx.shortest_path(G_primal, source=vA_on_mirror, target=vB_on_mirror, weight="weight")

            N_path = len(vertex_path)
            du = 1.0 / N_path
            pA, pB = data_points[segments[s_idx]]

            # ps.register_point_cloud("vertices source", V[[vA_on_mirror, vB_on_mirror]])
            # ps.register_point_cloud("vertices path", V[vertex_path])

            # ps.show()

            for i, vid in enumerate(vertex_path):
                p_i = pA * (1 - u) + pB * u
                p_idx = len(stroke_data_points)
                stroke_data_points = np.row_stack([stroke_data_points, p_i])
                vertex_cells[vid].append(p_idx)

                ec_normal = all_proxy_mesh_normals[vid]
                ec_normal /= np.linalg.norm(ec_normal)
                proxy_normals = np.row_stack([proxy_normals, ec_normal])


    # if adaptive_proxy_mesh:
        # _, span = get_bounds(mesh.positions(), padding=0)
    proxy_graph_mesh, index_map, kept_vertices_idx, new_edges, new_edge_crossings_by_edge = simplify_graph(
        mesh,
        edge_crossings_by_vertex,
        edge_crossings_by_edge,
        label_in_out,
        remove_unused_vertices=adaptive_proxy_mesh)

    graph_edges = new_edges
    # graph_edge_weights = new_edges_weights
    vertex_cells = vertex_cells[kept_vertices_idx]

    edge_crossings_by_graph_edge = new_edge_crossings_by_edge

    # Compute per-edge "dual" length (weight of cut is set to be the distance between corners of the adjacent faces of the edge)
    if adaptive_proxy_mesh:
        per_edge_cut_cost = compute_cut_cost(proxy_graph_mesh, graph_edges)
    else:
        per_edge_cut_cost = np.ones(len(graph_edges))

    proxy = Proxy(
        mesh,
        mirror_symmetry,
        index_map,
        proxy_graph_mesh,
        vertex_cells,
        stroke_data_points,
        proxy_normals,
        crossing_segments_id,
        graph_edges,
        per_edge_cut_cost,
        edge_crossings_by_graph_edge,
        edge_crossings_by_edge,
        default_segment_sharpness,
        w0, ws, edge_length_pow,
        in_out_labelling=label_in_out)

    return proxy#, edge_crossings_by_vertex.keys()



def label_inside_outside(proxy_mesh, stroke_segments, projected_points_face, crossings_by_edge, crossing_segments_id, point_is_border, in_vertices=None, out_vertices=None, vertex_is_on_mirror=None):
    # import polyscope as ps

    print("[IN/OUT LABELLING] start")
    if out_vertices is None:
        # Find all vertices that are on mesh boundary but NOT on mirror
        out_vertices = []
        for v in proxy_mesh.vertices():
            if proxy_mesh.is_vertex_at_boundary(v):
                if vertex_is_on_mirror is None or not vertex_is_on_mirror[v]:
                    out_vertices.append(v)

        # export_to_obj(proxy_mesh.positions()[out_vertices], None, filename='/Users/emilie/Documents/Work/3d-sketch-surfacing-results/results/meshes/in_out_labelling/out_vertices.obj')

    if in_vertices is None:
        # Find all vertices that are associated with some stroke
        # and are far enough from the border strokes

        V = proxy_mesh.positions()
        F = get_faces(proxy_mesh)

        # Faces from the mesh which have a projected stroke point
        # excluding those from the boundary
        faces_with_interior_point = projected_points_face[np.logical_not(point_is_border)]

        # Faces from the mesh which have a projected stroke point from the boundary
        faces_with_boundary_point = projected_points_face[point_is_border]

        dist = pp3d.compute_distance_multisource(V,F, F[faces_with_boundary_point].flatten())

        per_face_dist = np.min(dist[F], axis = 1)
        per_interior_face_dist = per_face_dist[faces_with_interior_point]
        sorted_interior_faces = faces_with_interior_point[np.flip(np.argsort(per_interior_face_dist))]

        in_faces = sorted_interior_faces[:len(sorted_interior_faces) // 5]

        in_vertices = np.unique(F[in_faces])

        # export_to_obj(proxy_mesh.positions()[in_vertices], None, filename='/Users/emilie/Documents/Work/3d-sketch-surfacing-results/results/meshes/in_out_labelling/in_vertices.obj')

        sorting_value = np.zeros(len(F))

        sorting_value[sorted_interior_faces] = np.arange(len(sorted_interior_faces))

        # ps_proxy = ps.register_surface_mesh("proxy", V, F)
        # ps_proxy.add_scalar_quantity("distance to boundary strokes", dist, defined_on='vertices')
        # ps_proxy.add_scalar_quantity("face with points sorted", sorting_value, defined_on='faces')

        if len(in_vertices) == 0:
            # There are no in boundary strokes
            # - Find the N boundary stroke point closest to the mesh boundary
            # - Find the face they project on
            # - Find the vertex of the face that is furthest from the mesh boundary

            mesh_boundary_vertices = []
            for v in proxy_mesh.vertices():
                if proxy_mesh.is_vertex_at_boundary(v):
                    mesh_boundary_vertices.append(v)

            dist_to_mesh_boundary = pp3d.compute_distance_multisource(V,F, mesh_boundary_vertices)

            # Faces from the mesh which have a projected stroke point from the boundary
            faces_with_boundary_point = projected_points_face[point_is_border]

            # N Closest to boundary
            per_face_dist = np.min(dist_to_mesh_boundary[F], axis = 1)
            per_boundary_face_dist = per_face_dist[faces_with_boundary_point]
            sorted_boundary_faces = faces_with_boundary_point[np.flip(np.argsort(per_boundary_face_dist))]
            closest_faces_to_boundary = sorted_boundary_faces[:len(sorted_boundary_faces) // 5]

            # For each face, the vertex furthest from any mesh boundary
            verts_per_face = F[closest_faces_to_boundary]
            dists_per_verts = dist_to_mesh_boundary[verts_per_face]
            argmax_per_face = np.argmax(dists_per_verts, axis = 1)
            furthest_vert_per_face = verts_per_face[np.arange(len(verts_per_face)), argmax_per_face]

            in_vertices = np.unique(furthest_vert_per_face)



    print("[IN/OUT LABELLING] create graph")
    G = nx.Graph()

    edges, _ = get_edges(proxy_mesh)

    crossing_segments_id = np.array(crossing_segments_id, dtype=int)

    edge_weights = []

    for e in edges:
        e_key = get_edge_key(e)
        w = 1
        if e_key in crossings_by_edge:
            # Check if those crossings are from border stroke segments
            if np.any(point_is_border[stroke_segments[crossing_segments_id[crossings_by_edge[e_key]]]]):
                w = 0
        G.add_edge(e[0],e[1],capacity=w)
        edge_weights.append(w)

    in_s = len(proxy_mesh.positions())
    out_t = in_s + 1

    # ps_graph = ps.register_curve_network("graph", proxy_mesh.positions(), edges)
    # ps_graph.add_scalar_quantity("weight", 1 - np.array(edge_weights), defined_on='edges')

    # ps.register_point_cloud("in nodes", proxy_mesh.positions()[in_vertices])
    # ps.register_point_cloud("out nodes", proxy_mesh.positions()[out_vertices])

    # ps.show()

    # Add edges from the source and sink to the known in/out nodes
    for n in in_vertices:
        G.add_edge(in_s, n, capacity=100)

    for n in out_vertices:
        G.add_edge(out_t, n, capacity=100)

    print("[IN/OUT LABELLING] graph cut")

    cut_cost, subsets = nx.minimum_cut(G, in_s, out_t)

    in_subset = np.array(list(subsets[0]), dtype=int)
    in_subset = in_subset[in_subset < in_s]

    out_subset = np.array(list(subsets[1]), dtype=int)
    out_subset = out_subset[out_subset < in_s]

    vertex_labelling = np.zeros(len(proxy_mesh.positions()), dtype=bool)
    vertex_labelling[out_subset] = 1

    # ps_proxy.add_scalar_quantity("labelling", vertex_labelling)
    # ps.show()

    # vertex_labelling = proxy.property_by_vertex(node_labelling)

    return vertex_labelling

def compute_cut_cost(he_mesh, edges):
    edge_costs = []
    for e in edges:
        # Compute distance between corners of the adjacent faces
        # v0 = np.flatnonzero(index_map == e[0])[0]
        # v1 = np.flatnonzero(index_map == e[1])[0]
        he = get_halfedge(he_mesh, e[0], e[1])
        cA = he_mesh.incident_vertex(he_mesh.next_halfedge(he))
        cB = he_mesh.incident_vertex(he_mesh.next_halfedge(he_mesh.opposite_halfedge(he)))
        edge_costs.append(np.linalg.norm(he_mesh.positions()[cA] - he_mesh.positions()[cB]))
    return edge_costs