from re import sub
import numpy as np
import scipy
from scipy.optimize import minimize
import time
from pygel3d import hmesh
import igl

from main.implicit_polynomial_surface import ImplicitPolynomial, project

from utils.labelling import get_subsets_from_labels
from utils.math import cross_product_matrix, normalize, index_into_flat_3d, remove_mesh_vertices
from utils.pygel import get_faces
from main.mesh_refinement import insert_boundary_vertices
from utils.mirroring import is_on_mirror

def vec3(v):
    return v.reshape((-1,3))

def row_wise_multiply(mat, vec):
    D = scipy.sparse.diags(vec)
    return D @ mat

def E_fit(model, pts):
    return np.sum(model.compute_residuals(pts, approx='first_order')**2)

# def dE_fit(model, pts):
#     df = scipy.sparse.csr_matrix(model.compute_grad_at(pts))
#     df_norm = scipy.sparse.linalg.norm(df, axis = 1)
#     df_norm2 = df_norm**2
#     df_norm4 = df_norm2**2
#     f = model.compute_at(pts)
#     d2f = model.compute_grad2_at(pts)

#     numerator = 2 * row_wise_multiply(df, f * df_norm2) - 2 * row_wise_multiply((d2f @ df.reshape((-1,1))).reshape((-1,3)), f**2)
#     denominator = df_norm4[:, None]
#     # print("dE_fit")
#     # print(numerator / denominator)
#     return numerator / denominator

def dE_fit(model, pts):
    df = model.compute_grad_at(pts)
    df_norm = np.linalg.norm(df, axis = 1)
    df_norm2 = df_norm**2
    df_norm4 = df_norm2**2
    d2f_times_df = model.compute_directional_hessian_at(pts, grad_at_pts=df)
    f = model.compute_at(pts)

    first_term = 2 * df * f[:, None] / df_norm2[:, None]
    second_term = - 2 * d2f_times_df.reshape((-1,3)) * f[:, None]**2 / df_norm4[:, None]

    return (first_term + second_term).flatten()
    # return (numerator / denominator).flatten()

def dE_fit_approx(model, pts):
    df = model.compute_grad_at(pts)
    df_norm = np.linalg.norm(df, axis = 1)
    df_norm2 = df_norm**2
    f = model.compute_at(pts)
    numerator = 2 * df * f[:, None]
    denominator = df_norm2[:, None]
    return (numerator / denominator).flatten()


def E_fit_multi(pts, models, subsets):
    e = 0
    for l in range(len(subsets)):
        pts_idx = subsets[l]
        pts_l = pts[pts_idx]
        e += E_fit(models[l], pts_l)
    return e

def dE_fit_multi(pts, models, subsets, approx=False):
    dE = np.zeros((pts.shape[0], 3))
    for l in range(len(subsets)):
        pts_idx = subsets[l]
        pts_l = pts[pts_idx]
        if approx:
            dE[pts_idx, :] += vec3(dE_fit_approx(models[l], pts_l))
        else:
            dE[pts_idx, : ] += vec3(dE_fit(models[l], pts_l))
    return dE.flatten()

def E_smooth_mesh(V, L):
    Lv = (L @ V).flatten()
    E = np.dot(Lv, Lv)
    return E

def E_tangential_smooth_mesh(V, L, models, subsets):
    G = np.zeros(V.shape)
    for l in range(len(subsets)):
        pts_idx = subsets[l]
        pts_l = V[pts_idx]
        G[pts_idx, : ] += models[l].compute_grad_at(pts_l)
    Lv = (L @ V)
    # u = np.einsum('ij, ij->i', Lv, G)[:, None] * G
    Lt = (Lv - np.einsum('ij, ij->i', Lv, G)[:, None] * G).flatten()
    return np.dot(Lt, Lt)

def dE_tangential_smooth_mesh(V, L, models, subsets):
    return scipy.optimize.approx_fprime(V.flatten(), lambda pts: E_tangential_smooth_mesh(vec3(pts), L, models, subsets), 0.0001)


def dE_smooth_mesh(V, L):
    # print("dE_smooth_mesh")
    # print(2 * (L.T @ L @ V).flatten())
    return 2 * (L.T @ L @ V).flatten()

def E_smooth_polylines(V, L_polylines):
    E = np.trace(V.T @ L_polylines.T @ L_polylines @ V)
    return E

def dE_smooth_polylines(V, L_polylines):
    return 2 * (L_polylines.T @ L_polylines @ V).flatten()


def E_cstr(V, A_cstr, P_cstr):
    diff = (A_cstr @ V - P_cstr)
    # return np.dot(diff, diff)
    return np.trace(diff @ diff.T)

def dE_cstr(V, A_cstr, P_cstr):
    return 2 * (A_cstr.T @ A_cstr @ V - A_cstr.T @ P_cstr).flatten()

def sparse_add_value(value, i, j, data, row_indices, col_indices, flat=False):
    if flat:
        data.extend(np.repeat(value, 3))
        row_indices.extend([3 * i, 3 * i + 1, 3 * i + 2])
        col_indices.extend([3 * j, 3 * j + 1, 3 * j + 2])
    else:
        data.append(value)
        row_indices.append(i)
        col_indices.append(j)


# def Laplacian_polylines(polyline_vertices_idx, mesh):
def Laplacian_polylines(
    polyline_vertices_idx,
    boundary_vertices_idx,
    mesh,
    labelling_in_out=None,
    on_stroke_vertices_id=None,
    estimated_normals_by_vertex=None, detect_T_junctions=True,
    flat=False):
    if estimated_normals_by_vertex is None or labelling_in_out is None:
        detect_T_junctions = False

    polyline_vertices_set = set(polyline_vertices_idx)
    boundary_vertices_set = set(boundary_vertices_idx)

    data = []
    row_indices = []
    col_indices = []

    V = mesh.positions()

    N_pts = len(V)

    t_junctions_pos = np.empty((0,3))

    vertex_has_stroke = np.zeros(N_pts, dtype=bool)
    if on_stroke_vertices_id is not None:
        vertex_has_stroke[on_stroke_vertices_id] = True

    for vi in polyline_vertices_idx:
        # Find vertices that are adjacent to vertex i using the half edge mesh
        neighbors_i = []
        for v_n in mesh.circulate_vertex(vi, mode='v'):
            if v_n in polyline_vertices_set:
                neighbors_i.append(v_n)
        if len(neighbors_i) > 1:
            # if len(neighbors_i) == 3 and not detect_T_junctions:
            #     continue
            if len(neighbors_i) == 3 and detect_T_junctions:
                # If we are on an in/out boundary, smooth only along the boundary line
                if len(np.flatnonzero(labelling_in_out[neighbors_i])) == 2:
                    neighbors_i = np.array(neighbors_i, dtype=int)[np.flatnonzero(labelling_in_out[neighbors_i])]
                # # Try to align the T-junction with strokes
                # elif len(np.flatnonzero(vertex_has_stroke[neighbors_i])) == 2:
                #     neighbors_i = np.array(neighbors_i)[vertex_has_stroke[neighbors_i]]
                else:
                    t_junctions_pos = np.row_stack([t_junctions_pos, V[vi]])
                    # T-junction or sharp corner
                    # Choose to place the continuity along the edges
                    # which have the most different normal
                    # - List normals
                    normals = []
                    v_indices = []
                    for h_n in mesh.circulate_vertex(vi, mode='h'):
                        # Opposite vertex
                        v_n = mesh.incident_vertex(h_n)
                        if v_n not in neighbors_i:
                            # Get normal
                            normals.append(estimated_normals_by_vertex[v_n])
                            v_indices.append(v_n)
                    normals = np.array(normals)

                    # If we are in the simple case of 3 labelled vertices on each side
                    if len(normals) == 3:
                        # ps_pts = ps.register_point_cloud("pts",V[v_indices])
                        # ps_pts.add_vector_quantity("normals", normals)
                        # Find most different normal
                        normal_dots = np.einsum('ij, ij->i', normals, normals[[1,2,0]])
                        # print(np.abs(normal_dots))
                        most_similar_normal_pair_idx = np.argmax(np.abs(normal_dots))
                        # if 2 most similar normals are quite different, we are at a completely sharp corner
                        if normal_dots[most_similar_normal_pair_idx] < 0.5: # cos(pi/3)
                            continue
                        sorted_angles = np.sort(np.abs(normal_dots))
                        if sorted_angles[1] > 0.6 and len(np.flatnonzero(vertex_has_stroke[neighbors_i])) == 2: # all normals are kinda aligned
                            # Try to align the T-junction with strokes
                            # if len(np.flatnonzero(vertex_has_stroke[neighbors_i])) == 2:
                            neighbors_i = np.array(neighbors_i)[vertex_has_stroke[neighbors_i]]
                            # else:
                            #     # continue
                            #     neighbors_i = neighbors_i # smooth intersection
                        else:
                            most_different_normal_idx = (most_similar_normal_pair_idx - 1 + 3) % 3
                            vertex_idx_smooth_side = v_indices[most_different_normal_idx]

                            # neighbor_should_remove = [True, True, True]
                            # for v_n in mesh.circulate_vertex(vertex_idx_smooth_side, mode='v'):
                            #     if v_n in neighbors_i:
                            #         neighbor_should_remove[neighbors_i.index(v_n)] = False

                            # neighbors_i = [v_n for should_remove, v_n in zip(neighbor_should_remove, neighbors_i) if not should_remove]

                            # ps.register_point_cloud("smooth side vertex", V[vertex_idx_smooth_side].reshape((1,3)))
                            # ps.register_point_cloud("all neighbors", V[neighbors_i])

                            corrected_neighbors_i = []
                            for v_n in mesh.circulate_vertex(vertex_idx_smooth_side, mode='v'):
                                if v_n in neighbors_i:
                                    corrected_neighbors_i.append(v_n)
                                    # neighbor_should_remove[neighbors_i.index(v_n)] = False
                            

                            neighbors_i = corrected_neighbors_i

                        # ps.register_point_cloud("smooth neighbors", V[neighbors_i])
                        # ps.show()


            # Add terms to matrix:
            #  - Diagonal term
            # w_ii = -len(neighbors_i)
            w_ii = -1
            sparse_add_value(w_ii, vi, vi, data, row_indices, col_indices, flat=flat)

            #  - Off-diagonal terms
            for vj in neighbors_i:
                # w_ij = 1
                w_ij = 1 / len(neighbors_i)
                sparse_add_value(w_ij, vi, vj, data, row_indices, col_indices, flat=flat)


    for vi in boundary_vertices_idx:
        # Find vertices that are adjacent to vertex i using the half edge mesh
        neighbors_i = []
        for v_n in mesh.circulate_vertex(vi, mode='v'):
            if v_n in boundary_vertices_set:
                neighbors_i.append(v_n)
        if len(neighbors_i) > 1:
            # is vi on a seam?
            if vi in polyline_vertices_set:
                if len(neighbors_i) == 2 and detect_T_junctions:
                    normal_dot = np.dot(estimated_normals_by_vertex[neighbors_i[0]], estimated_normals_by_vertex[neighbors_i[1]])
                    if normal_dot < 0.5:
                        continue
            # Add terms to matrix:
            #  - Diagonal term
            # w_ii = -len(neighbors_i)
            w_ii = -1
            sparse_add_value(w_ii, vi, vi, data, row_indices, col_indices, flat=flat)

            #  - Off-diagonal terms
            for vj in neighbors_i:
                # w_ij = 1
                w_ij = 1 / len(neighbors_i)
                sparse_add_value(w_ij, vi, vj, data, row_indices, col_indices, flat=flat)

    if flat:
        N = 3 * N_pts
    else:
        N = N_pts

    # import polyscope as ps

    # ps.register_point_cloud("t junctions", t_junctions_pos)

    # ps.show()

    return scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(N, N))

def Laplacian_polylines_only(polyline_vertices_idx, mesh):
    polyline_vertices_set = set(polyline_vertices_idx)

    data = []
    row_indices = []
    col_indices = []

    polyline_vertices_idx = np.array(polyline_vertices_idx)

    N_pts = len(polyline_vertices_idx)

    for vi, mesh_vi in enumerate(polyline_vertices_idx):
        # Find vertices that are adjacent to vertex i using the half edge mesh
        neighbors_i = []
        for mesh_vn in mesh.circulate_vertex(mesh_vi, mode='v'):
            if mesh_vn in polyline_vertices_set:
                neighbors_i.append(mesh_vn)
        if len(neighbors_i) > 1:
            # Add terms to matrix:
            #  - Diagonal term
            # w_ii = -len(neighbors_i)
            w_ii = -1
            sparse_add_value(w_ii, vi, vi, data, row_indices, col_indices)

            #  - Off-diagonal terms
            for mesh_vj in neighbors_i:
                # w_ij = 1
                w_ij = 1 / len(neighbors_i)
                vj = np.flatnonzero(polyline_vertices_idx == mesh_vj).item()
                sparse_add_value(w_ij, vi, vj, data, row_indices, col_indices)

    return scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(N_pts, N_pts))

def Laplacian_cotan(V, F, ignore_smooth_indices=None):
    print("Building mesh Laplacian")
    M = igl.massmatrix(V, F)
    Ls = igl.cotmatrix(V, F)
    for i in range(M.shape[0]):
        M[i,i] = 1 / M[i,i]

    if ignore_smooth_indices is not None:
        # Ignore smoothness for some vertices
        Ls = zero_rows(Ls, ignore_smooth_indices)

    normalizer = 1 / M.max()

    L = normalizer * M @ Ls
    return L

def Laplacian_umbrella(proxy_mesh, ignore_boundaries=True, ignore_smooth_indices=None, flat=False):
    data = []
    row_indices = []
    col_indices = []

    boundary_vertices_idx = []

    N_pts = len(proxy_mesh.positions())

    for vi in proxy_mesh.vertices():
        if proxy_mesh.is_vertex_at_boundary(vi):
            boundary_vertices_idx.append(vi)
            if ignore_boundaries:
                continue # Ignore boundary vertices
        neighbors_i = []
        for vj in proxy_mesh.circulate_vertex(vi, mode='v'):
            neighbors_i.append(vj)
        if len(neighbors_i) > 1:
            # Add terms to matrix:
            #  - Diagonal term
            # w_ii = -len(neighbors_i)
            w_ii = -1
            sparse_add_value(w_ii, vi, vi, data, row_indices, col_indices, flat=flat)

            #  - Off-diagonal terms
            for vj in neighbors_i:
                w_ij = 1 / len(neighbors_i)
                sparse_add_value(w_ij, vi, vj, data, row_indices, col_indices, flat=flat)

    if flat:
        N = 3 * N_pts
    else:
        N = N_pts

    L = scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(N, N))

    if ignore_smooth_indices is not None:
        # Ignore smoothness for some vertices
        L = zero_rows(L, ignore_smooth_indices, flat=flat)

    return L, np.array(boundary_vertices_idx, dtype=int)
    

def smooth_seams_at_mirror(mesh, V, vertex_is_on_mirror, seam_vertices):
    N_pts = len(V)

    is_seam_vertex = np.zeros(N_pts, dtype=bool)
    is_seam_vertex[seam_vertices] = True

    is_on_mirror_seam = np.logical_and(vertex_is_on_mirror, is_seam_vertex)

    data = []
    row_indices = []
    col_indices = []


    n = np.array([1, 0, 0]).reshape((3, 1))
    Nx = cross_product_matrix(np.array([1, 0, 0]))


    for vi in np.flatnonzero(is_on_mirror_seam):
        vj = -1
        for v in mesh.circulate_vertex(vi, mode='v'):
            if is_seam_vertex[v] and not vertex_is_on_mirror[v]:
                vj = v
                break
        if vj == -1:
            continue
        edge_vec = mesh.positions()[vi] - mesh.positions()[vj] 
        edge_length2 = np.dot(edge_vec, edge_vec)
        edge_length = np.sqrt(edge_length2)

        start_idx_i = 3 * vi
        start_idx_j = 3 * vj

        A_v = 2 * Nx.T @ Nx

        # d/dvi --> block row = i, block col = i
        row_indices.extend(start_idx_i + np.array([0, 0, 0, 1, 1, 1, 2, 2, 2], dtype=int))
        col_indices.extend(start_idx_i + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        data.extend(A_v.flatten())

        # d/dvi --> block row = i, block col = j
        row_indices.extend(start_idx_i + np.array([0, 0, 0, 1, 1, 1, 2, 2, 2], dtype=int))
        col_indices.extend(start_idx_j + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        data.extend(-A_v.flatten())

        # d/dvj --> block row = j, block col = i
        row_indices.extend(start_idx_j + np.array([0, 0, 0, 1, 1, 1, 2, 2, 2], dtype=int))
        col_indices.extend(start_idx_i + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        data.extend(-A_v.flatten())

        # d/dvj --> block row = j, block col = j
        row_indices.extend(start_idx_j + np.array([0, 0, 0, 1, 1, 1, 2, 2, 2], dtype=int))
        col_indices.extend(start_idx_j + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        data.extend(A_v.flatten())


    A = scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(3 * N_pts, 3 * N_pts))

    return A

def smooth_at_mirror_matrix(V, F, vertex_is_on_mirror, seam_vertices):
    N_pts = len(V)

    # F = get_faces(proxy_mesh) 

    Nx = cross_product_matrix(np.array([1, 0, 0]))

    V_is_seam = np.isin(np.arange(len(V)), seam_vertices)

    # Find all the faces with vertices on the mirror
    faces_on_mirror = np.flatnonzero(np.sum(vertex_is_on_mirror[F].astype(int), axis = 1) == 2)

    data = []
    row_indices = []
    col_indices = []

    b = np.zeros(3 * N_pts)

    # free_vertices_indices = []
    # grad_vecs = np.empty((0,3))

    # edge_perps = np.empty((0,3))

    matrix_by_vertex = {}

    for f in faces_on_mirror:
        v_indices = F[f]

        e_mirror = v_indices[np.flatnonzero(vertex_is_on_mirror[v_indices])]
        v_free = v_indices[np.flatnonzero(np.logical_not(vertex_is_on_mirror[v_indices]))].item()

        # Does this face contain a seam edge?
        if V_is_seam[v_free] and len(e_mirror[V_is_seam[e_mirror]]) == 1:
            # Constrain free vertex such that the seam edge is aligned with the normal
            v_seam_on_mirror = e_mirror[V_is_seam[e_mirror]]

            A_f = Nx.T @ Nx

            b_f = A_f @ V[v_seam_on_mirror].reshape((3, 1))

        else:

            e_vector = V[e_mirror[1]] - V[e_mirror[0]]
            e_vector = e_vector / np.linalg.norm(e_vector)

            # Rotate vector in mirror plane by 90 degrees
            e_perp = np.array([0, -e_vector[2], e_vector[1]]).reshape((3,1))

            # edge_perps = np.row_stack([edge_perps, e_perp.T])

            # Ex = cross_product_matrix(e_vector)

            # A_f = Ex.T @ n @ n.T @ Ex

            # A_f *= np.linalg.norm(V[v_free] - V[e_mirror[0]])

            A_f = e_perp @ e_perp.T

            b_f = A_f @ V[e_mirror[0]].reshape((3, 1))

            # grad_vec = A_f @ (V[v_free] - V[e_mirror[0]])

            # free_vertices_indices.append(v_free)
            # grad_vecs = np.row_stack([grad_vecs, grad_vec])

        # Add the A_f block to the full matrix A
        # - First store matrices in a per-vertex dictionary in order to sum up correctly the blocks before putting in sparse matrix
        start_idx = 3 * v_free
        
        if v_free not in matrix_by_vertex:
            matrix_by_vertex[v_free] = A_f
        else:
            matrix_by_vertex[v_free] = matrix_by_vertex[v_free] + A_f

        b[start_idx:start_idx + 3] += b_f.flatten()

    for v_id, A_f in matrix_by_vertex.items():
        # Add the A_f block to the full matrix A
        start_idx = 3 * v_id
        row_indices.extend(start_idx + np.array([0, 0, 0, 1, 1, 1, 2, 2, 2], dtype=int))
        col_indices.extend(start_idx + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        data.extend(A_f.flatten())



    # ps_free = ps.register_point_cloud("variable pt", V[free_vertices_indices].reshape((-1,3)))
    # ps_free.add_vector_quantity("grad", grad_vecs)
    # ps_free.add_vector_quantity("perpendicular edges", edge_perps)

    # ps.show()


    A = scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(3 * N_pts, 3 * N_pts))

    return A, b

def zero_rows(M, rows, flat=False):
    if flat:
        rows = index_into_flat_3d(rows)
    diag = scipy.sparse.eye(M.shape[0]).tolil()
    diag[rows, rows] = 0
    return diag.dot(M)

def weigh_rows(M, weights, flat=False):
    rows = np.arange(len(weights))
    if flat:
        rows = index_into_flat_3d(rows)
        weights = np.repeat(weights, 3)
    diag = scipy.sparse.eye(M.shape[0]).tolil()
    diag[rows, rows] = weights
    return diag.dot(M)

def store_energy(energy_list, gradient_norm_list, current_pts, E, dE):
    energy_list.append(E(current_pts))
    gradient_norm_list.append(np.max(np.abs(dE(current_pts))))

def compute_energy(pts_flat, models, subsets, w_fit, A, B, c):
    # e_fit_multiple = np.zeros((len(models), len(pts_flat) // 3))
    if w_fit > 0:
        e_fit_by_point = np.zeros(len(pts_flat) // 3)
        counter_by_point = np.zeros(len(e_fit_by_point))

        for l in range(len(models)):
            pts_idx = subsets[l]
            flat_idx_l = index_into_flat_3d(pts_idx)
            # e_fit_multiple[[l] * len(pts_idx), pts_idx] = models[l].compute_residuals(vec3(pts_flat[flat_idx_l]), approx='first_order')**2
            e_fit_by_point[pts_idx] = models[l].compute_residuals(vec3(pts_flat[flat_idx_l]), approx='first_order')**2
            # grads[idx_l] = model_l.compute_grad_at(V[idx_l], normalize=True)
            counter_by_point[pts_idx] += 1

        # Take the mean if there are multiple non zero values (this happens at seam vertices)
        e_fit = np.sum(e_fit_by_point / counter_by_point)
        e_fit *= w_fit
    else:
        e_fit = 0

    # for l in range(len(subsets)):
    #     pts_idx = subsets[l]
    #     flat_idx_l = index_into_flat_3d(pts_idx)
    #     surface_count_per_vertex[flat_idx_l] += 1
    #     pts_l = vec3(pts_flat[flat_idx_l])
    #     e_fit += E_fit(models[l], pts_l)

    V = pts_flat
    
    return (e_fit + V.T @ A @ V - 2 * np.dot(pts_flat, B)).item() + c

def compute_gradient(pts_flat, models, subsets, w_fit, A, B, fix_vertex_coord, approx=True):
    V = pts_flat
    grad_fit = np.zeros(len(pts_flat))

    if w_fit > 0:
        counter_by_point = np.zeros(len(pts_flat))
        # grad_fit_multiple = np.zeros((len(models), len(pts_flat)))
        # dE = np.zeros((pts.shape[0], 3))
        for l in range(len(subsets)):
            pts_idx = subsets[l]
            flat_idx_l = index_into_flat_3d(pts_idx)
            pts_l = vec3(pts_flat[flat_idx_l])
            if approx:
                grad_fit[flat_idx_l] += dE_fit_approx(models[l], pts_l)
                # grad_fit_multiple[[l] * len(flat_idx_l), flat_idx_l] = dE_fit_approx(models[l], pts_l)
            else:
                # print(np.ravel(dE_fit(models[l], pts_l)).shape)
                grad_fit[flat_idx_l] += np.ravel(dE_fit(models[l], pts_l))
                # grad_fit_multiple[[l] * len(flat_idx_l), flat_idx_l] = np.ravel(dE_fit(models[l], pts_l))

            counter_by_point[flat_idx_l] += 1

        # Take the mean if there are multiple non zero values (this happens at seam vertices)
        grad_fit = grad_fit / counter_by_point
        grad_fit *= w_fit

    G = grad_fit + (A @ V) - B

    # Fix on-mirror vertices (x coordinate)
    G[fix_vertex_coord] = 0

    return G

import polyscope as ps
counter = 0
def visualize_it(pts_flat, F, grad):
    global counter
    counter += 1

    if counter % 10 == 0:
        ps_mesh = ps.register_surface_mesh("iteration", vec3(pts_flat), F)

        # Gradients
        grads = -vec3(grad(pts_flat))

        ps_mesh.add_vector_quantity("gradients", grads, defined_on='vertices')

        ps.show()


# def optimize_mesh(mesh, V, models, subsets, smoothing_weights, polylines_idx, w_fit, w_smooth, w_smooth_boundaries, maxIt=500, gtol=1e-3):
def optimize_mesh(
    mesh, V,
    is_on_mirror,
    labelling_in_out,
    models, subsets,
    smoothing_weights,
    seams_idx,
    w_fit, w_smooth, w_smooth_boundaries, w_cstr=0, w_smooth_mirror=0,
    constrained_vertices_idx=None, constrained_pt_target=None,
    constrained_pt_weights=None,
    maxIt=200, gtol=1e-3,
    detect_T_junctions=True,
    approx_df=True,
    iprint=False):

    start_time = time.perf_counter()

    # L = Laplacian_cotan(V, F)
    L, boundary_vertices_idx = Laplacian_umbrella(mesh, flat=True)

    estimated_normals_by_vertex = np.ones((len(V), 3))
    for m, pts_idx in zip(models, subsets):
        estimated_normals_by_vertex[pts_idx] = m.normal_at(V[pts_idx])

    L_polylines = Laplacian_polylines(
        seams_idx,
        boundary_vertices_idx,
        mesh,
        labelling_in_out,
        constrained_vertices_idx,
        estimated_normals_by_vertex,
        detect_T_junctions,
        flat=True)

    L = weigh_rows(L, smoothing_weights, flat=True)

    # Normalizers
    # w_normalize_2D = 1 / np.sum(smoothing_weights)
    # w_normalize_1D = 1 / len(seams_idx)

    # w_smooth = 0 * w_normalize_2D
    # w_smooth_boundaries = 200

    # print(w_smooth, w_smooth_boundaries)

    # w_smooth = 0
    # w_smooth_boundaries = 0

    # Compute linear part matrices
    A = w_smooth * L.T @ L + w_smooth_boundaries * L_polylines.T @ L_polylines
    B = np.zeros(len(V) * 3)
    c = 0

    if constrained_vertices_idx is not None:
        if constrained_pt_weights is None:
            constrained_pt_weights = np.ones(len(constrained_vertices_idx))
        # N = len(mesh.positions())
        # M = len(constrained_vertices_idx)
        # A_cstr = scipy.sparse.csr_matrix((np.ones(M), (np.arange(M), np.array(constrained_vertices_idx))), shape=(M, N))
        # P_cstr = constrained_pt_target
        # w_cstr = 1

        N = len(V)
        M = len(constrained_vertices_idx)
        # A = scipy.sparse.csr_matrix((np.ones(M), (np.arange(M), np.array(constrained_vertices_idx))), shape=(M, N))
        row_indices = index_into_flat_3d(np.arange(M))
        col_indices = index_into_flat_3d(constrained_vertices_idx)
        A_cstr = scipy.sparse.csr_matrix((np.ones(3 * M), (row_indices, col_indices)), shape=(3 * M, 3 * N))

        A_cstr = weigh_rows(A_cstr, np.sqrt(constrained_pt_weights), flat=True)
        A += w_cstr * A_cstr.T @ A_cstr
        P = (constrained_pt_target * np.sqrt(constrained_pt_weights)[:, None]).flatten()
        B += w_cstr * A_cstr.T @ P
        c += w_cstr * np.dot(P, P)
        # E = lambda pts: w_fit * E_fit_multi(vec3(pts), models, subsets) + w_smooth * E_smooth_mesh(vec3(pts), L) + w_smooth_boundaries * E_smooth_polylines(vec3(pts), L_polylines) + w_cstr * E_cstr(vec3(pts), A_cstr, P_cstr)
    #     dE = lambda pts: w_fit * dE_fit_multi(vec3(pts), models, subsets, approx=approx_df) + w_smooth * dE_smooth_mesh(vec3(pts), L) + w_smooth_boundaries * dE_smooth_polylines(vec3(pts), L_polylines) + w_cstr * dE_cstr(vec3(pts), A_cstr, P_cstr)


    # else:
    #     E = lambda pts: w_fit * E_fit_multi(vec3(pts), models, subsets) + w_smooth * E_smooth_mesh(vec3(pts), L) + w_smooth_boundaries * E_smooth_polylines(vec3(pts), L_polylines)
    #     dE = lambda pts: w_fit * dE_fit_multi(vec3(pts), models, subsets, approx=approx_df) + w_smooth * dE_smooth_mesh(vec3(pts), L) + w_smooth_boundaries * dE_smooth_polylines(vec3(pts), L_polylines)

    fix_vertex_coord = np.zeros(len(V) * 3, dtype=bool)

    if is_on_mirror is not None and np.any(is_on_mirror):
        fix_vertex_coord[3 * np.flatnonzero(is_on_mirror)] = True
        if w_smooth_mirror > 0:
            A_m = smooth_seams_at_mirror(mesh, V, is_on_mirror, seams_idx)
            A += w_smooth_mirror * A_m

    # w_normalize_fit = len(V) # total nb of points
    w_normalize_fit = 1
    # w_fit = 0.1

    E = lambda pts : compute_energy(pts, models, subsets, w_fit/w_normalize_fit, A, B, c)
    dE = lambda pts : compute_gradient(pts, models, subsets, w_fit/w_normalize_fit, A, B, fix_vertex_coord, approx=approx_df)

    print("energy", E(V.flatten()))

    print("gradient", dE(V.flatten()))

    energy_list = []
    gradient_norm_list = []

    store = lambda pts: store_energy(energy_list, gradient_norm_list, pts, E, dE)
    print_it = lambda x: print("Iteration done, f = {}".format(E(x)))
    vis_it = lambda pts: visualize_it(pts, get_faces(mesh), dE)


    t0 = time.perf_counter()
    res = minimize(E, V, method='L-BFGS-B', jac=dE, callback=store,
            #    options={'disp': True, 'maxiter': maxIt,'gtol': gtol})
            options = {'gtol': gtol, 'iprint': 1 if iprint else 0, 'maxiter': maxIt, 'maxcor': 50})
    t1 = time.perf_counter()
    print("Minimized in {}s".format(t1 - t0))
    # res = gradient_descent(E, dE, V.flatten(), 200, 0.002)

    print(res)

    V = vec3(res.x)

    end_time = time.perf_counter()

    print(f"Total time for optimization = {end_time - start_time}s")

    # if is_on_mirror is not None and np.any(is_on_mirror) and w_smooth_mirror > 0:
    #     V = smooth_at_mirror(A, B, V, get_faces(mesh), is_on_mirror, seams_idx, w_smooth_mirror)

    return V, energy_list, gradient_norm_list


def smooth_at_mirror(A, B, V, F, vertex_is_on_mirror, seam_vertices_idx, lambda_smooth_mirror):
    on_mirror_vertices = np.flatnonzero(vertex_is_on_mirror)
    # Fix on-mirror vertices (they are not allowed to move)
    A_all = zero_rows(A, on_mirror_vertices, flat=True)
    B = B.copy()
    B[index_into_flat_3d(on_mirror_vertices)] = 0

    # Add term to promote smoothness across mirror
    mirror_smoothness_A, mirror_smoothness_b = smooth_at_mirror_matrix(V, F, vertex_is_on_mirror, seam_vertices_idx)

    A_all += lambda_smooth_mirror * mirror_smoothness_A
    B += lambda_smooth_mirror * mirror_smoothness_b

    # - Add identity preserving term
    A_all += scipy.sparse.identity(len(V) * 3)
    B += V.flatten()


    # solve
    V = (scipy.sparse.linalg.spsolve(A_all, B)).reshape((-1, 3))

    return V

def project_to_models(V, models, subsets, maxIt=50, gtol=1e-3):
    # print(subsets)
    E = lambda pts: E_fit_multi(vec3(pts), models, subsets) 
    dE = lambda pts: dE_fit_multi(vec3(pts), models, subsets)

    t0 = time.perf_counter()
    res = scipy.optimize.minimize(E, V, method='L-BFGS-B', jac=dE,
               options={'disp': False, 'maxiter': maxIt,'gtol': gtol})
    t1 = time.perf_counter()
    print("Minimized in {}s".format(t1 - t0))

    return vec3(res.x)

def smooth_project_to_models(mesh, models, subsets, is_on_mirror=None, in_out_labelling=None, lambda_smooth=1, lambda_smooth_boundaries=1, maxIt=10, gtol=1e-3):

    V = mesh.positions().copy()

    V0 = mesh.positions()

    lambda_fit = 1

    n = np.array([1,0,0])

    # Find on-mirror points
    if is_on_mirror is not None:
        on_mirror_vertices = np.flatnonzero(is_on_mirror)
    else:
        on_mirror_vertices = []

    L, boundary_vertices_idx = Laplacian_umbrella(mesh)
    # L_boundary = Laplacian_polylines(boundary_vertices_idx, mesh)

    # zero_rows(L_boundary, on_mirror_vertices)

    # Remove on-mirror vertices from boundary vertices list
    # boundary_vertices_idx = boundary_vertices_idx[np.logical_not(np.isin(boundary_vertices_idx, on_mirror_vertices))]

    for i in range(maxIt):
        V_i = V.copy()
        print("Iteration {}".format(i))
        # Project to models
        for l in range(len(models)):
            model_l = models[l]
            idx_l = subsets[l]
            projs = model_l.project_pts(V[idx_l], step_size=lambda_fit)
            if is_on_mirror is not None:
                is_on_mirror_l = is_on_mirror[idx_l]
                # Remove mirror normal component for projection of on mirror vertices
                if len(projs[is_on_mirror_l]) > 0:
                    projs[is_on_mirror_l] = projs[is_on_mirror_l] - np.dot(projs[is_on_mirror_l], n).reshape((len(projs[is_on_mirror_l]), 1)) * n.reshape((1,3))
            V[idx_l] = projs
        
        # Do not project outside points
        if in_out_labelling is not None:
            V[in_out_labelling] = V0[in_out_labelling]

        # Reset boundaries
        # V[boundary_vertices_idx] = V0[boundary_vertices_idx]

        # Smooth
        A = scipy.sparse.identity(len(V)) + lambda_smooth * L.T @ L #+ lambda_smooth_boundaries * L_boundary.T @ L_boundary
        B = V

        # solve
        V = scipy.sparse.linalg.spsolve(A, B)

        G = V_i - V
        g_max_norm = max(np.linalg.norm(G, axis = 1))

        print("g max =", g_max_norm)

        if g_max_norm <= gtol:
            break

    # Reset mirror vertices onto the mirror exactly
    if is_on_mirror is not None:
        V[is_on_mirror, 0] = 0

    return V



def optimize_boundary_vertices(mesh, models, subsets, polylines_idx, w_fit, w_smooth_boundary, maxIt=500, gtol=1e-4):
    L = Laplacian_polylines_only(polylines_idx, mesh)

    E = lambda pts: w_fit * E_fit_multi(vec3(pts), models, subsets) + w_smooth_boundary * E_smooth_polylines(vec3(pts), L)
    dE = lambda pts: w_fit * dE_fit_multi(vec3(pts), models, subsets) + w_smooth_boundary * dE_smooth_polylines(vec3(pts), L)

    V0 = mesh.positions()[polylines_idx]

    t0 = time.perf_counter()
    res = scipy.optimize.minimize(E, V0, method='L-BFGS-B', jac=dE,
               options={'disp': True, 'maxiter': maxIt,'gtol': gtol})
    t1 = time.perf_counter()
    print("Minimized in {}s".format(t1 - t0))

    return vec3(res.x)

def optimize_boundary_vertices_iterative(mesh, models, subsets, polylines_idx, lambda_fit, lambda_smooth_seams, maxIt=50, g_max_stop=1e-4):
    L = Laplacian_polylines_only(polylines_idx, mesh)

    V0 = mesh.positions()[polylines_idx]
    V = V0

    N = len(V)

    if N == 0:
        return np.empty((0,3))

    for i in range(maxIt):
        print("Iteration {}".format(i))
        # - Project to surfaces
        V_i = V.copy()

        V_multiple = np.zeros((len(models), V.shape[0], V.shape[1]))

        for l in range(len(models)):
            model_l = models[l]
            idx_l = subsets[l]
            V_multiple[[l] * len(idx_l), idx_l, :] = model_l.project_pts(V[idx_l], step_size=lambda_fit)

        # Take the mean if there are multiple non zero values (this happens at seam vertices)
        non_zeros_count = np.count_nonzero(np.linalg.norm(V_multiple, axis = 2), axis = 0)
        non_zeros_count[non_zeros_count == 0] = 1
        V = np.sum(V_multiple, axis = 0) / non_zeros_count[:, None]


        # - Smooth and match seams to strokes
        A_all = scipy.sparse.identity(N) + lambda_smooth_seams * L.T @ L
        B = V

        # solve
        V = scipy.sparse.linalg.spsolve(A_all, B)

        G = V_i - V
        g_max_norm = max(np.linalg.norm(G, axis = 1))

        # print("g max =", g_max_norm)

        if g_max_norm <= g_max_stop:
            break

    # import polyscope as ps
    # ps.register_point_cloud("boundary points initial", V0)
    # ps.register_point_cloud("boundary points optimized", V)

    # ps.show()

    return V


def ReLU(x, thresh_min=0, thresh_max=1):

    x2 = (x - thresh_min) / (thresh_max-thresh_min)
    return np.clip(x2, 0, 1)


def compute_smoothing_weights(proxy_mesh, models, boundary_vertices_subsets, boundary_vertices_id, w_fit, g_max_stop):

    # - Project boundary vertices onto the intersection
    # V_boundary_opt = optimize_boundary_vertices(
    #     proxy_mesh, models, boundary_vertices_subsets, boundary_vertices_id,
    #     w_fit=w_fit, w_smooth_boundary=10, maxIt=50)

    V_boundary_opt = optimize_boundary_vertices_iterative(
        proxy_mesh, models, boundary_vertices_subsets, boundary_vertices_id,
        lambda_fit=w_fit, lambda_smooth_seams=0, maxIt=3, g_max_stop=g_max_stop)

    # import polyscope as ps

    # ps.register_point_cloud("boundary initial", proxy_mesh.positions()[boundary_vertices_id])
    # ps.register_point_cloud("boundary final", V_boundary_opt)

    # ps.show()

    # # - Evaluate the normals of the 2 models at each boundary point
    per_vertex_multiple_normals = np.zeros((len(models), len(V_boundary_opt), 3))
    for l, (model, subset) in enumerate(zip(models, boundary_vertices_subsets)):
        # Compute normals for all vertices from the subset
        pts = V_boundary_opt[subset]
        normals = model.normal_at(pts)
        per_vertex_multiple_normals[[l] * len(subset), subset, :] = normals
    
    sharpness = np.zeros(len(V_boundary_opt))
    vertex_model_pair_has_normal = np.linalg.norm(per_vertex_multiple_normals, axis = 2) > 0
    non_zeros_count_normals = np.count_nonzero(vertex_model_pair_has_normal, axis = 0)
    vertices_with_two_normals = np.flatnonzero(non_zeros_count_normals == 2)
    non_zero_normal_indices = np.nonzero(np.linalg.norm(per_vertex_multiple_normals[:, vertices_with_two_normals, :], axis = 2).T)[1]
    # print(non_zero_normal_indices)
    non_zero_normal_indices = non_zero_normal_indices.reshape((-1, 2))
    sharpness[vertices_with_two_normals] = np.abs(
                                            np.einsum(
                                                'ij,ij->i',
                                                per_vertex_multiple_normals[non_zero_normal_indices[:, 0], vertices_with_two_normals, :],
                                                per_vertex_multiple_normals[non_zero_normal_indices[:, 1], vertices_with_two_normals, :]
                                            ))

    smoothing_weights = np.ones(len(proxy_mesh.positions()))
    # alpha_min = pi/3, alpha_max = pi/8
    smoothing_weights[boundary_vertices_id] = ReLU(np.array(sharpness), thresh_min=0.5, thresh_max = 0.9)
    # smoothing_weights[boundary_vertices_id] = np.array(sharpness)**power

    return smoothing_weights


def smooth_proxy_trimmed_boundary(proxy_mesh, labelling_in_out, stroke_pt_by_edge):
    proxy_mesh_cp = hmesh.Manifold(proxy_mesh)
    # Insert vertices on edges from the trim boundary
    seam_vertices_id, boundary_vertices_subsets, subsets, on_stroke_vertices_id, constraint_pts, attraction_is_mandatory, in_out_labelling, vertex_is_on_mirror = insert_boundary_vertices(
        proxy_mesh_cp,
        labels = np.zeros(len(proxy_mesh_cp.positions()), dtype=int),
        stroke_position_by_edge = stroke_pt_by_edge,
        in_out_labelling = labelling_in_out,
        vertex_is_on_mirror = is_on_mirror(proxy_mesh_cp.positions()))

    # Smooth out that trim boundary polyline, while keeping close to original positions
    # L_trim_polyline = Laplacian_polylines_only(seam_vertices_id, proxy_mesh_cp)
    L, boundary_vertices_idx = Laplacian_umbrella(proxy_mesh_cp, flat=False)
    L_trim_polyline = Laplacian_polylines(
        seam_vertices_id,
        boundary_vertices_idx,
        proxy_mesh_cp,
        in_out_labelling,
        on_stroke_vertices_id,
        detect_T_junctions=False,
        flat=False
    )

    # print(L_trim_polyline.shape)

    # A_fit = scipy.sparse.csr_matrix((np.ones(M), (np.arange(M), np.array(constrained_vertices_idx))), shape=(M, N))

    b_fit = proxy_mesh_cp.positions().copy()

    w_smooth = 100
    A = w_smooth * L_trim_polyline.T @ L_trim_polyline + scipy.sparse.identity(L_trim_polyline.shape[0])
    b = b_fit

    opt_V = scipy.sparse.linalg.spsolve(A, b)
    # print(opt_V.shape)
    
    # Drop vertices labelled as outside
    trimmed_V, trimmed_F, V_mask = remove_mesh_vertices(opt_V, get_faces(proxy_mesh_cp), in_out_labelling)

    return trimmed_V, trimmed_F



###### BELOW IS OLD STUFF


def attract_seams_to_strokes(proxy_mesh, polylines_idx, smoothing_weights, constrained_vertices_idx, constrained_target_positions, w_smooth, w_polylines, w_fit):
    # L = Laplacian_cotan(proxy_mesh.positions(), get_faces(proxy_mesh))
    L, _ = Laplacian_umbrella(proxy_mesh)
    L_polylines = Laplacian_polylines(polylines_idx, proxy_mesh)

    L = weigh_rows(L, smoothing_weights)

    N = len(proxy_mesh.positions())
    M = len(constrained_vertices_idx)

    # A_fit = np.zeros((M, N))

    # A_fit[np.arange(M), constrained_vertices_idx]

    A_fit = scipy.sparse.csr_matrix((np.ones(M), (np.arange(M), np.array(constrained_vertices_idx))), shape=(M, N))

    b_fit = A_fit.T @ constrained_target_positions

    A = w_smooth * L.T @ L + w_polylines * L_polylines.T @ L_polylines + w_fit * A_fit.T @ A_fit
    b = b_fit

    X = scipy.sparse.linalg.spsolve(A, b)

    return X


def optimize_mesh_iterative(mesh, V, is_on_mirror, models, subsets, smoothing_weights, polylines_idx, lambda_fit, lambda_smooth, lambda_smooth_mirror, lambda_smooth_seams, lambda_precise_seams, constrained_vertices_idx, constrained_pt_target, constrained_pt_weights=None, labelling_in_out=None, nIt=10, g_max_stop=1e-3, detect_T_junctions=True):

    V0 = V.copy()
    # import polyscope as ps
    L, boundary_vertices_idx = Laplacian_umbrella(mesh, flat=True)

    if is_on_mirror is not None:
        on_mirror_vertices = np.flatnonzero(is_on_mirror)
    else:
        on_mirror_vertices = []

    if constrained_pt_weights is None:
        constrained_pt_weights = np.ones(len(constrained_vertices_idx))

    n = np.array([1, 0, 0])

    print(g_max_stop)

    estimated_normals_by_vertex = np.ones((len(V), 3))
    for m, pts_idx in zip(models, subsets):
        estimated_normals_by_vertex[pts_idx] = m.normal_at(V[pts_idx])

    # all_seams = np.append(polylines_idx, boundary_vertices_idx)

    L_polylines = Laplacian_polylines(
        polylines_idx,
        boundary_vertices_idx,
        mesh,
        labelling_in_out,
        estimated_normals_by_vertex, detect_T_junctions=detect_T_junctions,
        flat=True)

    # L_polylines = zero_rows(L_polylines, on_mirror_vertices, flat=True)
    # L_polylines = zero_rows(L_polylines, 3 * on_mirror_vertices, flat=False)

    # L_boundary = Laplacian_polylines(boundary_vertices_idx, mesh, flat=True)
    # L_polylines = L_polylines + L_boundary

    L = weigh_rows(L, np.sqrt(smoothing_weights), flat=True)

    # L = zero_rows(L, on_mirror_vertices)
    # L = zero_rows(L, 3 * on_mirror_vertices, flat=False)
    # L = zero_rows(L, boundary_vertices_idx, flat=True)

    N = len(V)
    M = len(constrained_vertices_idx)
    # A = scipy.sparse.csr_matrix((np.ones(M), (np.arange(M), np.array(constrained_vertices_idx))), shape=(M, N))
    row_indices = index_into_flat_3d(np.arange(M))
    col_indices = index_into_flat_3d(constrained_vertices_idx)
    A = scipy.sparse.csr_matrix((np.ones(3 * M), (row_indices, col_indices)), shape=(3 * M, 3 * N))

    A = weigh_rows(A, np.sqrt(constrained_pt_weights), flat=True)

    b_fit = A.T @ (constrained_pt_target * np.sqrt(constrained_pt_weights)[:, None]).flatten()
    # b_fit = zero_rows(b_fit, 3 * on_mirror_vertices, flat=False)

    A_smoothing = L.T @ L
    A_smooth_polylines = L_polylines.T @ L_polylines
    A_seam_match = A.T @ A

    # A_seam_match = zero_rows(A_seam_match, 3 * on_mirror_vertices, flat=False)

    F = get_faces(mesh)

    E = lambda pts: lambda_fit * E_fit_multi(vec3(pts), models, subsets) + lambda_smooth * E_smooth_mesh(vec3(pts), L) + lambda_smooth_seams * E_smooth_polylines(vec3(pts), L_polylines) + lambda_precise_seams * E_cstr(vec3(pts), A, constrained_pt_target)
    
    for i in range(nIt):
        # print("Energy before projection = {}".format(E(V.flatten())))
        print("Iteration {}".format(i))
        # - Project to surfaces
        V_i = V.copy()

        # grads = np.zeros(V.shape)

        V_multiple = np.zeros((len(models), V.shape[0], V.shape[1]))

        for l in range(len(models)):
            model_l = models[l]
            idx_l = subsets[l]
            # V[idx_l] = model_l.project_pts(V[idx_l], step_size=lambda_fit)
            projs = model_l.project_pts(V[idx_l], step_size=lambda_fit)
            if is_on_mirror is not None:
                is_on_mirror_l = is_on_mirror[idx_l]
                # Remove mirror normal component for projection of on mirror vertices
                if len(projs[is_on_mirror_l]) > 0:
                    # projs[is_on_mirror_l] = projs[is_on_mirror_l] - np.dot(projs[is_on_mirror_l], n).reshape((len(projs[is_on_mirror_l]), 1)) * n.reshape((1,3))
                    projs[is_on_mirror_l, 0] = 0

            V_multiple[[l] * len(idx_l), idx_l, :] = projs
            # grads[idx_l] = model_l.compute_grad_at(V[idx_l], normalize=True)

        # Take the mean if there are multiple non zero values (this happens at seam vertices)
        non_zeros_count = np.count_nonzero(np.linalg.norm(V_multiple, axis = 2), axis = 0)
        non_zeros_count[non_zeros_count == 0] = 1
        V = np.sum(V_multiple, axis = 0) / non_zeros_count[:, None]

        # print("Energy after projection = {}".format(E(V.flatten())))

        # ps.register_surface_mesh("intermediate mesh 1", V, F)
        # ps.show()

        # - Smooth and match seams to strokes
        A_all = lambda_smooth * A_smoothing + lambda_smooth_seams * A_smooth_polylines + lambda_precise_seams * A_seam_match
        B = lambda_precise_seams * b_fit
        # if is_on_mirror is not None and lambda_smooth_mirror != 0:
        #     # Fix on-mirror vertices (they are not allowed to move)
        #     A_all = zero_rows(A_all, on_mirror_vertices, flat=True)
        #     B[index_into_flat_3d(on_mirror_vertices)] = 0

        #     # Add term to promote smoothness across mirror
        #     mirror_smoothness_A, mirror_smoothness_b = smooth_at_mirror_matrix(V, F, is_on_mirror, polylines_idx, None)

        #     A_all += lambda_smooth_mirror * mirror_smoothness_A
        #     B += lambda_smooth_mirror * mirror_smoothness_b

        # - Add identity preserving term
        A_all += scipy.sparse.identity(N * 3)
        B += V.flatten()


        # solve
        V = (scipy.sparse.linalg.spsolve(A_all, B)).reshape((-1, 3))

        V[on_mirror_vertices, 0] = 0

        # print("Energy after smoothing = {}".format(E(V.flatten())))
        # print("Energy = {}".format(E(V.flatten())))

        G = V_i - V
        if labelling_in_out is not None:
            # Do not take into account vertices outside of the boundaries
            G = G[np.logical_not(labelling_in_out)]
        g_max_norm = max(np.linalg.norm(G, axis = 1))

        print("g max =", g_max_norm)

        if g_max_norm <= g_max_stop:
            break

        # ps.register_surface_mesh("intermediate mesh 2", V, F)
        # ps.show()

    # A_all = lambda_smooth * A_smoothing + lambda_smooth_seams * A_smooth_polylines + lambda_precise_seams * A_seam_match
    # B = lambda_precise_seams * b_fit
    if is_on_mirror is not None and lambda_smooth_mirror != 0:
        A_all = lambda_smooth * A_smoothing + lambda_smooth_seams * A_smooth_polylines + lambda_precise_seams * A_seam_match
        B = lambda_precise_seams * b_fit
        # Fix on-mirror vertices (they are not allowed to move)
        A_all = zero_rows(A_all, on_mirror_vertices, flat=True)
        B[index_into_flat_3d(on_mirror_vertices)] = 0

        # Add term to promote smoothness across mirror
        mirror_smoothness_A, mirror_smoothness_b = smooth_at_mirror_matrix(V, F, is_on_mirror, polylines_idx, None)

        A_all += lambda_smooth_mirror * mirror_smoothness_A
        B += lambda_smooth_mirror * mirror_smoothness_b

        # - Add identity preserving term
        A_all += scipy.sparse.identity(N * 3)
        B += V.flatten()


            # solve
        V = (scipy.sparse.linalg.spsolve(A_all, B)).reshape((-1, 3))

    # Reset mirror vertices onto the mirror exactly
    if is_on_mirror is not None:
        V[is_on_mirror, 0] = 0

    return V

def build_tangential_constraint_matrix(vertex_index, position_at_vertex, normal_at_vertex, N_v, seam_vertices):
    # N_constr = len(position_at_vertex)

    data = []
    row_indices = []
    col_indices = []

    Bc = []

    c_idx = 0

    for idx, pos, normal in zip(vertex_index, position_at_vertex, normal_at_vertex):
        if idx in seam_vertices:
            continue
        start_idx = 3 * idx
        col_indices.extend(start_idx + np.array([0, 1, 2], dtype=int))
        # col_indices.extend(start_idx + np.array([0, 1, 2, 0, 1, 2, 0, 1, 2], dtype=int))
        row_indices.extend(np.repeat(c_idx, 3))
        data.extend(normal.flatten())
        # print(normal.shape)
        Bc.append(np.dot(normal, pos))
        c_idx += 1
    
    N_constr = len(Bc)
    Bc = np.array(Bc)

    C = scipy.sparse.csr_matrix((np.array(data), (np.array(row_indices), np.array(col_indices))), shape=(N_constr, 3 * N_v))

    return C, Bc


def optimize_mesh_tangential(mesh, is_on_mirror, seam_vertices, models, subsets, labelling_in_out, smoothing_weights, lambda_fit, lambda_smooth, lambda_smooth_seams, lambda_smooth_mirror, tangential_cstr=True):
    import polyscope as ps
    F = get_faces(mesh)

    V0 = mesh.positions().copy()
    V = V0.copy()

    L, boundary_vertices_idx = Laplacian_umbrella(mesh, flat=True)

    estimated_normals_by_vertex = np.ones((len(V), 3))
    for m, pts_idx in zip(models, subsets):
        estimated_normals_by_vertex[pts_idx] = m.normal_at(V[pts_idx])

    L_polylines = Laplacian_polylines(
    seam_vertices,
    boundary_vertices_idx,
    mesh,
    labelling_in_out,
    estimated_normals_by_vertex, detect_T_junctions=True,
    flat=True)

    L = weigh_rows(L, np.sqrt(smoothing_weights), flat=True)

    if is_on_mirror is not None:
        on_mirror_vertices = np.flatnonzero(is_on_mirror)
    else:
        on_mirror_vertices = []


    A_smoothing = L.T @ L
    A_polylines = L_polylines.T @ L_polylines


    for i in range(1):
        # Project all vertices unto their surface or surface intersection
        V_multiple = np.zeros((len(models), V.shape[0], V.shape[1]))

        vertex_indices = []
        vertex_positions = np.empty((0,3))
        vertex_normals = np.empty((0,3))

        for l in range(len(models)):
            model_l = models[l]
            idx_l = subsets[l]
            # V[idx_l] = model_l.project_pts(V[idx_l], step_size=lambda_fit)
            projs = model_l.project_pts(V[idx_l], step_size=lambda_fit)
            # projs = project(V[idx_l], model_l, 1e-6)
            if is_on_mirror is not None:
                is_on_mirror_l = is_on_mirror[idx_l]
                # Remove mirror normal component for projection of on mirror vertices
                if len(projs[is_on_mirror_l]) > 0:
                    # projs[is_on_mirror_l] = projs[is_on_mirror_l] - np.dot(projs[is_on_mirror_l], n).reshape((len(projs[is_on_mirror_l]), 1)) * n.reshape((1,3))
                    projs[is_on_mirror_l, 0] = 0

            V_multiple[[l] * len(idx_l), idx_l, :] = projs
            # grads[idx_l] = model_l.compute_grad_at(V[idx_l], normalize=True)

            # Store tangential constraints data
            vertex_indices.extend(idx_l)
            vertex_positions = np.row_stack([vertex_positions, projs])
            vertex_normals = np.row_stack([vertex_normals, model_l.normal_at(projs)])

        # Take the mean if there are multiple non zero values (this happens at seam vertices)
        non_zeros_count = np.count_nonzero(np.linalg.norm(V_multiple, axis = 2), axis = 0)
        non_zeros_count[non_zeros_count == 0] = 1
        V = np.sum(V_multiple, axis = 0) / non_zeros_count[:, None]

        ps.register_surface_mesh("intermediate mesh 1", V, F)
        ps.show()

        # Solve linear problem constrained to stay in the surface tangent space
        A_all = lambda_smooth * A_smoothing + lambda_smooth_seams * A_polylines + scipy.sparse.identity(len(V0) * 3)
        B = np.zeros(3 * len(V0)) + V.flatten()

        if tangential_cstr:

            C, Bc = build_tangential_constraint_matrix(vertex_indices, vertex_positions, vertex_normals, len(V0), seam_vertices)

            A_all = scipy.sparse.bmat([
                [A_all, C.T], 
                [C, None]
                ], format='csr')

            B = np.concatenate([B, Bc])

        V = (scipy.sparse.linalg.spsolve(A_all, B)[: 3 * len(V0)]).reshape((-1, 3))

        V[on_mirror_vertices, 0] = 0

        ps.register_surface_mesh("intermediate mesh 2", V, F)
        ps.show()

    # Last step : simple smoothing
    A_all = lambda_smooth * A_smoothing + lambda_smooth_seams * A_polylines# + lambda_precise_seams * A_seam_match
    B = np.zeros(len(V) * 3)
    if is_on_mirror is not None and lambda_smooth_mirror != 0:
        
        # Fix on-mirror vertices (they are not allowed to move)
        A_all = zero_rows(A_all, on_mirror_vertices, flat=True)
        B[index_into_flat_3d(on_mirror_vertices)] = 0

        # Add term to promote smoothness across mirror
        mirror_smoothness_A, mirror_smoothness_b = smooth_at_mirror_matrix(V, F, is_on_mirror, seam_vertices, None)

        A_all += lambda_smooth_mirror * mirror_smoothness_A
        B += lambda_smooth_mirror * mirror_smoothness_b

    # - Add identity preserving term
    A_all += scipy.sparse.identity(len(V) * 3)
    B += V.flatten()


    # solve
    V = (scipy.sparse.linalg.spsolve(A_all, B)).reshape((-1, 3))

    return V
        


# def df2(model, pts):
#     d2f = model.compute_grad2_at(pts)
#     df = model.compute_grad_at(pts)
#     return 2 * d2f.T @ df.flatten()

# pts = np.random.rand(2, 3)
# model = ImplicitPolynomial(
#     np.array([0.0, -0.02312966102400747, 0.00280041682034973, -0.00756471956551144, 0.007661877949549624, -0.007269949390383727, 0.03425962332925674, -0.007656948281982828, -0.005032391365515065, -0.036671478757478754, -0.0001552724395832915, -0.013394733194254322, -0.001060732120403767, -0.004696679054718319, -0.01671194105442134, 0.04481688961376651, 0.05116120080153512, -0.02908578341950624, -0.01404580148378147,
#     1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]),
#     np.array([1.0, 0.09116657866361795, 0.06105392772679827, 0.06890764660886624, 0.057048507099538966, 0.13271333102518273, 0.024614628315275326, 0.1751744723398696, 0.09732335722203271, 0.006824513381574132, 0.02893111348976897, 0.37763384779978354, 0.0011156302802603356, 0.19776554715846328, 0.0029646367662369004, 0.08158351493199212, 0.009258704970408036, 0.010133725659259326, 0.13811277020134483,
#     1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]),
#     np.array([1.0, 0.305734528307246, 1.4203756601032382, 0.046734644808477825, 0.10178494686448383, 0.4346726541288537, 0.017389403675049916, 2.0211945979045787, 0.0631978148513165, 0.006932390786546976, 0.03625708505876372, 2.8815479654747973, 0.0006599329202356609, 0.6191744329240592, 0.0024270014654882346, 0.1448532548399323, 0.009652851382562793, 0.006697829168361669, 0.0853274517178074,
#     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
#     0,
#     4
# )
# f = lambda pts: E_fit(model, vec3(pts))
# df = lambda pts: dE_fit(model, vec3(pts))

# print(f(pts))
# from scipy import optimize
# print(df(pts).flatten())
# print(optimize.approx_fprime(pts.flatten(), f, 1e-4))


# f = lambda pts: model.compute_at(pts)
# df = lambda pts: model.compute_grad_at(pts)


# print(df(pts))
# print(optimize.approx_fprime(pts.flatten(), f, 1e-4))

# grad_norm_squared = lambda pts: np.linalg.norm(model.compute_grad_at(pts), axis = 1)**2
# d_grad_norm_squared = lambda pts: df2(model, vec3(pts))

# print(grad_norm_squared(pts))
# print(d_grad_norm_squared(pts))
# print(optimize.approx_fprime(pts[1], grad_norm_squared, 1e-4))