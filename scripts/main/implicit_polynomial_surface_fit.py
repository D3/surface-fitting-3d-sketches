import numpy as np
from scipy.linalg import eig

from main.implicit_polynomial_surface import X, dX, ImplicitPolynomial, N_C_by_degree, cost_by_degree_weights


def M_block(gamma):
    M = X(gamma)
    return M

def get_normalizers(x, y):
    mu_x = np.mean(x, axis = 0)
    S = np.std(x - mu_x, axis = 0)
    mu_y = np.mean(y)

    S[S == 0] = 1 # Prevent division by zero

    # S = np.ones(S.shape)
    # mu_x = np.zeros(mu_x.shape)
    # mu_y = np.zeros(mu_y.shape)

    return mu_x, mu_y, S

def least_squares_fit_implicit_no_3l(data_pts, degree=3):
    # Build matrix M
    N_C = N_C_by_degree[degree]

    M = M_block(data_pts)[:, :N_C]

    lambdas, vs = eig(M.T @ M)

    # print(lambdas)
    # print(vs)

    C = vs[:, np.argsort([np.linalg.norm(l) for l in lambdas])[0]].real

    return ImplicitPolynomial(C, np.ones(N_C), np.zeros(N_C), 0, degree)

# def least_squares_fit_implicit(data_pts, proxy_mesh_normals, W = None, estimate_normals = True, c_p = 0.05, neighborhood_size = 10, lambda_reg=0, degree=3):
def least_squares_fit_implicit(
    data_pts,
    proxy_mesh_normals,
    c,
    W = None,
    lambda_reg=0,
    degree=3,
    normalize_cols=True):

    if W is None:
        W = np.ones(len(data_pts))


    local_normals = proxy_mesh_normals

    N_pts = len(data_pts)

    W_diag = np.diag(W)

    b0 = np.zeros(N_pts)
    bm = -c * np.ones(N_pts)
    bp = c * np.ones(N_pts)

    b_3l = np.hstack([W_diag @ bm, W_diag @ b0, W_diag @ bp])

    gamma_0 = data_pts
    gamma_c_m = data_pts + c * local_normals
    gamma_c_p = data_pts - c * local_normals

    # Orientation of + and - is arbitrary for now


    M_g0 = W_diag @ M_block(gamma_0)
    M_gp = W_diag @ M_block(gamma_c_p)
    M_gm = W_diag @ M_block(gamma_c_m)

    M_3l = np.row_stack([M_gm, M_g0, M_gp])

    # Depending on degree, remove some columns of the matrix
    M_3l = M_3l[:,:N_C_by_degree[degree]]

    # Normnalize data
    mu_x, mu_y, S_x = get_normalizers(M_3l, b_3l)

    if not normalize_cols:
        S_x = np.ones(N_C_by_degree[degree])

    # import polyscope as ps
    # ps.register_point_cloud("scaled pts", data_pts/S_x[[1,2,3]][None, :])

    # ps.show()

    M_3l = (M_3l - mu_x) / S_x

    b_3l -= mu_y
    
    # W_3l = np.hstack([W, W, W])[:, None]
    W_3l = np.diag(np.hstack([W, W, W]))


    if lambda_reg == 0:
        M_3l_pi = np.linalg.pinv(M_3l)

        C = M_3l_pi @ b_3l
    else:
        C = np.linalg.solve(M_3l.T @ M_3l + lambda_reg * np.eye(N_C_by_degree[degree]), M_3l.T @ b_3l)

    model = ImplicitPolynomial(C, S_x, mu_x, mu_y, degree)

    return model

def least_squares_fit_gradient_one(
    data_pts,
    normal_data_pts_positions,
    normal_data_pts_normals,
    w_normals=1,
    lambda_reg=0,
    degree=4,
    normalize_cols=True):


    N_pts = len(data_pts)
    N_C = N_C_by_degree[degree]
    # N_C = N_C_by_degree[4] # max degree

    B = np.zeros(N_C - 1) # remove first constant value (it is always zero)

    M = M_block(data_pts)

    # Depending on degree, remove some columns of the matrix
    M = M[:,:N_C]

    # Normalize data
    mu_x, mu_y, S_x = get_normalizers(M, B)

    if not normalize_cols:
        S_x = np.ones(N_C)

    pts_coords_var = np.std(data_pts - np.mean(data_pts, axis = 0), axis = 0)
    if np.any(pts_coords_var == 0):
        print("near zero dimension detected, that's just an axis aligned plane")
        C = np.zeros(N_C)
        # First zeroing coordinate of S_x indicates the plane orientation
        # + 1 because first coordinate of C is the constant term
        C[1 + np.flatnonzero(pts_coords_var == 0)[0]] = 1
        return ImplicitPolynomial(C, S_x, mu_x, mu_y, degree)

    # import polyscope as ps
    # ps.register_point_cloud("scaled pts", data_pts/S_x[[1,2,3]][None, :])

    # ps.show()

    M = (M - mu_x) / S_x
    M = M[:, 1:] # remove first constant value (it is always zero)

    B -= mu_y

    # A = M.T @ M + lambda_reg * cost_by_degree_weights[:N_C_by_degree[degree], :N_C_by_degree[degree]] 
    A = M.T @ M 

    N_normals = len(normal_data_pts_normals)
    if N_normals > 0:
        w_normals /= N_normals
        dX_V = dX(normal_data_pts_positions)[:, :N_C] / S_x
        dX_V = dX_V.reshape((N_normals, 3, N_C))
        dX_V = dX_V[:, :, 1:] # remove first constant value (it is always zero)
        N_matrix = normal_data_pts_normals.reshape((N_normals, 3))
        M_normals = np.einsum('ijk,ij->ik', dX_V, N_matrix)
        B_normals = np.sum(M_normals, axis=0)
        A += w_normals * M_normals.T @ M_normals
        B += w_normals * B_normals.flatten()
    # # else:
    # for n_v, p_v in zip(normal_data_pts_normals, normal_data_pts_positions):
    #     n_v = n_v.reshape((3, 1))
    #     dX_v = dX(p_v)[:, :N_C_by_degree[degree]]
    #     dX_v /= S_x
    #     A += w_normals * dX_v.T @ n_v @ n_v.T @ dX_v
    #     B += (w_normals * n_v.T @ dX_v).flatten()

    A += lambda_reg * np.eye(N_C - 1)

    C = np.linalg.solve(A, B)

    # Add zero first value
    C = np.insert(C, 0, 0)

    model = ImplicitPolynomial(C, S_x, mu_x, mu_y, degree)

    return model


def robust_fit_implicit(data_pts, proxy_normals, lambda_reg, k_proportion, N, estimate_normals = True, c_p = 0.02, neighborhood_size = 10, degree = 3):

    k = int(len(data_pts) * k_proportion)
    models = []
    median_residuals = []
    
    for j in range(N):
        indices = np.random.choice(len(data_pts), k, replace=False)

        subset = data_pts[indices]
        subset_normals = proxy_normals[indices]

        m = least_squares_fit_implicit(subset, subset_normals, estimate_normals, c_p, neighborhood_size, lambda_reg, degree)

        # Compute median residual over all points
        r = np.median(m.compute_residuals(data_pts))

        models.append(m)
        median_residuals.append(r)

    # Select best model
    best_idx = np.argmin(median_residuals)
    best_model = models[best_idx]
    print("Best model {} error = {}".format(best_idx, median_residuals[best_idx]))

    return best_model
