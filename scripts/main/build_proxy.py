import numpy as np
import time
import sys
from pygel3d import hmesh
import os
import polyscope as ps


from utils.paths import ROOT_FOLDER
from utils.loader import try_load_obj_data
from utils.pygel import get_faces
from utils.mirroring import remove_symmetric_data
from main.strokes_projection import build_proxy
from utils.math import remove_data_points

def build_from_files(sketch_file,
    proxy_file,
    is_symmetric,
    open_boundary,
    on_border_stroke_points,
    ignored_stroke_points,
    default_edge_weight, stroke_edge_weight, edge_length_factor_power, adaptive_graph_simplification,
    display):
    # Load files
    data_points, segments = try_load_obj_data(os.path.join(ROOT_FOLDER, sketch_file))
    proxy_mesh = hmesh.load(os.path.join(ROOT_FOLDER, proxy_file))

    proxy, data_points, segments = build(
        data_points, segments,
        proxy_mesh,
        is_symmetric,
        open_boundary,
        on_border_stroke_points,
        ignored_stroke_points,
        default_edge_weight, stroke_edge_weight, edge_length_factor_power, adaptive_graph_simplification,
        display
    )

    return proxy, data_points, segments


def build(
    # sketch_file,
    # proxy_file,
    sketch_pts, sketch_segments,
    proxy_mesh,
    is_symmetric,
    open_boundary,
    on_border_stroke_points,
    ignored_stroke_points,
    default_edge_weight, stroke_edge_weight, edge_length_factor_power, adaptive_graph_simplification,
    display):

    # Build proxy
    # data_points, segments = try_load_obj_data(os.path.join(ROOT_FOLDER, sketch_file))
    # proxy_mesh = hmesh.load(os.path.join(ROOT_FOLDER, proxy_file))

    segment_weights = np.ones(len(sketch_segments))

    point_is_on_border = np.zeros(len(sketch_pts), dtype=bool)
    if open_boundary and len(on_border_stroke_points) > 0 :
        point_is_on_border[on_border_stroke_points] = True

    # If there are some points ignored, remove them
    data_points, segments, mask_pts, mask_segments = remove_data_points(sketch_pts, sketch_segments, ignored_stroke_points)
    segment_weights = segment_weights[mask_segments]
    point_is_on_border = point_is_on_border[mask_pts]

    # If there is a mirror symmetry, remove any data points and segments that are on the other side of the mirror
    if is_symmetric:
        data_points, segments, segment_mask, data_points_mask = remove_symmetric_data(data_points, segments)
        point_is_on_border = point_is_on_border[data_points_mask]
    else:
        data_points_mask = None

    if display:
        ps_sketch = ps.register_curve_network("sketch", data_points, segments)
        if point_is_on_border is not None:
            on_border_color = (0.15, 0.44, 0.72)
            stroke_color = (0.1, 0.1, 0.1)
            colors = np.repeat(stroke_color, len(data_points)).reshape((-1, 3))
            colors[point_is_on_border] = on_border_color
            ps_sketch.add_color_quantity("border",colors, defined_on='nodes')

        ps.show()

    time0 = time.perf_counter()
    # Build proxy graph
    proxy = build_proxy(
            proxy_mesh,
            is_symmetric,
            data_points, segments, segment_weights,
            default_edge_weight, stroke_edge_weight,
            adaptive_proxy_mesh=adaptive_graph_simplification, exclude_path_ratio=1000, edge_length_pow=edge_length_factor_power,
            point_is_on_border=point_is_on_border)
    time1 = time.perf_counter()

    print("Time to build proxy =", time1 - time0)

    if display:
        ps_proxy_mesh = ps.register_surface_mesh("proxy mesh", proxy.V, proxy.F, enabled=False)
        ps_proxy_mesh.add_scalar_quantity("in/out", proxy.in_out_labelling)

        normals = proxy.mesh_normals

        ps_proxy_mesh.add_vector_quantity("normals", normals)

        # V_cropped, F_cropped = proxy.get_boundary_cropped_proxy()
        # ps_cropped_proxy = ps.register_surface_mesh("proxy mesh cropped", V_cropped, F_cropped, enabled=False)
        

        proxy.visualize()

        # ps.show()

    return proxy, data_points, segments