import polyscope as ps
import sys
import time
from pygel3d import hmesh
import math
from queue import PriorityQueue
import numpy as np

from utils.pygel import get_edges
from utils.graph import get_edge_key



def simplify_graph(proxy_mesh_in, vertices_with_data_set, crossings_by_edge, vertex_labelling_in_out, remove_unused_vertices=True):

    # Make a copy of the mesh
    proxy_mesh = hmesh.Manifold(proxy_mesh_in)

    Nv = len(proxy_mesh.vertices())

    # print(proxy_mesh.positions())

    print("[BUILDING PROXY] start graph simplification")
    
    # Compute distance to closest node for every vertex
    vertices_with_data = list(vertices_with_data_set)
    # distance_to_any_node = proxy.compute_geodesic_distances_multi(vertices_with_data)
    # max_dist = np.max(distance_to_any_node)

    # Get all start edges
    # edges, halfedges = get_edges(proxy.mesh)

    # Build priority queue for edges to collapse
    # - Priority = mean distance to any node for the edge endpoint vertices
    he_queue = PriorityQueue()
    he_timestamps = {}
    current_timestamp = 1

    new_vertex_indexing = np.arange(len(proxy_mesh.positions()))

    for vid in proxy_mesh.vertices():
        if vertex_labelling_in_out[vid]:
            proxy_mesh.remove_vertex(vid)

    if remove_unused_vertices:

        for hid in proxy_mesh.halfedges():
            if proxy_mesh.is_halfedge_at_boundary(hid):
                continue # Ignore boundary edges

            vA = proxy_mesh.incident_vertex(hid)
            vB = proxy_mesh.incident_vertex(proxy_mesh.opposite_halfedge(hid))

            if vA in vertices_with_data_set or vB in vertices_with_data_set:
                # Edge should not be considered for removal
                continue

            if proxy_mesh.is_vertex_at_boundary(vA) or proxy_mesh.is_vertex_at_boundary(vB):
                # print("vertex is at boundary")
                continue
            
            # Otherwise compute its priority cost and insert in queue (along with timestamp)
            # mean_dist_to_node = np.min(distance_to_any_node[[vA, vB]])
            # he_queue.put((max_dist - mean_dist_to_node, {"he": hid, "timestamp": current_timestamp}))
            # he_queue.put((max_dist - mean_dist_to_node, hid, current_timestamp))
            # he_queue.put((proxy_mesh.edge_length(hid), hid, current_timestamp))

            he_queue.put((proxy_mesh.valency(vA) + proxy_mesh.valency(vB), hid, current_timestamp))

            he_timestamps[hid] = current_timestamp
            

        while he_queue.qsize() > 0:
            # Get best candidate from queue
            to_collapse = he_queue.get()
            # print(to_collapse)
            timestamp_queue = to_collapse[2]
            hid = to_collapse[1]
            if timestamp_queue < he_timestamps[hid]:
                # Outdated element
                continue
            
            if not proxy_mesh.halfedge_in_use(hid):
                continue

            # Is the valency of one of the vertices too high?
            vA = proxy_mesh.incident_vertex(hid)
            vB = proxy_mesh.incident_vertex(proxy_mesh.opposite_halfedge(hid))

            if proxy_mesh.valency(vA) + proxy_mesh.valency(vB) > 12:
                continue

            # Try to collapse it
            success = proxy_mesh.collapse_edge(hid, avg_vertices=False)

            if success:
                # print("collapsed he {}".format(hid))
                vi = proxy_mesh.incident_vertex(hid)

                # Put in priority queue the new values for the neighboring edges
                current_timestamp += 1
                for h in proxy_mesh.circulate_vertex(vi, mode='h'):
                    if not proxy_mesh.halfedge_in_use(h):
                        continue
                    if proxy_mesh.is_halfedge_at_boundary(h):
                        continue # Ignore boundary edges
                    he_timestamps[h] = current_timestamp
                    vA = proxy_mesh.incident_vertex(h)
                    vB = proxy_mesh.incident_vertex(proxy_mesh.opposite_halfedge(h))
                    if vA in vertices_with_data_set or vB in vertices_with_data_set:
                        # Edge should not be considered for removal
                        continue
                    if proxy_mesh.is_vertex_at_boundary(vA) or proxy_mesh.is_vertex_at_boundary(vB):
                        continue
                    # mean_dist_to_node = np.min(distance_to_any_node[[vA, vB]])
                    # he_queue.put((max_dist - mean_dist_to_node, {"he": h, "timestamp": current_timestamp}))
                    # he_queue.put((max_dist - mean_dist_to_node, h, current_timestamp))
                    # he_queue.put((proxy_mesh.edge_length(h), h, current_timestamp))
                    he_queue.put((proxy_mesh.valency(vA) + proxy_mesh.valency(vB), h, current_timestamp))


    # Kept vertices?

    for vi in range(Nv):
        if not proxy_mesh.vertex_in_use(vi):
            # Vertex is removed
            new_vertex_indexing[vi] = -1
            new_vertex_indexing[vi+1:] -= 1

    kept_vertices_idx = np.flatnonzero(new_vertex_indexing >= 0)
    # kept_vertices = proxy_mesh.positions()[kept_vertices_idx]

    # proxy_mesh.cleanup()

    graph_edges, _ = get_edges(proxy_mesh)
    nodes_positions = np.copy(proxy_mesh.positions())

    # Modify edges indexing
    new_edges = np.empty((0,2), dtype=int)
    # new_edges_weights = []

    # new_crossing_segments_by_edge = {}
    new_crossings_by_edge = {}

    for e in graph_edges:
        # Keep edge?
        remapped_e = new_vertex_indexing[e]
        if np.any(remapped_e < 0):
            continue
        # print("edge : {} {}".format(remapped_e[0], remapped_e[1]))
        new_edges = np.row_stack([new_edges, remapped_e])

        # Compute new edge weight by looking at edge crossings of a vertex
        key = get_edge_key(e)
        remapped_key = get_edge_key(remapped_e)

        if key in crossings_by_edge:
            new_crossings_by_edge[remapped_key] = crossings_by_edge[key]

    print("[BUILDING PROXY] end graph simplification")

    proxy_mesh.cleanup()

    return proxy_mesh, new_vertex_indexing, kept_vertices_idx, new_edges, new_crossings_by_edge
    # return new_edges, kept_vertices, kept_vertices
