import numpy as np
import mcubes
import scipy.sparse

from utils.math import divide_by_square_norm, normalize, estimate_gradient


N_C_by_degree = {
    1: 4,
    2: 10,
    3: 20,
    4: 35
}

def cost_by_degree(d):
    return N_C_by_degree[d] / N_C_by_degree[4]

cost_by_degree_weights = np.diag(
    np.concatenate([
        np.repeat(cost_by_degree(1), 4),
        np.repeat(cost_by_degree(2), 10 - 4),
        np.repeat(cost_by_degree(3), 20 - 10),
        np.repeat(cost_by_degree(4), 35 - 20)]
    )
)

def X(pts):
    pts = pts.reshape((-1,3))
    # print(pts.T[0])
    x, y, z = pts.T
    # print(x.shape, y.shape, z.shape)
    res = np.array([np.ones(len(x)), x, y, z,
                x * x, x * y, x*z, y*y, y*z, z*z,
                x**3, y**3, z**3, x * y * y, x * z * z, y * x * x, y * z * z, z * x * x, z * y * y, x * y * z,
                x**4, y**4, z**4, x**3*y, x**3*z, y**3*x, y**3*z, z**3*x, z**3*y, x**2*y**2, x**2*y*z, x**2*z**2, y**2*z**2, y**2*x*z, z**2*x*y]).T

    return res

N_C = 35

def dX(pts):
    pts = pts.reshape((-1,3)) # In case pts is 1D with only one point
    x, y, z = pts.T
    
    dX_x = np.zeros((len(pts), N_C))
    dX_y = np.zeros((len(pts), N_C))
    dX_z = np.zeros((len(pts), N_C))
    
    
    dX_x[:, 1] = 1
    dX_y[:, 2] = 1
    dX_z[:, 3] = 1
    
    dX_x[:, 4] = 2 * x
    dX_x[:, 5] = y
    dX_y[:, 5] = x
    dX_x[:, 6] = z
    dX_z[:, 6] = x
    
    dX_y[:, 7] = 2 * y
    dX_y[:, 8] = z
    dX_z[:, 8] = y
    
    dX_z[:, 9] = 2 * z
    
    dX_x[:, 10] = 3 * x * x
    dX_y[:, 11] = 3 * y * y
    dX_z[:, 12] = 3 * z * z
    
    dX_x[:, 13] = y * y
    dX_y[:, 13] = 2 * x * y
    
    dX_x[:, 14] = z * z
    dX_z[:, 14] = 2 * x * z
    
    dX_y[:, 15] = x * x
    dX_x[:, 15] = 2 * x * y
    
    dX_y[:, 16] = z * z
    dX_z[:, 16] = 2 * y * z

    dX_z[:, 17] = x * x
    dX_x[:, 17] = 2 * z * x
    
    dX_z[:, 18] = y * y
    dX_y[:, 18] = 2 * y * z

    dX_x[:, 19] = y * z
    dX_y[:, 19] = x * z
    dX_z[:, 19] = x * y

    # Degree 4
    dX_x[:, 20] = 4 * x**3
    dX_y[:, 21] = 4 * y**3
    dX_z[:, 22] = 4 * z**3

    dX_x[:, 23] = 3 * x**2 * y
    dX_y[:, 23] = x**3

    dX_x[:, 24] = 3 * x**2 * z
    dX_z[:, 24] = x**3

    dX_x[:, 25] = y**3
    dX_y[:, 25] = 3 * y**2 * x

    dX_y[:, 26] = 3 * y**2 * z
    dX_z[:, 26] = y**3

    dX_x[:, 27] = z**3
    dX_z[:, 27] = 3 * z**2 * x

    dX_y[:, 28] = z**3
    dX_z[:, 28] = 3 * z**2 * y

    dX_x[:, 29] = 2 * x * y**2
    dX_y[:, 29] = 2 * y * x**2

    dX_x[:, 30] = 2 * x * y * z
    dX_y[:, 30] = x**2 * z
    dX_z[:, 30] = x**2 * y

    dX_x[:, 31] = 2 * x * z**2
    dX_z[:, 31] = 2 * z * x**2

    dX_y[:, 32] = 2 * y * z**2
    dX_z[:, 32] = 2 * z * y**2

    dX_x[:, 33] = y**2 * z
    dX_y[:, 33] = 2 * x * y * z
    dX_z[:, 33] = y**2 * x

    dX_x[:, 34] = z**2 * y
    dX_y[:, 34] = z**2 * x
    dX_z[:, 34] = 2 * x * y * z
    
    dX_all = np.zeros((3 * len(pts), N_C))
    
    dX_all[::3] = dX_x
    dX_all[1::3] = dX_y
    dX_all[2::3] = dX_z
    
    
    return dX_all


def d2X(pts):
    pts = pts.reshape((-1,3)) # In case pts is 1D with only one point
    x, y, z = pts.T
    
    d2X_x = np.zeros((len(pts), 3, N_C))
    d2X_y = np.zeros((len(pts), 3, N_C))
    d2X_z = np.zeros((len(pts), 3, N_C))

    d2X_x[:, 0, 4] = 2
    d2X_x[:, 0, 10] = 6 * x
    d2X_x[:, 0, 15] = 2 * y
    d2X_x[:, 0, 17] = 2 * z
    d2X_x[:, 1, 5] = 1
    d2X_x[:, 1, 13] = 2 * y
    d2X_x[:, 1, 15] = 2 * x
    d2X_x[:, 1, 19] = z
    d2X_x[:, 2, 6] = 1
    d2X_x[:, 2, 14] = 2 * z
    d2X_x[:, 2, 17] = 2 * x
    d2X_x[:, 2, 19] = y
    # Degree 4
    d2X_x[:, 0, 20] = 12 * x**2
    d2X_x[:, 0, 23] = 6 * x * y
    d2X_x[:, 0, 24] = 6 * x * z
    d2X_x[:, 0, 29] = 2 * y**2
    d2X_x[:, 0, 30] = 2 * y * z
    d2X_x[:, 0, 31] = 2 * z**2
    d2X_x[:, 1, 23] = 3 * x**2
    d2X_x[:, 1, 25] = 3 * y**2
    d2X_x[:, 1, 29] = 4 * x * y
    d2X_x[:, 1, 30] = 2 * x * z
    d2X_x[:, 1, 33] = 2 * y * z
    d2X_x[:, 1, 34] = z**2
    d2X_x[:, 2, 24] = 3 * x**2
    d2X_x[:, 2, 27] = 3 * z**2
    d2X_x[:, 2, 30] = 2 * x * y
    d2X_x[:, 2, 31] = 4 * x * z
    d2X_x[:, 2, 33] = y**2
    d2X_x[:, 2, 34] = 2 * y * z

    # By commutativity
    d2X_y[:, 0] = d2X_x[:, 1]

    d2X_y[:, 1, 7] = 2
    d2X_y[:, 1, 11] = 6 * y
    d2X_y[:, 1, 13] = 2 * x
    d2X_y[:, 1, 18] = 2 * z
    d2X_y[:, 2, 8] = 1
    d2X_y[:, 2, 16] = 2 * z
    d2X_y[:, 2, 18] = 2 * y
    d2X_y[:, 2, 19] = x
    # Degree 4
    d2X_y[:, 1, 21] = 12 * y**2
    d2X_y[:, 1, 25] = 6 * x * y
    d2X_y[:, 1, 26] = 6 * y * z
    d2X_y[:, 1, 29] = 2 * x**2
    d2X_y[:, 1, 32] = 2 * z**2
    d2X_y[:, 1, 33] = 2 * x * z
    d2X_y[:, 2, 26] = 3 * y**2
    d2X_y[:, 2, 28] = 3 * z**2
    d2X_y[:, 2, 30] = x**2
    d2X_y[:, 2, 32] = 4 * y * z
    d2X_y[:, 2, 33] = 2 * x * y
    d2X_y[:, 2, 34] = 2 * x * z

    # By commutativity
    d2X_z[:, 0] = d2X_x[:, 2]
    d2X_x[:, 1] = d2X_y[:, 2]
    
    d2X_z[:, 2, 9] = 2
    d2X_z[:, 2, 12] = 6 * z
    d2X_z[:, 2, 14] = 2 * x
    d2X_z[:, 2, 16] = 2 * y
    # Degree 4
    d2X_z[:, 2, 22] = 12 * z**2
    d2X_z[:, 2, 27] = 6 * x * z
    d2X_z[:, 2, 28] = 6 * y * z
    d2X_z[:, 2, 31] = 2 * x**2
    d2X_z[:, 2, 32] = 2 * y**2
    d2X_z[:, 2, 34] = 2 * x * y
        
    return d2X_x, d2X_y, d2X_z


class ImplicitPolynomial:

    def __init__(self, C, S, mu_x, mu_y, degree):
        self.C = C
        self.S = S
        self.mu_x = mu_x
        self.mu_y = mu_y
        self.degree = degree
        self.N_c = len(C) # This will depend on the degree of the polynomial

        self.penalty = 0

    def compute_at(self, pts):
        X_p = ((X(pts)[:, : self.N_c] - self.mu_x) / self.S)
        return self.mu_y + X_p @ self.C

    def compute_grad_at(self, pts, normalize=False):
        dX_scaled = (dX(pts)[:, : self.N_c]) / self.S
        # dX_scaled = dX(pts)
        if normalize:
            G = (dX_scaled @ self.C).reshape((-1, 3))
            norms = np.linalg.norm(G, axis = 1)
            norms[norms < 1e-8] = 1
            G = G / norms[:, None]
            return G
        else:
            return (dX_scaled @ self.C).reshape((-1, 3))

    def compute_grad2_at(self, pts):
        d2X_x, d2X_y, d2X_z = d2X(pts)

        # Keeping only the relevant part according to degree
        d2X_x = d2X_x[:, :, : self.N_c]
        d2X_y = d2X_y[:, :, : self.N_c]
        d2X_z = d2X_z[:, :, : self.N_c]

        # scaling
        d2X_x /= self.S
        d2X_y /= self.S
        d2X_z /= self.S

        d2f_x = d2X_x @ self.C # (N, 3)
        d2f_y = d2X_y @ self.C # (N, 3)
        d2f_z = d2X_z @ self.C # (N, 3)


        row_indices_x = np.repeat(np.arange(len(pts)) * 3, 3)
        row_indices_y = row_indices_x + 1
        row_indices_z = row_indices_x + 2

        col_indices = np.arange(len(pts) * 3)

        all_rows = np.hstack([row_indices_x, row_indices_y, row_indices_z])
        all_cols = np.hstack([col_indices, col_indices, col_indices])

        all_data = np.hstack([d2f_x.flatten(), d2f_y.flatten(), d2f_z.flatten()])

        d2f = scipy.sparse.csr_matrix((all_data, (all_rows, all_cols)), shape=(3 * len(pts), 3 * len(pts)))

        return d2f

    def compute_directional_hessian_at(self, pts, grad_at_pts=None):
        # compute H(f)(pts).T @ grad(f)(pts) without building H
        # If the gradient at the points has already been computed, it can be passed to avoid recomputation

        d2X_x, d2X_y, d2X_z = d2X(pts)

        # Keeping only the relevant part according to degree
        d2X_x = d2X_x[:, :, : self.N_c]
        d2X_y = d2X_y[:, :, : self.N_c]
        d2X_z = d2X_z[:, :, : self.N_c]

        # scaling
        d2X_x /= self.S
        d2X_y /= self.S
        d2X_z /= self.S

        d2f_x = d2X_x @ self.C # (N, 3)
        d2f_y = d2X_y @ self.C # (N, 3)
        d2f_z = d2X_z @ self.C # (N, 3)

        if grad_at_pts is None:
            df = self.compute_grad_at(pts) # (N, 3)
        else:
            df = grad_at_pts

        d2f_x_times_grad = np.sum(np.multiply(d2f_x, df), axis=1)
        d2f_y_times_grad = np.sum(np.multiply(d2f_y, df), axis=1)
        d2f_z_times_grad = np.sum(np.multiply(d2f_z, df), axis=1)

        H_times_grad = np.zeros(len(pts) * 3)

        spliced_indices = 3 * np.arange(len(pts))

        H_times_grad[spliced_indices] = d2f_x_times_grad
        H_times_grad[spliced_indices + 1] = d2f_y_times_grad
        H_times_grad[spliced_indices + 2] = d2f_z_times_grad

        return H_times_grad


    def normal_at(self, pts):
        grad = self.compute_grad_at(pts)
        return normalize(grad)

    def polygonize(self, center, span, res=15):
        f = lambda x, y, z : self.compute_at(np.array([x, y, z]))
        min_bound = center - 0.5 * span
        max_bound = center + 0.5 * span
        verts, faces = mcubes.marching_cubes_func(tuple(min_bound), tuple(max_bound), res, res, res, f, 0)

        return verts, faces

    def closest_points(self, points):
        projection, disp = project(
            points, 
            lambda pts: self.compute_at(pts),
            eps=1e-5,
            gradient_func= lambda pts: self.compute_grad_at(pts))

        return projection

    def compute_residuals(self, pts, approx='sdf'):

        if approx == 'sdf':
            distances = np.abs(self.compute_at(pts))
        elif approx == 'first_order':
            # Estimate distance with first order approximation
            gradient_norms = np.linalg.norm(self.compute_grad_at(pts), axis = 1)
            # Prevent division by zero
            gradient_norms = np.maximum(gradient_norms, np.ones(gradient_norms.shape) * 1e-6)
            distances = np.divide(np.abs(self.compute_at(pts)), gradient_norms)
        else:
            # projections, displacements = project(pts, lambda pts: self.compute_at(pts), eps=0.0001, gradient_func= lambda pts: self.compute_grad_at(pts))
            projs = project(pts, self, eps=1e-6)
            distances = np.linalg.norm(pts - projs, axis = 1)
            # Exclude points that have a near zero gradient => set their residual to the max of the other distances
            # grads = self.compute_grad_at(pts)
            # ignore_pts = np.linalg.norm(grads, axis = 1) < 1e-4
            # pts_to_ignore = np.flatnonzero(ignore_pts)
            # distances[pts_to_ignore] = np.max(distances[np.logical_not(ignore_pts)])


        return distances

    def project_pts(self, pts, step_size):
        fs = self.compute_at(pts)
        grads = self.compute_grad_at(pts)
        dirs = - divide_by_square_norm(grads) * fs[:, None]
        return pts+ step_size * dirs

    def set_penalty(self, penalty):
        self.penalty = penalty

    @property
    def cost(self):
        # return np.dot(self.C, self.C)
        return cost_by_degree(self.degree) + self.penalty
        # return cost_by_degree(self.degree) + np.dot(self.C, self.C) + self.penalty
        # return self.penalty + np.sum(np.abs(self.C))
        # return self.penalty + np.dot(self.C, self.C)
        # weighted_C = cost_by_degree_weights[:N_C_by_degree[self.degree], :N_C_by_degree[self.degree]] @ self.C
        # return np.dot(weighted_C, weighted_C) + self.penalty

    def to_dict(self):
        return {'C': self.C.tolist(), 'S': self.S.tolist(), 'mu_x': self.mu_x.tolist(), 'mu_y': self.mu_y, 'degree': self.degree, 'penalty': self.penalty}

    def __eq__(self, other): 
        if not isinstance(other, ImplicitPolynomial):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return (np.array_equal(other.C, self.C)
        and np.array_equal(other.S, self.S)
        and np.array_equal(other.mu_x, self.mu_x)
        and np.array_equal(other.mu_y, self.mu_y)
        and other.degree == self.degree
        and other.penalty == self.penalty)


# def project(points, distance_field_func, eps, gradient_func=None):
#     # Project all data points on the iso surface

#     distances = distance_field_func(points)
#     max_dist = np.max(distances)

#     current_pts = points
#     total_displacement = np.zeros(points.shape)
#     print("Start projection")
#     it = 0

#     while max_dist > eps:
#         # print("taking a step towards the surface")
#         # Estimate all gradients
#         if gradient_func is None:
#             gradients = estimate_gradient(lambda x: distance_field_func(x), current_pts)
#         else:
#             gradients = gradient_func(current_pts)
#         current_distances = distance_field_func(current_pts)
#         max_dist = np.max(current_distances)
#         dirs = -divide_by_square_norm(gradients) * current_distances[:,None]
#         total_displacement += dirs
#         current_pts = current_pts + dirs
#         it += 1

#     proj = points + total_displacement
#     print("End projection")
#     print("Iterations = {}".format(it))

#     return proj, total_displacement

def project(points, model, eps):
    current_pts = points.copy()

    max_dist = np.max(model.compute_residuals(current_pts, approx='first_order'))

    while max_dist > eps:
        fs = model.compute_at(current_pts)
        grads = model.compute_grad_at(current_pts)
        dirs = - divide_by_square_norm(grads) * fs[:, None]
        current_pts = current_pts + dirs
        max_dist = np.max(np.linalg.norm(dirs, axis=1))

    return current_pts


def models_from_data(models_data):
    models = []
    for m in models_data:
        models.append(ImplicitPolynomial(
            np.array(m['C']),
            np.array(m['S']),
            np.array(m['mu_x']),
            m['mu_y'],
            m['degree']
        ))

    return models