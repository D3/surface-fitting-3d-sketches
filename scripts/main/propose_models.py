import numpy as np
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
import polyscope as ps
import networkx as nx
import sys

from sklearn.cluster import SpectralClustering
from scipy.sparse.csgraph import connected_components

from main.implicit_polynomial_surface_fit import least_squares_fit_implicit, least_squares_fit_implicit_no_3l, least_squares_fit_gradient_one

from utils.math import get_bounds
from utils.labelling import get_subsets_from_labels, get_subset_id_from_label
from utils.Plane import least_squares_fit_plane

MIN_POINTS_COUNT_FOR_FIT = 10

def fit_model_to_nodes(
    proxy,
    nodes_idx,
    lambda_reg, w_normals_fit, degree,
    estimate_normals=False,
    min_nb_pts=MIN_POINTS_COUNT_FOR_FIT):

    pts_subset, _ = proxy.get_data_for_nodes(nodes_idx)
    # print(f"{len(pts_subset)} pts")
    if len(pts_subset) < min_nb_pts:
        print(f"WARNING: {len(pts_subset)} pts is not enough to fit a model, giving up.")
        return None

    # if degree == 1:
    #     estimate_normals = True
    
    if estimate_normals:
        normal = least_squares_fit_plane(pts_subset).normal()
        normal_cstr_normals = np.tile(normal, len(pts_subset)).reshape((len(pts_subset), 3))
        normal_cstr_pts = pts_subset.copy()
    else:
        normal_cstr_pts, normal_cstr_normals = proxy.get_proxy_data_for_nodes(nodes_idx)

    # if degree == 1 :
    #     model = least_squares_fit_implicit(
    #         pts_subset,
    #         normal_cstr_normals,
    #         0.03,
    #         lambda_reg=lambda_reg,
    #         degree=1
    #     )
    # else:
    model = least_squares_fit_gradient_one(
                    pts_subset,
                    normal_cstr_pts,
                    normal_cstr_normals,
                    w_normals=w_normals_fit,
                    lambda_reg=lambda_reg, degree=degree)
    return model


def refit_models(
    proxy,
    subsets,
    previous_models,
    w_normals_fit, lambda_reg = 1, estimate_normals=False):

    models = []
    for l in range(len(subsets)):
        subset = subsets[l]

        model = fit_model_to_nodes(
            proxy,
            subset,
            lambda_reg, w_normals_fit, previous_models[l].degree,
            estimate_normals=estimate_normals)

        if model is None:
            print("subset {} doesn't have enough data points, we don't refit the model.".format(l))
            # Keep the same model as before, so as not to increase energy, but don't refit
            models.append(previous_models[l])
        else:
            models.append(model)

    # print("models count =", len(models))
    return models

def propose_models_lower_degree(
    proxy,
    subsets,
    # assigned_labels,
    previous_models,
    w_normals_fit, lambda_reg=1):

    new_models = []
    original_labels = []

    for l in range(len(previous_models)):
        # Is this label assigned in labels?
        # subset_idx_l = get_subset_id_from_label(assigned_labels, l)
        # if subset_idx_l == -1:
        #     # Label l is not assigned => do not propose lower degree models
        #     continue
        subset = subsets[l]

        d = previous_models[l].degree - 1

        while d > 0:

            model = fit_model_to_nodes(
                proxy,
                subset,
                lambda_reg,
                w_normals_fit,
                degree=d
            )

            if model is None:
                print("Subset is too small, continue with others.")
                break

            new_models.append(model)
            original_labels.append(l)

            d -= 1
    
    return new_models, original_labels


# def propose_new_models(proxy, subsets, assigned_labels, previous_models, center, span, translation, lambda_reg = 1):
def propose_new_models_by_split(proxy, subset, lambda_reg, w_normals_fit, degree):
    new_models = []
    new_subsets = []

    # Are there multiple connected components to the subgraph?
    # sub_affinity_matrix = proxy.affinity_matrix[np.array(subset)[:, None], np.array(subset)]
    # n_components, labels = connected_components(sub_affinity_matrix, directed=False)
    n_components, labels = proxy.connected_components(subset)

    if n_components == 1 :
    #     print("there is 1 component for a region")
        return new_models

        # Fit a model to each subset
        # subset_partition_labels = find_partition(proxy, subset, N = 2)
        # sub_subsets = subset_partition_labels
    # subset_partition_labels.extend(find_partition(proxy, subset, N= 3))

    sub_subsets, _ = get_subsets_from_labels(labels)

    for sub_subset in sub_subsets:
    # for i in range(n_components):
        nodes_idx = np.array(subset)[sub_subset]
        # nodes_idx = np.array(subset)[labels == i]
        new_subsets.append(nodes_idx)

        model = fit_model_to_nodes(
            proxy,
            nodes_idx,
            lambda_reg,
            w_normals_fit,
            degree
        )

        if model is not None:

        # pts_to_fit, _ = proxy.get_data_for_nodes(nodes_idx)

        # if len(pts_to_fit) > 10:

        #     normal_cstr_points, normal_cstr_normals = proxy.get_proxy_data_for_nodes(nodes_idx)
        #     # print("Subset has {} points".format(len(pts_to_fit)))

        #     # model = least_squares_fit_implicit(pts_to_fit, normals_to_fit, c=c, lambda_reg=lambda_reg, degree=previous_model.degree)
        #     # print("New proposal mean residual = {}".format(np.mean(model.compute_residuals(pts_to_fit))))

        #     model = least_squares_fit_gradient_one(
        #             pts_to_fit,
        #             normal_cstr_points,
        #             normal_cstr_normals,
        #             w_normals=w_normals_fit,
                    # lambda_reg=lambda_reg, degree=degree)

            new_models.append(model)

    return new_models

def propose_new_models_by_merge(proxy, labelling, n_merges, lambda_reg, w_normals_fit, models, max_degree):
    new_proposals = []
    merged_labels = []

    # I need reindexed labels, where we assume the labels present go from 0 -> n_labels
    # for my connected component aware labelling
    assigned_labels, reindexed_labels = np.unique(labelling, return_inverse=True)

    # Separate labelling by connected components
    labelling_components = reindexed_labels.copy()
    unique_labels = np.unique(reindexed_labels)

    current_label = len(unique_labels)
    corresponding_labels = unique_labels.copy()

    for l in unique_labels:
        subset_l = np.flatnonzero(labelling == l)
        n_components, labels_components = proxy.connected_components(subset_l)

        if n_components > 1:
            for c in range(1, n_components):
                labelling_components[subset_l[labels_components == c]] = current_label
                current_label += 1
                corresponding_labels = np.append(corresponding_labels, l)

    # Edges cut
    edge_labels = np.sort(labelling_components[proxy.edges], axis = 1) # put them in asc order
    edges_with_disparate_labels = np.flatnonzero(edge_labels[:, 0] != edge_labels[:, 1])
    cut_edge_weights = proxy.edges_weight[edges_with_disparate_labels]

    # Labels of cut
    unique_cut_labels, inverse = np.unique(edge_labels[edges_with_disparate_labels], return_inverse=True, axis=0)

    cost_by_label_pairs = []

    for unique_cut_l in range(len(unique_cut_labels)):
        cost_by_label_pairs.append(np.sum(cut_edge_weights[inverse == unique_cut_l]))

    # Sort by cost cut
    label_pairs_idx_sorted = np.argsort(cost_by_label_pairs)[::-1]
    label_pairs_sorted_by_cost = unique_cut_labels[label_pairs_idx_sorted]

    merge_tentatives = 0

    for label_pair_idx in label_pairs_idx_sorted:
        l_A_c, l_B_c = unique_cut_labels[label_pair_idx]
        # l_A and l_B are the original labels (before reindexing)
        l_A, l_B = assigned_labels[corresponding_labels[[l_A_c, l_B_c]]]

        print(f"trying merge {l_A} - {l_B}, cost = {cost_by_label_pairs[label_pair_idx]}")

        merged_subset = np.flatnonzero(np.logical_or(labelling_components == l_A_c, labelling_components == l_B_c))

        # shared_edges_AB = proxy.edges[np.all(edge_labels == [l_A, l_B], axis = 1)]
        # augmented_subset_A = np.unique(np.append(np.flatnonzero(labelling == l_A), shared_edges_AB.flatten()))
        # augmented_subset_B = np.unique(np.append(np.flatnonzero(labelling == l_B), shared_edges_AB.flatten()))

        # for subset in [merged_subset, augmented_subset_A, augmented_subset_B]:
        for subset in [merged_subset]:
            # Fit model
            # normal_cstr_pts, normal_cstr_normals = proxy.get_proxy_data_for_nodes(subset)

            # if len(pts_subset) > 10:
            degree = min(max_degree, max([2, models[l_A].degree, models[l_B].degree]))

            model = fit_model_to_nodes(
                proxy,
                subset,
                lambda_reg,
                w_normals_fit,
                degree
            )

            if model is not None:
                # pts_subset, _ = proxy.get_data_for_nodes(subset)

                # center, span = get_bounds(pts_subset)

                # V, F = model.polygonize(center, span)

                # ps_mesh = ps.register_surface_mesh("merged model", V, F)

                # ps_pts = ps.register_point_cloud("merged data", pts_subset)
                # ps.register_point_cloud("merged nodes", proxy.nodes_positions[subset])

                # ps.show()

                new_proposals.append(model)

                merged_labels.append([l_A, l_B])

        merge_tentatives += 1
        if merge_tentatives >= n_merges:
            break

    return new_proposals, merged_labels



def propose_new_model_at(proxy, node_idx, c, lambda_reg, subset_size_ratio):
    distances_to_seed = proxy.compute_geodesic_distances(proxy.node_to_vertex_idx(node_idx))

    ordered_vertex_ids = np.argsort(distances_to_seed)
    N = int(len(proxy.data_points) * subset_size_ratio)
    nodes_to_use_for_fit = ordered_vertex_ids[:N]    

    data_points_idx = []

    # Sample data points from closest nodes, until we have N
    for vertex_id in ordered_vertex_ids:
        if len(data_points_idx) >= N:
            break
        data_points_idx.extend(proxy.get_indices_for_vertex(vertex_id))

    # Fit a model to those points
    pts_subset = proxy.data_points[data_points_idx]
    normals_subset = proxy.normals[data_points_idx]

    if len(pts_subset) == 0:
        print("empty subset")
        return None

    model = least_squares_fit_implicit(pts_subset, normals_subset, c=c, lambda_reg=lambda_reg, degree=3)

    return model

def propose_models_for_subsets(
    proxy,
    data_pt_idx_subsets,
    w_normals_fit, lambda_reg, degree=4):
    models = []

    for subset in data_pt_idx_subsets:
        pts_subset = proxy.data_points[subset]
        normals_subset = proxy.normals[subset]

        # print("nb points = {}".format(len(subset)))

        # TODO: minimal nb of points to fit a model?
        if len(pts_subset) < MIN_POINTS_COUNT_FOR_FIT:
            print(f"subset is too small with {len(subset)} pts")
            continue

        # model = least_squares_fit_implicit(pts_subset, normals_subset, c=c, lambda_reg=lambda_reg, degree=degree)

        model = least_squares_fit_gradient_one(
            pts_subset,
            pts_subset,
            normals_subset,
            w_normals=w_normals_fit,
            lambda_reg=lambda_reg, degree=degree)

        models.append(model)

    return models

def sample_subsets_randomly(proxy, L0, sample_ratio_bounds, random_seed=None):

    # Seed random
    if random_seed is None:
        # random_seed = RandomState(MT19937(SeedSequence(123456789)))
        random_seed = 123456789

    rs = RandomState(random_seed)

    subsets = []
    N_min = sample_ratio_bounds[0] * len(proxy.data_points)
    N_max = sample_ratio_bounds[1] * len(proxy.data_points)
    # Sample randomly points L0 times
    for i in range(L0):
        N = rs.randint(N_min, N_max)
        subset = rs.choice(len(proxy.data_points), size=N, replace=False)
        subsets.append(subset)

    return subsets

def sample_subsets_by_geodesic_distance(proxy, L0, sample_ratio_bounds, random_seed=None):
    # Seed random
    if random_seed is None:
        # random_seed = RandomState(MT19937(SeedSequence(123456789)))
        random_seed = 123456789

    rs = RandomState(random_seed)

    subsets = []
    N_min = sample_ratio_bounds[0] * len(proxy.data_points)
    N_max = sample_ratio_bounds[1] * len(proxy.data_points)
    initial_seeds_id = rs.choice(len(proxy.V), size=L0, replace=False)
    Ns = rs.randint(N_min, N_max, size=L0)
    # For each seed, compute geodesic distance to all other vertices
    for seed_id, N in zip(initial_seeds_id, Ns):
        distances_to_seeds = proxy.compute_geodesic_distances(seed_id)
        ordered_vertex_ids = np.argsort(distances_to_seeds)

        data_points_idx = []

        # used_proxy_vertices_idx = []

        # Sample data points from closest nodes, until we have N
        for vertex_id in ordered_vertex_ids:
            if len(data_points_idx) >= N:
                break
            data_points_idx.extend(proxy.get_indices_for_vertex(vertex_id))
        #     used_proxy_vertices_idx.append(vertex_id)

        # ps.register_point_cloud("seed vertex", proxy.V[[seed_id]])
        # ps.register_point_cloud("used proxy vertices", proxy.V[used_proxy_vertices_idx])
        # ps.register_point_cloud("data points", proxy.data_points[data_points_idx])

        # ps.show()

        subsets.append(data_points_idx)

    return subsets

def sample_subsets_by_clustering(proxy, L0, random_seed):
    subsets = []

    # Perform a spectral clustering on the proxy graph to cluster the nodes
    sc = SpectralClustering(L0, affinity='precomputed', n_init=1, assign_labels='discretize', random_state=random_seed)
    node_labels = sc.fit_predict(proxy.affinity_matrix_sparse)

    # Visualize
    # ps_nodes = ps.register_point_cloud("nodes", proxy.nodes_positions)
    # ps_nodes.add_scalar_quantity("clustering label", node_labels)
    # ps.show()

    subset_nodes, _ = get_subsets_from_labels(node_labels)

    # For each node cluster, create a data point subset
    for n_subset in subset_nodes:
        p_subset = proxy.get_indices_for_nodes(n_subset)
        subsets.append(p_subset)

    return subsets


def find_partition(proxy, indices, N=2, method='spectral_clustering'):
    # Separate the indices of the nodes of the graph described by the affinity matrix
    # into 2 sets, using the given method

    # if method == 'spectral_clustering':
    # Spectral clustering (N=2)
    affinity_matrix = proxy.affinity_matrix_sparse[np.array(indices)[:, None], np.array(indices)]**2
    sc = SpectralClustering(N, affinity='precomputed', n_init=1, assign_labels='discretize', random_state=123)
    subset_labels = sc.fit_predict(affinity_matrix)

    # return subset_labels
    return subset_labels

    # elif method == 'cut':
    #     # Minimum cut
    #     # First choose 2 nodes to be source/sink
    #     errors = proxy.compute_fidelity_energy_by_node(current_model, indices=indices)

    #     # Choose the node of worse error
    #     s = np.argmax(errors)

    #     vertex_s = proxy.node_to_vertex_idx(s)

    #     vertex_indices = [proxy.node_to_vertex_idx(idx) for idx in indices]

    #     # Then choose the node that is furthest from node s on the proxy
    #     distances_to_s = proxy.compute_geodesic_distances(vertex_s)[vertex_indices]
    #     t = np.argmax(distances_to_s)

    #     # Find minimum cut to separate those 2
    #     subsetA, subsetB = proxy.segment_by_min_cut(s, t, indices=vertex_indices)

    #     return [subsetA, subsetB]

