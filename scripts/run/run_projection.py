
import polyscope as ps
import sys
import time
from pygel3d import hmesh
import os
import json
import argparse
import math
import csv
import re
import numpy as np
from datetime import datetime
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))

from main.implicit_polynomial_surface import models_from_data
from main.mesh_optimization import attract_seams_to_strokes, compute_smoothing_weights, optimize_mesh
from main.mesh_refinement import insert_boundary_vertices, remove_islands_from_labelling

from utils.paths import *
from utils.loader import try_load_data, try_load_obj_data
from utils.pygel import get_faces
from utils.math import bounding_box_diagonal, remove_mesh_vertices, get_bounds, remove_data_points
from utils.labelling import get_subsets_from_labels
from utils.mirroring import remove_symmetric_data, symmetrize_mesh, cut_mesh, is_on_mirror
from utils.mesh_export import export_to_ply
from utils.mesh_properties_io import pack_mesh_properties_to_rgb


def run_projection_from_log(
    sketch_info,
    log_file_path,
    attract_seams_to_strokes=False,
    output_suffix=None,
    display=True,
    display_rel_offset=np.array([1,1,1]),
    export_folder=None,
    max_iterations=300,
    gradient_approx=True,
    print_it=False):

    ps_vis = display

    sketch_file_path = os.path.join(ROOT_FOLDER, sketch_info['sketch_file'])
    mirror_symmetry = bool(int(sketch_info['is_symmetric']))
    proxy_file_path = os.path.join(ROOT_FOLDER, sketch_info['proxy_file'])
    # display_rel_offset = np.array(json.loads(sketch_info['display_rel_offset']), dtype=float)

    # Load data
    data_points, segments = try_load_obj_data(sketch_file_path)
    ignored_pts_idx = sketch_info['ignored_stroke_points']
    _, _, pts_mask, _ = remove_data_points(data_points, segments, ignored_pts_idx)
    proxy_mesh_initial = hmesh.load(proxy_file_path)

    center, span = get_bounds(data_points)

    if ps_vis:
        # Position camera
        camera_position = center + np.multiply(span, display_rel_offset)
        ps.look_at(camera_position, center)

    # Read log file
    log_data = try_load_data(log_file_path)

    # in_out_labelling = np.array(log_data['in_out_labelling'], dtype=int).astype(bool)
    in_out_labelling = np.zeros(len(proxy_mesh_initial.positions()), dtype=bool)
    if len(log_data['vertices_outside']) > 0:
        in_out_labelling[log_data['vertices_outside']] = True

    stroke_position_by_edge = log_data['stroke_position_by_edge']

    states = log_data["states"]

    # ps_proxy_mesh = ps.register_surface_mesh("proxy mesh", proxy_mesh_initial.positions(), get_faces(proxy_mesh_initial), enabled=True)
    # ps_proxy_mesh.add_scalar_quantity("label", np.array(states[-1]['labels'], dtype=int))
    # ps.show()

    final_step = states[-1]
    labels = np.array(final_step['labels'], dtype=int)
    initial_labels = np.copy(labels)
    # assigned_labels, reindexed_labels = np.unique(labels, return_inverse=True)
    labels = remove_islands_from_labelling(proxy_mesh_initial, labels)
    assigned_labels, reindexed_labels = np.unique(labels, return_inverse=True)
    models = np.array(models_from_data(final_step['models']))[assigned_labels]

    if ps_vis:
        ps_sketch = ps.register_curve_network("sketch", data_points, segments, color=(0, 0, 0), radius=0.002)
        ps_sketch.add_scalar_quantity("ignored?", pts_mask)
        ps_proxy_mesh = ps.register_surface_mesh("proxy mesh", proxy_mesh_initial.positions(), get_faces(proxy_mesh_initial), enabled=False)
        ps_proxy_mesh.add_scalar_quantity("in/out", in_out_labelling)
        ps_proxy_mesh.add_scalar_quantity("label", reindexed_labels)
        ps_proxy_mesh.add_scalar_quantity("initial label", initial_labels)

    if display:
        ps.show()

    V_opt, F, all_labels, is_seam, is_sharp, smoothing_weights, projection_log = run_projection(
        proxy_mesh_initial,
        reindexed_labels,
        models,
        stroke_position_by_edge,
        in_out_labelling,
        mirror_symmetry,
        attract_seams_to_strokes,
        max_iterations,
        gradient_approx,
        print_it
    )

    if ps_vis:
        ps_proj = ps.register_surface_mesh(f"projection", V_opt, F, material='wax')
        ps_proj.add_scalar_quantity("label", all_labels, enabled=True, cmap='rainbow')
        ps_proj.add_scalar_quantity("smooth weight", smoothing_weights)


    if display:
        ps.show()

    # file_base_name = os.path.basename(log_file_path).split(".")[0]
    file_base_name = os.path.splitext(os.path.basename(log_file_path))[0]

    if export_folder is not None:
        # filename = os.path.join(export_folder, "{}.ply".format(file_base_name))
        filename = os.path.join(export_folder, "{}{}.ply".format(file_base_name, output_suffix or ""))

        # seam_marker = np.zeros(len(V_opt))
        # seam_marker[is_seam] = 0.5
        # seam_marker[is_sharp] = 1

        model_degrees = np.array([models[l - 1].degree if l > 0 else 0 for l in all_labels])

        rgb_colors = pack_mesh_properties_to_rgb(all_labels, model_degrees, is_seam, is_sharp)

        export_to_ply(
            V_opt, F,
            filename,
            vertex_colors=rgb_colors
        )

        # Save the sketch info as an auxiliary json file
        # This is useful when seeking to load the result and corresponding sketch/proxy in Blender
        parameters_file_path = os.path.join(export_folder, get_parameter_file_name(os.path.basename(filename)))
        with open(parameters_file_path, "w") as f:
            json.dump(sketch_info, f, indent=2)

        # Save the run log
        run_log_file_path = os.path.join(export_folder, "{}{}_log.json".format(file_base_name, output_suffix or ""))
        with open(run_log_file_path, "w") as f:
            json.dump(projection_log, f, indent=2)

    if ps_vis:
        ps.remove_all_structures()


def run_projection(
    proxy_mesh,
    labels,
    models,
    stroke_position_by_edge,
    in_out_labelling,
    mirror_symmetry,
    attract_seams_to_strokes=False,
    max_iterations=300,
    gradient_approx=False,
    print_it=False
    ):

    V = proxy_mesh.positions()
    sketch_bb_diag = bounding_box_diagonal(V)

    # If there is a mirror symmetry, remove any data points and segments that are on the other side of the mirror
    if mirror_symmetry:
        # data_points, segments, segment_mask, data_points_mask = remove_symmetric_data(data_points, segments)
        vertex_is_on_mirror = is_on_mirror(V)
    else:
        vertex_is_on_mirror = np.zeros(len(V), dtype=bool)
    
    # Insert seam vertices in mesh
    start_insertion = time.perf_counter()

    seam_vertices_id, boundary_vertices_subsets, subsets, on_stroke_vertices_id, constraint_pts, attraction_is_mandatory, in_out_labelling, vertex_is_on_mirror = insert_boundary_vertices(
        proxy_mesh,
        labels,
        stroke_position_by_edge,
        in_out_labelling,
        vertex_is_on_mirror)

    insertion_time = time.perf_counter() - start_insertion

    # Convention: label 0 corresponds to a seam vertex
    all_labels = np.append(labels + 1, np.zeros(len(proxy_mesh.positions()) - len(labels))).astype(int)

    smoothing_weights = compute_smoothing_weights(
                    proxy_mesh,
                    models,
                    boundary_vertices_subsets, seam_vertices_id,
                    w_fit=1,
                    g_max_stop=sketch_bb_diag * 0.005)


    V = proxy_mesh.positions()
    F = get_faces(proxy_mesh)

    attraction_weights = np.zeros(len(on_stroke_vertices_id))
    attraction_weights[attraction_is_mandatory] = 1

    if attract_seams_to_strokes:
        attraction_weights[np.logical_not(attraction_is_mandatory)] = 0.5

    start_proj = time.perf_counter()

    V_opt, energy_progression, gradient_norm_progression = optimize_mesh(
        proxy_mesh,
        V,
        vertex_is_on_mirror,
        in_out_labelling,
        models,
        subsets,
        smoothing_weights,
        seam_vertices_id,
        w_fit=1,
        w_smooth=100,
        w_smooth_boundaries=500,
        w_cstr=1,
        w_smooth_mirror=100,
        constrained_vertices_idx=on_stroke_vertices_id,
        constrained_pt_target=constraint_pts,
        constrained_pt_weights=attraction_weights,
        maxIt=max_iterations,
        gtol=1e-6 * len(V),
        approx_df=gradient_approx,
        iprint=print_it
    )

    proj_time = time.perf_counter() - start_proj

    log = {
            'insertion_time': insertion_time, 
            'projection_time': proj_time, 
            'energy_progression': energy_progression, 
            'gradient_norm_progression': gradient_norm_progression
        }

    V_is_seam = np.zeros(len(V_opt), dtype=bool)
    V_is_seam[seam_vertices_id] = 1


    # Cut out excess
    V_opt, F, reindex_V = remove_mesh_vertices(V_opt, F, in_out_labelling)
    all_labels = all_labels[reindex_V]
    smoothing_weights = smoothing_weights[reindex_V]
    V_is_seam = V_is_seam[reindex_V]

    # Symmetrize
    if mirror_symmetry:
        V_opt, F, index_sym = symmetrize_mesh(V_opt, F)
        all_labels = all_labels[index_sym]
        smoothing_weights = smoothing_weights[index_sym]
        V_is_seam = V_is_seam[index_sym]

    # Mark sharp vertices
    V_is_sharp = smoothing_weights < 1

    return V_opt, F, all_labels, V_is_seam, V_is_sharp, smoothing_weights, log
        

if __name__ == "__main__":

    ps.init()
    ps.set_ground_plane_mode("shadow_only")

    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch', type=str, default=".+")
    parser.add_argument('--folder-name', help='Name of the log folder', type=str, default=None)
    parser.add_argument('--exact-grad', help='Use exact gradient', dest='gradient_approx', action='store_false', default=False)
    parser.add_argument('--approx-grad', help='Use approxiamtion of gradient', dest='gradient_approx', action='store_true')
    parser.add_argument('-H', '--no-display', help='Hide debug display', dest='display', action='store_false')
    parser.add_argument('-E', '--export', help='Export the 3D mesh', dest='export_mesh', action='store_true')
    parser.add_argument('--out-folder', help='Name of the folder for outputs', type=str, default=None)
    parser.add_argument('--suffix', help='Output files suffix', default=None)
    parser.add_argument('--snap-to-strokes', help='Snap seams to strokes', dest='snap_to_strokes', action='store_true')
    parser.add_argument('--max-it', help='Max nb of optimization steps', type=int, default=300)
    parser.add_argument('-x', help='Relative camera x position', type=float, default=None)
    parser.add_argument('-y', help='Relative camera y position', type=float, default=None)
    parser.add_argument('-z', help='Relative camera z position', type=float, default=None)
    parser.set_defaults(save=False, display=True, export=False, snap_to_strokes=False)

    args = parser.parse_args()

    # Extract arguments
    sketch_name_pattern = args.sketch
    display = args.display
    output_suffix = args.suffix
    export_mesh = args.export_mesh
    attract_seams_to_strokes = args.snap_to_strokes
    max_iterations = args.max_it

    if output_suffix is not None:
        output_suffix = '_' + output_suffix

    # Default logs folder is last modified folder
    folder_path = max([os.path.join(LOGS_ROOT_FOLDER, d) for d in os.listdir(LOGS_ROOT_FOLDER) if os.path.isdir(os.path.join(LOGS_ROOT_FOLDER, d))], key=os.path.getmtime)
    if args.folder_name is not None:
        folder_name = args.folder_name.replace("/", ":")
        folder_path = os.path.join(LOGS_ROOT_FOLDER, folder_name)


    if export_mesh:
        if args.out_folder is not None:
            # export_folder_name = f"projection_{args.out_folder}"
            export_folder_name = f"{datetime.now().strftime('%Y-%m-%d')}_{args.out_folder}"
        else:
            export_folder_name = os.path.basename(folder_path)
        export_folder = os.path.join(MESH_FOLDER, export_folder_name)
        if not os.path.exists(export_folder):
            os.makedirs(export_folder)
    else:
        export_folder = None


    for root, dirs, files in os.walk(os.path.join(folder_path)):
        for f in sorted(files):
            file_name, extension = os.path.splitext(f)
            if extension == '.json' and 'parameters' not in file_name and re.search(sketch_name_pattern, f):
                # sketch_name = f.split(".")[0]

                parameter_data = try_load_data(os.path.join(folder_path, get_parameter_file_name(f)))

                sketch_name = parameter_data['sketch_name']

                print(sketch_name)

                # display_rel_offset = np.array(json.loads(metadata_per_sketch[sketch_name]['display_rel_offset']), dtype=float)
                display_rel_offset = np.array([1,1,1])
                if args.x is not None:
                    display_rel_offset[0] = args.x
                if args.y is not None:
                    display_rel_offset[1] = args.y
                if args.z is not None:
                    display_rel_offset[2] = args.z

                run_projection_from_log(
                    # metadata_per_sketch[sketch_name],
                    parameter_data,
                    log_file_path=os.path.join(folder_path, f),
                    attract_seams_to_strokes=attract_seams_to_strokes,
                    output_suffix=output_suffix,
                    display=display,
                    display_rel_offset=display_rel_offset,
                    export_folder=export_folder,
                    max_iterations=max_iterations,
                    gradient_approx=args.gradient_approx,
                    print_it=display
                )