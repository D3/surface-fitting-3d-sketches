
import polyscope as ps
import sys
import time
import os
import csv
from datetime import datetime
import json
import argparse
import re
import numpy as np
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))

from main.build_proxy import build_from_files
from main.segment_and_fit import SegmentFit

from utils.paths import *
from utils.read_user_interaction_log import get_new_segment_sharpness, get_selected_vertices



default_parameters = {
    'dist_approx': 'first_order',
    'L0': 50,
    'w_all': 100,
    'w_unary': 1.0,
    'w_smooth': 10.0,
    'w_labels': 0.01,
    'default_edge_weight': 100.0,
    'stroke_edge_weight': 1.0,
    'edge_length_factor_power': 1.0,
    'adaptive_graph_simplification': True,
    'max_model_degree': 4,
    'lambda_regularization': 1.0,
    'w_normals_fit': 0.1,
    'random_seed': 123,
    'initial_proposal_strategy': 3,
    'init_lambda_factor': 10.0,
    'init_normals_factor': 10.0,
    'max_iterations': 30,
    'propose_merge': True,
    'propose_split': False,
    'propose_lower': True,
}

def parameter_key_to_arg_string(param_key, negate=False):
    if negate:
        return "--not-{}".format(param_key.replace("_", "-"))
    else:
        return "--{}".format(param_key.replace("_", "-"))



def run_segment_and_fit(
    proxy,
    parameters,
    display,
    # save_log_path,
    starting_state=None,
    simulated_user_interactions=[],
    # run_count=1,
    # save_all_runs=False
    ):

    print(f"[SEGMENT AND FIT] start segmenting sketch: {parameters['sketch_name']}")

    sf = SegmentFit(
        proxy,
        parameters['dist_approx'],
        parameters['w_all'],
        parameters['w_unary'],
        parameters['w_smooth'],
        parameters['w_labels'],
        parameters['sketch_error_dist'],
        parameters['lambda_regularization'],
        w_proxy_vertices_fit=0,
        w_normals_fit=parameters['w_normals_fit'])

    # Initialize
    if starting_state is None :
        sf.initialize_models(
            parameters['L0'],
            sample_ratio_bounds = (0.05, 0.1),
            initial_proposal_strategy=parameters['initial_proposal_strategy'],
            init_lambda_factor=parameters['init_lambda_factor'],
            init_normals_factor=parameters['init_normals_factor'],
            random_seed=parameters['random_seed'],
            degree=parameters['max_model_degree'])
    else :
        # Initialize SegmentFit with previous state
        sf.initialize_with_state(**starting_state)


    n_it = 0

    while n_it < parameters['max_iterations']:
        # Optimize labelling
        time0 = time.perf_counter()
        sf.optimize_labelling()

        time1 = time.perf_counter()

        print("time to optimize labelling =", time1 - time0)

        # Visualize iteration
        if display:
            structs = sf.visualize_step_by_projection()
            # structs.extend(sf.visualize_step())
            ps.show()
            # ps_clear(structs)

        # Converged?
        if sf.converged:
            print("converged")
            print("user interactions left", len(simulated_user_interactions))
            if len(simulated_user_interactions) == 0:
                print("Converged after {} iterations".format(n_it))
                break
            else:
                # Continue after applying the first interaction left in the list
                while len(simulated_user_interactions) > 0:
                    interaction = simulated_user_interactions.pop(0)
                    # According to type, call appropriate method on sf
                    if interaction['action'] == 'run':
                        # Run now and continue doing the other interactions after the run
                        print("Interaction: run method")
                        break
                    # elif interaction['action'] == 'mark_sharp':
                    #     new_sharpness = get_new_segment_sharpness(interaction, len(data_points), segments, proxy.segment_sharpness, data_points_mask)
                    #     # partition_labels, new_models = sf.update_stroke_sharpness(new_sharpness)
                    #     # ps_proxy_mesh.add_scalar_quantity("after interaction partiitons", partition_labels)
                    #     # ps.show()
                    elif interaction['action'] == 'propose_here':
                        selected_vertices = get_selected_vertices(interaction, proxy.V, proxy.F, parameters['is_symmetric'], proxy.in_out_labelling)
                        proxy_vertices_selected = selected_vertices[proxy.proxy_vertex_is_node[selected_vertices]]
                        # nodes_selected = proxy.vertex_idx_to_node_idx_map[proxy_vertices_selected]
                        sf.propose_here(selected_vertices)

                        # Visualize
                        if display:
                            ps.register_point_cloud(f"scribble propose {len(simulated_user_interactions)}", proxy.V[selected_vertices], color=(0.929, 0.290, 0.290), radius=0.008, material='flat')
                    elif interaction['action'] == 'merge':
                        selected_vertices = get_selected_vertices(interaction, proxy.V, proxy.F, parameters['is_symmetric'], proxy.in_out_labelling)
                        proxy.update_edge_weight_boost(selected_vertices, smoothness_value=parameters['default_edge_weight'])
                        sf.propose_merge(selected_vertices)

                        # Visualize
                        if display:
                            ps.register_point_cloud(f"scribble merge {len(simulated_user_interactions)}", proxy.V[selected_vertices], color=(0, 0.439, 0.615), radius=0.008, material='flat')
                    elif interaction['action'] == 'add_stroke':
                        new_stroke_points = np.array(interaction['parameters']['vertices'])
                        ps.register_point_cloud("New stroke points", new_stroke_points)
                        # ps.show()
                        sf.propose_after_new_stroke(new_stroke_points)
                        # if display:
                        #     sf.proxy.visualize()
                        #     ps.show()
                    else:
                        print(f"ERROR: User interaction of type {interaction['action']} not supported")

                if display:
                    proxy.visualize()

                    ps.show()

        # Re-fit models
        time0 = time.perf_counter()

        sf.propose_models(
            # translation,
            max_model_degree=parameters['max_model_degree'],
            propose_lower_degree=parameters['propose_lower'],
            propose_new=False,
            propose_new_by_split=parameters['propose_split'],
            propose_new_by_merge=parameters['propose_merge'])

        time1 = time.perf_counter()

        print("time to refit/propose models =", time1 - time0)

        n_it += 1

    print(f"[SEGMENT AND FIT] end segmenting sketch: {parameters['sketch_name']}")

    return sf
    

def log_sf(sf, log_path, parameters, additional_properties_to_log={}):
    print("saving log at {}".format(log_path))
    sf.export_log(log_path, additional_properties_to_log)

    # Save parameters
    save_log_folder = os.path.dirname(log_path)
    log_file_name =os.path.basename(log_path)
    parameters_file_path = os.path.join(save_log_folder, get_parameter_file_name(log_file_name))
    print("saving parameters at", parameters_file_path)
    with open(parameters_file_path, "w") as f:
        json.dump(parameters, f, indent=2)

if __name__ == "__main__":

    start_time = time.perf_counter()

    parser = argparse.ArgumentParser()

    parser.add_argument('--sketch', help='Name of the sketch (filtering pattern)', type=str, default=None)
    parser.add_argument('--filtering-key', help='Data key that the filtering operates on (default is sketch_name)', type=str, default="sketch_name")
    parser.add_argument('--runs', help='Random runs count', type=int, default=1)
    parser.add_argument('-S', '--save', help='Save the log?', dest='save', action='store_true', default=False)
    parser.add_argument('--save-all', help='Save log for all runs (not only the best)', dest='save_all_runs', action='store_true', default=False)
    parser.add_argument('--log-folder', help='Log output folder', type=str, default=None)
    parser.add_argument('--log-suffix', help='Log file name suffix', type=str, default=None)
    parser.add_argument('-H', '--no-display', help='Debug display', dest='display', action='store_false', default=True)
    parser.add_argument('--sketch-file-path', help='Sketch path to override the default in metadata', type=str, default=None)
    parser.add_argument('--proxy-file-path', help='Proxy mesh path to override the default in metadata', type=str, default=None)
    parser.add_argument('--sketch-error-dist', help='Sketch error dist to override the default in metadata', type=float, default=None)
    parser.add_argument('--symmetric', help='Override is_symmetric flag from metadata', dest='is_symmetric', action='store_true', default=None)
    parser.add_argument('--not-symmetric', help='Override is_symmetric flag from metadata', dest='is_symmetric', action='store_false', default=None)
    parser.add_argument('--not-open', help='Override open_boundary flag from metadata', dest='open_boundary', action='store_false', default=None)
    parser.add_argument('--metadata-path', help='Override metadata file path', type=str, default=None)

    # Add method specific parameters
    for key, value in default_parameters.items():
        if type(value) is bool:
            parser.add_argument(parameter_key_to_arg_string(key), help=f"Method flag {key}", dest=key, action='store_true', default=value)
            parser.add_argument(parameter_key_to_arg_string(key, negate=True), help=f"Method negative flag {key}", dest=key, action='store_false', default=value)
        else:
            parser.add_argument(parameter_key_to_arg_string(key), help=f"Method parameter {key}", default=value, type=type(value))

    args = parser.parse_args()

    # Extract arguments
    
    if args.sketch is None:
        sketch_name_pattern = ".+"
    else:
        sketch_name_pattern = args.sketch

    runs = args.runs
    save_result = args.save
    debug_display = args.display
    log_folder = args.log_folder
    if log_folder is not None:
        save_result = True
        log_folder = log_folder.replace("/", ":")
    log_suffix = args.log_suffix

    args_dict = vars(args)
    # Set method parameters
    for key in default_parameters:
        default_parameters[key] = args_dict[key]

    # Create a new log folder
    if save_result:
        if log_folder is None:
            log_folder = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        save_log_folder = os.path.join(LOGS_ROOT_FOLDER, f"{datetime.now().strftime('%Y-%m-%d')}_{log_folder}")

        if not os.path.exists(save_log_folder):
            os.makedirs(save_log_folder)

    else:
        save_log_folder = None
        

    if debug_display:
        ps.init()
        ps.set_ground_plane_mode("shadow_only")

    metadata_file_path = METADATA_FILE_PATH
    if args.metadata_path is not None:
        metadata_full_path = os.path.join(ROOT_FOLDER, args.metadata_path)
        if os.path.isfile(metadata_full_path):
            metadata_file_path = metadata_full_path

    with open(metadata_file_path, mode='r', encoding='utf-8-sig') as csv_file:
        metadata = csv.DictReader(csv_file, delimiter=";")

        for i, row in enumerate(metadata):

            if re.search(sketch_name_pattern, row[args.filtering_key]):

                sketch_file_path = os.path.join(SKETCH_OBJ_FOLDER, f"{row['sketch_name']}.obj")
                proxy_file_path = os.path.join(CLEANED_PROXIES_FOLDER, f"{row['sketch_name']}.obj")

                if args.sketch_file_path is not None:
                    sketch_file_path = args.sketch_file_path

                if args.proxy_file_path is not None:
                    proxy_file_path = args.proxy_file_path

                if not Path(sketch_file_path).is_file():
                    print(f"Sketch {row['sketch_name']} does not have a strokes mesh")
                    continue

                if not Path(proxy_file_path).is_file():
                    print(f"Sketch {row['sketch_name']} does not have a proxy mesh")
                    continue

                print("Generating segmentation for sketch:", row['sketch_name'])

                parameters = default_parameters.copy()

                # Override metadata properties
                parameters['sketch_name'] = row['sketch_name']
                parameters['sketch_file'] = sketch_file_path
                parameters['proxy_file'] = proxy_file_path
                parameters['is_symmetric'] = args.is_symmetric if args.is_symmetric is not None else bool(int(row['is_symmetric']))
                parameters['sketch_error_dist'] = args.sketch_error_dist if args.sketch_error_dist is not None else float(row['sketch_error_dist'])
                parameters['open_boundary'] = args.open_boundary if args.open_boundary is not None else bool(int(row['open_boundary']))
                parameters['on_border_stroke_points'] = [] if len(row['on_border_stroke_points']) == 0 else json.loads(row['on_border_stroke_points'])
                parameters['ignored_stroke_points'] = [] if len(row['ignored_stroke_points']) == 0 else json.loads(row['ignored_stroke_points'])

                if save_log_folder is not None:
                    log_file_name = row['sketch_name']
                    if log_suffix is not None:
                        log_file_name += "_" + log_suffix
                    log_file_name +=  '.json'
                    save_log_path = os.path.join(LOGS_ROOT_FOLDER, save_log_folder, log_file_name)
                else:
                    save_log_path = None                    

                # Build proxy graph
                time0_proxy = time.perf_counter()
                proxy, _, _ = build_from_files(
                    parameters['sketch_file'],
                    parameters['proxy_file'],
                    parameters['is_symmetric'],
                    parameters['open_boundary'],
                    parameters['on_border_stroke_points'],
                    parameters['ignored_stroke_points'],
                    parameters['default_edge_weight'],
                    parameters['stroke_edge_weight'],
                    parameters['edge_length_factor_power'],
                    parameters['adaptive_graph_simplification'],
                    debug_display
                )
                runtime_build_proxy = time.perf_counter() - time0_proxy

                sf_per_run = []
                final_energy_per_run = []
                seeds = []

                # random_seed = parameters['random_seed']

                try:

                    for run in range(runs):

                        sf = run_segment_and_fit(
                            proxy,
                            parameters,
                            # **parameters,
                            display=debug_display,
                            # save_log_path=save_log_path,
                            # run_count=runs,
                            # save_all_runs=args.save_all_runs
                        )

                        # Save result
                        sf_per_run.append(sf)
                        final_energy_per_run.append(sf.energy_progression[-1])
                        seeds.append(parameters['random_seed'])

                        parameters['random_seed'] += 1


                except KeyboardInterrupt:
                    print('Interrupted')
                    sys.exit(0)

                additional_logs = {'proxy_build_time': runtime_build_proxy}

                if args.save_all_runs and save_log_path is not None:
                    for run_idx, sf in enumerate(sf_per_run):
                        log_path, log_path_extension = os.path.splitext(save_log_path)
                        run_log_path = f"{log_path}_{run_idx}{log_path_extension}"
                        # print("saving log at {}".format(run_log_path))
                        # sf.export_log(run_log_path, parameters['random_seed'])

                        # Save parameters
                        parameters['random_seed'] = seeds[run_idx]
                        log_sf(sf, run_log_path, parameters, additional_logs)

                else:
                    # Choose best run
                    print(f"Energies = {final_energy_per_run}")
                    best_run = np.argmin(final_energy_per_run)
                    sf_best = sf_per_run[best_run]
                    parameters['random_seed'] = seeds[best_run]

                    print("Best run is run", best_run + 1)

                    # Store log
                    if save_log_path is not None:
                        log_sf(sf_best, save_log_path, parameters, additional_logs)

                    sf_best.print()

                    # Display result
                    if debug_display:
                        structs = sf.visualize_step_by_projection()
                        ps.show()
                        sf_best.plot_energy_progression()

                        ps.remove_all_structures()

    print("Total generation runtime = {:.3f} s".format(time.perf_counter() - start_time))

